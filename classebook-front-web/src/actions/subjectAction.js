
import baseUrl from 'config/config';
import axios from 'axios';

export function getSubject() {
    return function (dispatch) {
        return fetch(`${baseUrl.baseUrl}/subjects`).then(response => response.json()).then(json => {
                const subjectList = json.filter(element => element.status);
                dispatch({ type: "DATA_LOADED_SUBJECT", payload: subjectList });
            });
    };
};

export function addData(subject) {

    return function (dispatch) {
        axios.post(`${baseUrl.baseUrl}/subjects`, {
            name: subject.name,
            description: subject.description,
            coefficient: subject.coefficient,
            status: true,
            id_establishment: subject.id_establishment
        }).then(response => {
            dispatch({ type: "ADD_SUBJECT", payload: response.data });
            alert('L\'ajout est effectué avec succès');
        })
            .catch(error => {
                console.log(error)
            });
    };

}

export function deleteData(idItem) {

    return function (dispatch) {
        axios.get(`${baseUrl.baseUrl}/subjects/` + idItem)
            .then(res => {
                const dataSubject = res.data;
                axios.put(`${baseUrl.baseUrl}/subjects/` + dataSubject.id, {
                    name: dataSubject.name,
                    description: dataSubject.description,
                    coefficient: dataSubject.coefficient,
                    status: false,
                    id_establishment: dataSubject.id_establishment,
                    id: dataSubject.id,
                })
                    .then(response => {
                        dispatch({ type: "DELETE_SUBJECT", payload: response.data });
                    })
                    .catch(function (error) {
                        alert('error')
                    });
            });
    };

}

export function editData(data) {

    return function (dispatch) {
                axios.put(`${baseUrl.baseUrl}/subjects/` + data.id, {
                    name: data.name,
                    description: data.description,
                    coefficient: data.coefficient,
                    status: true,
                    id_establishment: data.id_establishment,
                    id: data.id,
                })
                    .then(response => {
                        dispatch({ type: "EDIT_SUBJECT", payload: response.data });
                    })
                    .catch(function (error) {
                        alert('error')
                    });
            
    };

}
import axios from 'axios';
import baseUrl from 'config/config';

export function getPresence() {
    return function (dispatch) {
        return fetch(`${baseUrl.baseUrl}/presences`)
        .then(response => response.json())
        .then(json => {                
                dispatch({ type: "GET_ALL_PRESENCE", payload: json });
            });
    };
};

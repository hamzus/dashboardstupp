import axios from 'axios';
import {
    HIDE_MESSAGE,
    INIT_URL,
    ON_HIDE_LOADER,
    ON_SHOW_LOADER,
    SHOW_MESSAGE,
    SIGNIN_FACEBOOK_USER,
    SIGNIN_FACEBOOK_USER_SUCCESS,
    SIGNIN_GITHUB_USER,
    SIGNIN_GITHUB_USER_SUCCESS,
    SIGNIN_GOOGLE_USER,
    SIGNIN_GOOGLE_USER_SUCCESS,
    SIGNIN_TWITTER_USER,
    SIGNIN_TWITTER_USER_SUCCESS,
    SIGNIN_USER,
    SIGNIN_USER_SUCCESS,
    SIGNOUT_USER,
    SIGNOUT_USER_SUCCESS,
    SIGNUP_USER,
    SIGNUP_USER_SUCCESS,
    THEME_COLOR
} from 'constants/ActionTypes';
import cst from 'config/config';

export const userSignUp = (user) => {
    return {
        type: SIGNUP_USER,
        payload: user
    }
};

export const userSignIn = (user) => {
    return dispatch => axios.post(`${cst.baseUrl}/users/login`, user)
        .then(res => {
            console.log(res.headers)
            localStorage.setItem('token', res.data.id);
            localStorage.setItem('user_id', res.data.userId);
            getprofile(localStorage.user_id);
            return axios.get(`${cst.baseUrl}/users/${res.data.userId}?access_token=${res.data.id}`)
        })
        .then(res => {
            localStorage.setItem('user', JSON.stringify(res.data));
            return dispatch({
                type: SIGNIN_USER_SUCCESS,
                payload: user
            })
        }).catch((err)=> dispatch(
               
            showAuthMessage('Veuillez vérifier votre login et mot de passe')

        ))
};

function getprofile(userId) {
    axios.get(`${cst.baseUrl}/profiles/findOne?filter={"where":{"user_id":`+userId +`}}`)
    .then(res => {
        const profiles = res.data;
        localStorage.setItem('profileId', profiles.id);
        localStorage.setItem('roles_id', profiles.role_id);
        localStorage.setItem('establishment_id', profiles.establishment_id);
    });

}
export const userSignOut = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('profileId');
    localStorage.removeItem('roles_id');
    localStorage.removeItem('user_id');
    localStorage.removeItem('establishment_id');
    
    return {
        type: SIGNOUT_USER
    };
};
export const userSignUpSuccess = (authUser) => {
    return {
        type: SIGNUP_USER_SUCCESS,
        payload: authUser
    };
};

export const userSignInSuccess = (authUser) => {
    return {
        type: SIGNIN_USER_SUCCESS,
        payload: authUser
    }
};
export const userSignOutSuccess = () => {
    return {
        type: SIGNOUT_USER_SUCCESS,
    }
};

export const showAuthMessage = (message) => {
    return {
        type: SHOW_MESSAGE,
        payload: message
    };
};


export const userGoogleSignIn = () => {
    return {
        type: SIGNIN_GOOGLE_USER
    };
};
export const userGoogleSignInSuccess = (authUser) => {
    return {
        type: SIGNIN_GOOGLE_USER_SUCCESS,
        payload: authUser
    };
};
export const userFacebookSignIn = () => {
    return {
        type: SIGNIN_FACEBOOK_USER
    };
};
export const userFacebookSignInSuccess = (authUser) => {
    return {
        type: SIGNIN_FACEBOOK_USER_SUCCESS,
        payload: authUser
    };
};
export const setInitUrl = (url) => {
    return {
        type: INIT_URL,
        payload: url
    };
};
export const userTwitterSignIn = () => {
    return {
        type: SIGNIN_TWITTER_USER
    };
};
export const userTwitterSignInSuccess = (authUser) => {
    return {
        type: SIGNIN_TWITTER_USER_SUCCESS,
        payload: authUser
    };
};
export const userGithubSignIn = () => {
    return {
        type: SIGNIN_GITHUB_USER
    };
};
export const userGithubSignInSuccess = (authUser) => {
    return {
        type: SIGNIN_GITHUB_USER_SUCCESS,
        payload: authUser
    };
};
export const showAuthLoader = () => {
    return {
        type: ON_SHOW_LOADER,
    };
};

export const hideMessage = () => {
    return {
        type: HIDE_MESSAGE,
    };
};
export const hideAuthLoader = () => {
    return {
        type: ON_HIDE_LOADER,
    };
};

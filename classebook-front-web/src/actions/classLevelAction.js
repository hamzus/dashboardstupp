
import { classService } from '../_services';

export const ClassLevelAction = {
    getLevels,
    addLevel,
    archiverLevel,
    updateLevel
};

export const getLevels = () => {
    return dispatch => {
        let apiEndpoint = '/class_levels';
        classService.get(apiEndpoint)
            .then((response) => {
                dispatch(receiveLevels(response.data))
            }).catch((err) => {
            })
    };
}

const receiveLevels = (levels) => ({
    type: "FETECHED_ALL_LEVEL",
    payload: levels,
});

export const addLevel = (itemClass) => {
    console.log(itemClass)
    return (dispatch) => {
        let apiEndpoint = '/class_levels/';
        classService.post(apiEndpoint, itemClass)
            .then(response => {
                let data = response.data;
                console.log(data)
                dispatch({ type: "ADD_LEVEL", payload: data })
                //dispatch({ type: "FETECHED_ALL_CLASS" }) 

            })
            .catch(error => { throw (error) });
    };
};
export const archiverLevel = (id) => {

    return (dispatch) => {
        let apiEndpoint = '/class_levels/' + id;
        classService.get(apiEndpoint)
            .then(response => {
                console.log(response)
                const item = response.data;
                classService.put(apiEndpoint, {
                    name: item.name,
                    category: item.category,
                    level: item.level,
                    section: item.section,
                    description: item.description,
                    status : false

                })
                    .then(response => {
                        dispatch({ type: "REMOVE_LEVEL", payload: response.data })

                    })
                    .catch(function (error) {
                        alert('error')
                    });

            })
    }
};

export const updateLevel = (item) => {
    return (dispatch) => {
        let apiEndpoint = '/class_levels/' + item.id;
        const { name, level_class, establishment, capacity, start_date, end_date, description, students_number, student_id, professor_id } = item;

        classService.put(apiEndpoint, {
            name: item.name,
            category: item.category,
            level: item.level,
            section: item.section,
            description: item.description
        })
            .then(response => {
                dispatch({ type: "UPDATE_LEVEL", payload: response.data })
            }).catch(error => { throw (error) });
    };
};

import axios from 'axios';
import baseUrl from 'config/config';

export function getStudents() {
    return function (dispatch) {
        return fetch(`${baseUrl.baseUrl}/students`)
        .then(response => response.json())
        .then(json => {                
                dispatch({ type: "GET_ALL_STUDENT", payload: json });
            });
    };
};

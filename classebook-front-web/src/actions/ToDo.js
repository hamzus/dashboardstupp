import {
  FETCH_ALL_TODO,
  FETCH_ALL_TODO_CONVERSATION,
  FETCH_ALL_TODO_CONVERSATION_SUCCESS,
  FETCH_ALL_TODO_SUCCESS,
  GET_ALL_TODO,
  GET_IMPORTANT_TODO,
  GET_MARK_AS_START,
  GET_NAV_FILTERS,
  GET_NAV_LABELS,
  GET_STARRED_TODO,
  GET_UNIMPORTANT_TODO,
  GET_UNSELECTED_ALL_TODO,
  GET_UNSTARRED_TODO,
  HANDLE_REQUEST_CLOSE,
  ON_DELETE_TODO,
  ON_HIDE_LOADER,
  ON_LABEL_MENU_ITEM_SELECT,
  ON_LABEL_SELECT,
  ON_LABEL_UPDATE,
  ON_OPTION_MENU_SELECT,
  ON_SORTEND,
  ON_TODO_ADD,
  ON_TODO_CHECKED,
  ON_TODO_SELECT,
  ON_TODO_UPDATE,
  ON_TOGGLE_DRAWER,
  SEARCH_TODO,
  SELECT_ALL_TODO,
  SET_CURRENT_TODO_NULL,
  SHOW_MESSAGE,
  UPDATE_SEARCH,
  ADD_HOMEWORK
} from 'constants/ActionTypes'
import baseUrl from 'config/config';
import axios from 'axios';
const roleIdParent = 4;
const roleIdStudent = 5;

export const fetchTodo = () => {
  return {
    type: FETCH_ALL_TODO
  };
};
export const fetchTodoConversation = () => {
  return {
    type: FETCH_ALL_TODO_CONVERSATION
  };
};

export const fetchTodosSuccess = (mails) => {
  return {
    type: FETCH_ALL_TODO_SUCCESS,
    payload: mails
  }
};
export const fetchTodosConversationSuccess = (mails) => {
  return {
    type: FETCH_ALL_TODO_CONVERSATION_SUCCESS,
    payload: mails
  }
};

export const showTodoMessage = (message) => {
  return {
    type: SHOW_MESSAGE,
    payload: message
  };
};
export const onSortEnd = (data) => {
  return {
    type: ON_SORTEND,
    payload: data
  };
};

export const onOptionMenuSelect = () => {
  return {
    type: ON_OPTION_MENU_SELECT,
  };
};
export const onLabelSelect = () => {
  return {
    type: ON_LABEL_SELECT,
  };
};

export const selectAllTodo = () => {
  return {
    type: SELECT_ALL_TODO,
  };
};

// export const getAllTodo = () => {
//   return {
//     type: GET_ALL_TODO,
//   };
// };


export const getUnselectedAllTodo = () => {
  return {
    type: GET_UNSELECTED_ALL_TODO,
  };
};

export const getStarredToDo = () => {
  return {
    type: GET_STARRED_TODO,
  };
};

export const getUnStarredTodo = () => {
  return {
    type: GET_UNSTARRED_TODO,
  };
};
export const getImportantToDo = () => {
  return {
    type: GET_IMPORTANT_TODO,
  };
};
export const getUnimportantToDo = () => {
  return {
    type: GET_UNIMPORTANT_TODO,
  };
};
export const onLabelMenuItemSelect = (label) => {
  return {
    type: ON_LABEL_MENU_ITEM_SELECT,
    payload: label
  };
};
export const onLabelUpdate = (data) => {
  return {
    type: ON_LABEL_UPDATE,
    payload: data
  };
};
export const onMarkAsStart = (data) => {
  return {
    type: GET_MARK_AS_START,
    payload: data
  };
};

export const onToDoUpdate = (data) => {
  return {
    type: ON_TODO_UPDATE,
    payload: data
  };
};

export const onDeleteToDo = (data) => {
  return {
    type: ON_DELETE_TODO,
    payload: data
  };
};
export const getNavFilters = (data) => {
  return {
    type: GET_NAV_FILTERS,
    payload: data
  };
};
export const getNavLabels = (data) => {
  return {
    type: GET_NAV_LABELS,
    payload: data
  };
};

export const onSearchTodo = (searchText) => {
  return {
    type: SEARCH_TODO,
    payload: searchText
  };
};
export const onTodoChecked = (data) => {
  return {
    type: ON_TODO_CHECKED,
    payload: data
  };
};
export const onTodoAdd = (data) => {
  return {
    type: ON_TODO_ADD,
    payload: data
  };
};
export const onTodoSelect = (todo) => {
  return {
    type: ON_TODO_SELECT,
    payload: todo
  };
};
export const setCurrentToDoNull = () => {
  return {
    type: SET_CURRENT_TODO_NULL,
  };
};

export const toDoToggleDrawer = () => {
  return {
    type: ON_TOGGLE_DRAWER,
  };
};

export const updateSearch = (searchTodo) => {
  return {
    type: UPDATE_SEARCH,
    payload: searchTodo
  };
};

export const hideToDoLoader = () => {
  return {
    type: ON_HIDE_LOADER,
  };
};
export const handleToDoMenuRequestClose = () => {
  return {
    type: HANDLE_REQUEST_CLOSE,
  };
};

export function addHomework(homework) {


  return function (dispatch) {
    axios.post(`${baseUrl.baseUrl}/homeworks`, {
      title: homework.title,
      description: homework.description,
      date_creation: homework.date_creation,
      status: true,
      professor_id: homework.professor_id,
      subject_id: homework.subject_id
    }).then(response => {
      dispatch({ type: "ADD_HOMEWORK", payload: response.data });
      alert('L\'ajout est effectué avec succès');
    })
      .catch(error => {
        console.log(error)
      });
  };


};

export function assignHomework(homework) {

  return function (dispatch) {
    axios.post(`${baseUrl.baseUrl}/homeworks_classes`, {
      correction_date: homework.correction_date,
      status: true,
      homework_id: homework.homework_id,
      class_id: homework.class_id
    }).then(response => {
      dispatch({ type: "ASSIGN_HOMEWORK", payload: response.data });
      alert('L\'affectation est effectuée avec succès');
    })
      .catch(error => {
        console.log(error)
      });
  };
};

export const getAllTodo = () => {
  const roleId = parseInt(localStorage.roles_id);

  if (roleId === roleIdParent) {
    return function (dispatch) {
      axios.get(`${baseUrl.baseUrl}/parents/findOne?filter={"where":{"profile_id":` + localStorage.profileId + `}}`)
        .then(res => {
          let parentId = res.data.id;
          axios.get(`${baseUrl.baseUrl}/students/findOne?filter={"where":{"parent_id":` + parentId + `}}`)
            .then(res => {
              let classId = res.data.class_id;
              axios.get(`${baseUrl.baseUrl}/homeworks_classes`)
                .then(res => {
                  let homeworkClassesList = res.data;
                  const results = homeworkClassesList.filter(homeworkClasse => homeworkClasse.class_id === classId && homeworkClasse.status === true);
                  let homeworkToParent = [];
                  let obj = {};
                  results.map(element => {
                    axios.get(`${baseUrl.baseUrl}/homeworks/` + element.homework_id)
                      .then(res => {
                        let title = res.data.title;
                        let description = res.data.description;
                        let date_creation = res.data.date_creation;
                        let correction_date = element.correction_date;

                        axios.get(`${baseUrl.baseUrl}/subjects/` + res.data.subject_id)
                          .then(response => {
                            let subjectName = response.data.name;
                            axios.get(`${baseUrl.baseUrl}/professors/` + res.data.professor_id)
                              .then(response => {
                                obj = {
                                  'title': title,
                                  'description': description,
                                  'date_creation': date_creation,
                                  'correction_date': correction_date,
                                  'subjectName': subjectName,
                                  'professorName': response.data.surname + ' ' + response.data.name
                                }
                                homeworkToParent.push(obj)
                                if (homeworkToParent.length === results.length) {
                                  dispatch({ type: GET_ALL_TODO, payload: homeworkToParent });
                                }
                              })
                          })

                      })
                  })
                });
            });
        });
    }
  } else if (roleId === roleIdStudent) {
    return function (dispatch) {
      axios.get(`${baseUrl.baseUrl}/students/findOne?filter={"where":{"profile_id":` + localStorage.profileId + `}}`)
        .then(res => {
          let classId = res.data.class_id;
          axios.get(`${baseUrl.baseUrl}/homeworks_classes`)
            .then(res => {
              let homeworkClassesList = res.data;
              const results = homeworkClassesList.filter(homeworkClasse => homeworkClasse.class_id === classId && homeworkClasse.status === true);
              let homeworkToStudent = [];
              let obj = {};
              results.map(element => {
                axios.get(`${baseUrl.baseUrl}/homeworks/` + element.homework_id)
                  .then(res => {
                    let title = res.data.title;
                    let description = res.data.description;
                    let date_creation = res.data.date_creation;
                    let correction_date = element.correction_date;
                    axios.get(`${baseUrl.baseUrl}/subjects/` + res.data.subject_id)
                      .then(response => {
                        let subjectName = response.data.name;
                        axios.get(`${baseUrl.baseUrl}/professors/` + res.data.professor_id)
                          .then(response => {
                            obj = {
                              'title': title,
                              'description': description,
                              'date_creation': date_creation,
                              'correction_date': correction_date,
                              'subjectName': subjectName,
                              'professorName': response.data.surname + ' ' + response.data.name
                            }
                            homeworkToStudent.push(obj)
                            if (homeworkToStudent.length === results.length) {
                              dispatch({ type: GET_ALL_TODO, payload: homeworkToStudent });
                            }
                          })
                      })

                  })
              })
            });

        });
    }
  } else {
    return function (dispatch) {
      return fetch(`${baseUrl.baseUrl}/homeworks`)
        .then(response => response.json())
        .then(json => {
          const homeworks = json.filter(element => element.status);
          dispatch({ type: GET_ALL_TODO, payload: homeworks });
        });
    };
  }
};


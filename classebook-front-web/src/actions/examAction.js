import baseUrl from 'config/config';
import axios from 'axios';

export function getExam() {
    return function (dispatch) {
        return fetch(`${baseUrl.baseUrl}/exams`)
            .then(response => response.json())
            .then(json => {
                dispatch({ type: "DATA_LOADED_EXAM", payload: json });
            });
    };
};

export function addExam(exam) {

    return function (dispatch) {
        axios.post(`${baseUrl.baseUrl}/exams`, {
            type: exam.type,
            period: exam.period,
            coefficient: exam.coefficient,
            subject_id: exam.subject_id,
            establishment_id: exam.establishment_id,
            class_id: exam.class_id,
            status: true
        }).then(response => {
            dispatch({ type: "ADD_EXAM", payload: response.data });
            alert('L\'ajout est effectué avec succès');
        })
            .catch(error => {
                console.log(error)
            });
    };

}

export function deleteExam(idItem) {

    return function (dispatch) {
        axios.get(`${baseUrl.baseUrl}/exams/` + idItem)
            .then(res => {
                const exam = res.data;
                axios.put(`${baseUrl.baseUrl}/exams/` + exam.id, {
                    type: exam.type,
                    period: exam.period,
                    coefficient: exam.coefficient,
                    subject_id: exam.subject_id,
                    establishment_id: exam.establishment_id,
                    class_id: exam.class_id,
                    status: true
                })
                    .then(response => {
                        dispatch({ type: "DELETE_EXAM", payload: response.data });
                    })
                    .catch(function (error) {
                        alert('error')
                    });
            });
    };
}

export function editExam(data) {

    return function (dispatch) {
        axios.put(`${baseUrl.baseUrl}/exams/` + data.id, {
            type: data.type,
            period: data.period,
            coefficient: data.coefficient,
            subject_id: data.subject_id,
            status: true,
            id: data.id,
            establishment_id: data.establishment_id,
            class_id: data.class_id,
        })
            .then(response => {
                dispatch({ type: "EDIT_EXAM", payload: response.data });
            })
            .catch(function (error) {
                alert('error')
            });

    };

}
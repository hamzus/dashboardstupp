
import baseUrl from 'config/config';
import axios from 'axios';

export function getEstablishment() {
    return function (dispatch) {
        axios.get(`${baseUrl.baseUrl}/establishments`)
            .then(res => {
                const ListEstablishments = res.data;
                const establishmentList = ListEstablishments.filter(element => element.status);
                dispatch({ type: "GET_ALL_ESTABLISHMENT", payload: establishmentList });
            });
    };
};

export function addEstablishment(establishment) {

    return function (dispatch) {
        axios.post(`${baseUrl.baseUrl}/establishments`, {
            name: establishment.name,
            adress: establishment.adress,
            code: establishment.code,
            city: establishment.city,
            country: establishment.country,
            phone: establishment.phone,
            email_establishment: establishment.email_establishment,
            website: establishment.website,
            surname_director: establishment.surname_director,
            name_director: establishment.name_director,
            email_director: establishment.email_director,
            phone_director: establishment.phone_director,
            categories: establishment.categories,
            number_students: establishment.number_students,
            contract_start_date: establishment.contract_start_date,
            contract_end_date: establishment.contract_end_date,
            number_sms: establishment.number_sms,
            mode_payment: establishment.mode_payment,
            status: true,
            module_id : establishment.module_id,

        }).then(response => {
            dispatch({ type: "ADD_ESTABLISHMENT", payload: response.data });
            alert('L\'ajout est effectué avec succès');
        })
            .catch(error => {
                console.log(error)
            });
    };
}

export function deleteData(idItem) {

    return function (dispatch) {
        axios.get(`${baseUrl.baseUrl}/establishments/` + idItem)
            .then(res => {
                const dataEstablishment = res.data;
                axios.put(`${baseUrl.baseUrl}/establishments/` + dataEstablishment.id, {
                    name: dataEstablishment.name,
                    adress: dataEstablishment.adress,
                    code: dataEstablishment.code,
                    city: dataEstablishment.city,
                    country: dataEstablishment.country,
                    phone: dataEstablishment.phone,
                    email_establishment: dataEstablishment.email_establishment,
                    website: dataEstablishment.website,
                    surname_director: dataEstablishment.surname_director,
                    name_director: dataEstablishment.name_director,
                    email_director: dataEstablishment.email_director,
                    phone_director: dataEstablishment.phone_director,
                    categories: dataEstablishment.categories,
                    number_students: dataEstablishment.number_students,
                    contract_start_date: dataEstablishment.contract_start_date,
                    contract_end_date: dataEstablishment.contract_end_date,
                    number_sms: dataEstablishment.number_sms,
                    mode_payment: dataEstablishment.mode_payment,
                    status: false,
                    module_id : dataEstablishment.module_id,
                    id: dataEstablishment.id,
                })
                    .then(response => {
                        dispatch({ type: "DELETE_ESTABLISHMENT", payload: response.data });
                    })
                    .catch(function (error) {
                        alert('error')
                    });
            });
    };
}

export function editData(establishment) {

    return function (dispatch) {
        axios.put(`${baseUrl.baseUrl}/establishments/` + establishment.id, {
            name: establishment.name,
            adress: establishment.adress,
            code: establishment.code,
            city: establishment.city,
            country: establishment.country,
            phone: establishment.phone,
            email_establishment: establishment.email_establishment,
            website: establishment.website,
            surname_director: establishment.surname_director,
            name_director: establishment.name_director,
            email_director: establishment.email_director,
            phone_director: establishment.phone_director,
            categories: establishment.categories,
            number_students: establishment.number_students,
            contract_start_date: establishment.contract_start_date,
            contract_end_date: establishment.contract_end_date,
            number_sms: establishment.number_sms,
            mode_payment: establishment.mode_payment,
            status: establishment.status,
            module_id: establishment.module_id,
            id: establishment.id,

        })
            .then(response => {
                dispatch({ type: "EDIT_ESTABLISHMENT", payload: response.data });
            })
            .catch(function (error) {
                alert('error')
            });

    };
}
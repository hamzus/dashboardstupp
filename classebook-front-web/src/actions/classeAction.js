
import { classService } from '../_services';

export const classAction = {
  getClasses,
  addClass,
  archiverClass,
  updateClass
};

export const getClasses = () => { 
  return dispatch => {
    let apiEndpoint = '/classes';
    classService.get(apiEndpoint)
      .then((response) => {
         dispatch(receiveClasses(response.data))
      }).catch((err) => {
       })
  };
}

const receiveClasses = (classes) => ({
  type: "FETECHED_ALL_CLASS",
  classes: classes,
});

export const addClass = (itemClass) => {
  return (dispatch) => {
    let apiEndpoint = '/classes/';
    classService.post(apiEndpoint, itemClass)
      .then(response => {
        let data = response.data;
        dispatch({ type: "ADD_CLASS", payload: data })
        alert('L\'ajout est effectué avec succès');
      })
      .catch(error => { throw (error) });
  };
};
export const archiverClass = (classItem) => {
   
  return (dispatch) => {
    let apiEndpoint = '/classes/' + classItem;
    classService.get(apiEndpoint)
      .then(response => {
        const item = response.data;
        classService.put(apiEndpoint, {
          name: item.name,
          level_class: item.level_class,
          student_id: item.students,
          professor_id: item.professors,
          establishment_id: item.establishment_id,
          capacity: item.capacity,
          start_date: item.start_date,
          end_date: item.end_date,
          description: item.description,
          students_number: item.students_number,
          status: false,
          id: classItem.id,

        })
          .then(response => {
            dispatch({ type: "REMOVE_CLASS", payload: response.data })

          })
          .catch(function (error) {
            alert('error')
          });

      })
  }
};

export const updateClass = (item) => {
  return (dispatch) => {
    let apiEndpoint = '/classes/' + item.id;

    classService.put(apiEndpoint, {
          name: item.name,
          level_class: item.level_class,
          student_id: item.student_id,
          professor_id: item.professor_id,
          establishment_id: item.establishment_id,
          capacity: item.capacity,
          start_date: item.start_date,
          end_date: item.end_date,
          description: item.description,
          students_number: item.students_number,
    })
      .then(response => {
        dispatch({ type: "UPDATE_CLASS", payload: response.data })
      }).catch(error => { throw (error) });
  };
};
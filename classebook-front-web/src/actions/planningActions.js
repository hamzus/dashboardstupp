
import { classService } from '../_services';
import { ADD_EVENT, FETECHED_ALL_EVENTS, REMOVE_EVENT, UPDATE_EVENT } from "../constants/ActionTypes";
import { array } from 'prop-types';


export const PlanningAction = {
  getEvents,
  addEvent,
  archiverEvent,
  updateEvent
};

export const getEvents = () => {
  return dispatch => {
    let apiEndpoint = '/generic_events';
    classService.get(apiEndpoint)
      .then((response) => {
        // const events = response.data.map(event => {
        //   const container = {};

        //   container['start'] = new Date(event.start_date_generic_event);
        //   container['end'] = new Date(event.end_date_generic_event);
        //   container['title'] = event.frequency;


        //   return container;
        // });
        // console.log(events)

        dispatch(receiveEvents(response.data))

      }).catch((err) => {
        console.log(err);
      })
  };
}

const receiveEvents = (events) => ({
  type: FETECHED_ALL_EVENTS,
  payload: events,
});

export const addEvent = (itemClass) => {
  
  console.log(itemClass)
  return (dispatch) => {
    let apiEndpoint = '/generic_events';
    classService.post(apiEndpoint, itemClass)
      .then(response => {
        let data = response.data;
        dispatch({ type: ADD_EVENT, payload: data })
      })
      .catch(error => { throw (error) });
  };
};
export const archiverEvent = (classItem) => {
  console.log(classItem)
  return (dispatch) => {
    let apiEndpoint = '/generic_events/' + classItem.id;
    classService.get(apiEndpoint)
      .then(response => {
        console.log(response)
        const item = response.data;
        classService.put(apiEndpoint, {
          name: item.name,
          level_class: item.level_class,
          student_id: item.students,
          professor_id: item.professors,
          establishment_id: item.establishment_id,
          capacity: item.capacity,
          start_date: item.start_date,
          end_date: item.end_date,
          description: item.description,
          students_number: item.students_number,
          status: false,
          id: classItem.id,
          event_type:classItem.event_type,
          subject_id: classItem.subject,


        })
          .then(response => {
            dispatch({ type: REMOVE_EVENT, payload: classItem })

          })
          .catch(function (error) {
            alert('error')
          });

      })
  }
};

export const updateEvent = (item) => {
  return (dispatch) => {
    let apiEndpoint = '/generic_events/' + item.id;
    const { name, level_class, establishment, capacity, start_date, end_date, description, students_number, student_id, professor_id } = item;

    classService.put(apiEndpoint, {
      title: item.title,
      level_class: item.level_class,
      student_id: item.student_id,
      professor_id: item.professor_id,
      establishment_id: item.establishment_id,
      capacity: item.capacity,
      start_date: item.start_date,
      end_date: item.end_date,
      description: item.description,
      students_number: item.students_number,
    })
      .then(response => {
        dispatch({ type: UPDATE_EVENT, payload: response.data })
      }).catch(error => { throw (error) });
  };
};
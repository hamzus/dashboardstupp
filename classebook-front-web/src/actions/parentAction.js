
import baseUrl from 'config/config';

export function getParents() {
    return function (dispatch) {
        return fetch(`${baseUrl.baseUrl}/parents`)
        .then(response => response.json())
        .then(json => {                
                dispatch({ type: "GET_ALL_PARENT", payload: json });
            });
    };
};

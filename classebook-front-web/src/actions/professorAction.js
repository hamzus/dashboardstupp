
import baseUrl from 'config/config';
import axios from 'axios';


export function getData() {
    return function (dispatch) {
        return fetch(`${baseUrl.baseUrl}/professors`)
            .then(response => response.json())
            .then(json => {
                const professorList = json.filter(element => element.status);
                dispatch({ type: "DATA_LOADED", payload: professorList });
            });
    };
};

export function addData(professor) {
    return function (dispatch) {

        axios.post(`${baseUrl.baseUrl}/Users`, {
            realm: professor.name,
            username: professor.surname,
            password: professor.surname + '123',
            email: professor.address_mail,
            emailVerified: true,
        }).then(res => {
            sendMailActivation(res.data.id);


            axios.post(`${baseUrl.baseUrl}/profiles`, {
                user_id: res.data.id,
                role_id: 3,
                establishment_id: professor.establishment_id
            }).then(res => {
                axios.post(`${baseUrl.baseUrl}/professors`, {
                    name: professor.name,
                    surname: professor.surname,
                    gender: professor.gender,
                    date_of_birth: professor.date_of_birth,
                    date_start_contract: professor.date_start_contract,
                    date_end_contract: professor.date_end_contract,
                    address_mail: professor.address_mail,
                    phone: professor.phone,
                    cin: professor.cin,
                    status: true,
                    subject_id: professor.subject_id,
                    profile_id: res.data.id
                }).then(response => {
                    dispatch({ type: "ADD_PROFESSOR", payload: response.data });
                    alert('L\'ajout est effectué avec succès');
                }).catch(error => {
                    console.log(error)
                });

            }).catch(function (error) {

            });

        }).catch(function (error) {

        });
    };

}
async function sendMailActivation(id) {

    var x = await axios.get(`${baseUrl.baseUrl}/users/mailcreation/` + id).then(res => {
    })
    return x
    return

}
export function deleteData(idItem) {

    return function (dispatch) {
        axios.get(`${baseUrl.baseUrl}/professors/` + idItem)
            .then(res => {
                const dataProfessor = res.data;
                axios.put(`${baseUrl.baseUrl}/professors/` + dataProfessor.id, {
                    name: dataProfessor.name,
                    surname: dataProfessor.surname,
                    gender: dataProfessor.gender,
                    date_of_birth: dataProfessor.date_of_birth,
                    date_start_contract: dataProfessor.date_start_contract,
                    date_end_contract: dataProfessor.date_end_contract,
                    address_mail: dataProfessor.address_mail,
                    phone: dataProfessor.phone,
                    cin: dataProfessor.cin,
                    status: false,
                    profile_id: dataProfessor.profile_id,
                    subject_id: dataProfessor.subject_id
                })
                    .then(response => {
                        dispatch({ type: "DELETE_PROFESSOR", payload: response.data });
                    })
                    .catch(function (error) {
                        alert('error')
                    });
            });
    };

}


export function editData(data) {

    return function (dispatch) {
        axios.get(`${baseUrl.baseUrl}/professors/` + data.id)
            .then(res => {
                axios.put(`${baseUrl.baseUrl}/professors/` + data.id, {
                    name: data.name,
                    surname: data.surname,
                    gender: data.gender,
                    date_of_birth: data.date_of_birth,
                    date_start_contract: data.date_start_contract,
                    date_end_contract: data.date_end_contract,
                    address_mail: data.address_mail,
                    phone: data.phone,
                    cin: data.cin,
                    status: true,
                    profile_id: res.data.profile_id,
                    subject_id: data.subject_id,
                    id: data.id,
                })
                    .then(response => {
                        dispatch({ type: "EDIT_PROFESSOR", payload: response.data });
                    })
                    .catch(function (error) {
                        alert('error')
                    });
            });


    };

}
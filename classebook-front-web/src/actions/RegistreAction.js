
import { callService } from '../_services';
import { GET_ALL_CallRegister } from "../constants/ActionTypes";

export const callRegisterAction = {
  getCallRegister
};

export const getCallRegister = () => {
  return dispatch => {
    let apiEndpoint = '/call_registers';
    callService.get(apiEndpoint).then(
      (response) => {
        dispatch({ type: GET_ALL_CallRegister, payload: response.data })
      }).catch((err) => {
        console.log(err);
      })
  };
};
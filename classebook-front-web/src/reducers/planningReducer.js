import { ADD_EVENT, FETECHED_ALL_EVENTS, REMOVE_EVENT, UPDATE_EVENT } from "../constants/ActionTypes";

const initialState = {
  events: []
};

// export default function(state = initialState, action) {

//     if (action.type === DATA_CLASSES_LOADED) {
//       return Object.assign({}, state, {
//         remoteClasses: state.remoteClasses.concat(action.payload)
//       });
//     }
//     return state;

//   };
export default function planningReducer(state = initialState, action) {

  switch (action.type) {
    case FETECHED_ALL_EVENTS:
        console.log(action.planning)
        return Object.assign({}, state, {
            events: state.events.concat(action.payload)
          });
    case ADD_EVENT:
      return Object.assign({}, state.events, {
        events: [
          ...state.events,action.payload
        ]
      })
    case REMOVE_EVENT:
      console.log(action.payload.id)
      return state.filter((classe) => classe.id !== action.payload.id);

    case UPDATE_EVENT:
      return state.map((classItem) => {
        const { id, professors, students, name, niveau, establishment, capacity, start_date, end_date, description } = action.payload;
        if (classItem.id === action.payload.id) {
          return {
            ...classItem,
            name: action.payload.name,
            level_class: action.payload.level_class,
            student_id: action.payload.student_id,
            professor_id: action.payload.professor_id,
            establishment_id: action.payload.establishment_id,
            capacity: action.payload.capacity,
            start_date: action.payload.start_date,
            end_date: action.payload.end_date,
            description: action.payload.description,
            students_number: action.payload.students_number,
          }
        } else return classItem;
      })
    default:
      return state
  }
}


import {
  GET_ALL_STUDENT
 } from "../constants/ActionTypes";

 const initialState = {
  students: [],
  remoteStudents: [],
};

export default function (state = initialState, action) {
  if (action.type === GET_ALL_STUDENT) {
      return Object.assign({}, state, {
          remoteStudents: action.payload
      });
  }
  return state;
};



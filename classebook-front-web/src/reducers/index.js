import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router'
import Settings from './Settings';
import Auth from './Auth';
import establishmentReducer from './establishmentReducer';
import StudentReducer from './StudentReducer';
import professorReducer from './professorReducer';
import classeReducer  from './classeReducer';
import ModuleReducer from './ModuleReducer';
import planningReducer from './planningReducer';
import ClassLevel from './ClassLevel';


import Mail from './Mail';
import ToDo from './ToDo';
import gradeReducer from './gradeReducer';
import callRegisterReducer from './callRegisterReducer';


import subjectReducer from './subjectReducer';
import examReducer from './examReducer';
import presenceReducer from './presenceReducer';
import parentReducer from './parentReducer';



export default (history) => combineReducers({
  router: connectRouter(history),
  settings: Settings,
  auth: Auth,
  establishment: establishmentReducer,
  student: StudentReducer,
  professor: professorReducer,
  mail: Mail,
  toDo: ToDo,
  classes: classeReducer,
  subject: subjectReducer,
  grade: gradeReducer,
  callRegister: callRegisterReducer,
  module: ModuleReducer,
  planning : planningReducer,
  exam: examReducer,
  ClassLevels : ClassLevel,
  presence : presenceReducer,
  parent: parentReducer
});

import { ADD_CLASS, DATA_CLASSES_LOADED } from "../constants/ActionTypes";

const initialState = {
  ClassLevels: []
};


export default function classeReducer(state = initialState, action) {
  switch (action.type) {
    case 'FETECHED_ALL_LEVEL':
      return action.payload;
    case 'ADD_LEVEL':
    console.log(action.payload)
      return [action.payload, ...state];
    case 'REMOVE_LEVEL':
      console.log(action.payload.id)
      return state.filter((item) => item.id !== action.payload.id);

    case 'UPDATE_LEVEL':
      return state.map((item) => {
        const { id, professors, students, name, niveau, establishment, capacity, start_date, end_date, description } = action.payload;
        if (item.id === action.payload.id) {
          return {
            ...item,
            name: action.payload.name,
            description: action.payload.description,
            category: action.payload.category,
            level: action.payload.level,
            section: action.payload.section,
        
          }
        } else return item;
      })
    default:
      return state
  }
}


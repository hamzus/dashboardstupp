
import { ADD_ESTABLISHMENT, GET_ALL_ESTABLISHMENT, DELETE_ESTABLISHMENT, EDIT_ESTABLISHMENT } from "../constants/ActionTypes";

const initialState = {
    establishments: [],
    remoteEstablishments: []
};

export default function (state = initialState, action) {
     
    if (action.type === GET_ALL_ESTABLISHMENT) {
      return Object.assign({}, state, {
        remoteEstablishments: action.payload
      });
  }


    if (action.type === ADD_ESTABLISHMENT) {
        return Object.assign({}, state, {
          remoteEstablishments: state.remoteEstablishments.concat(action.payload)
        });
    }

    if (action.type === DELETE_ESTABLISHMENT) {
      return Object.assign({}, state, {
        remoteEstablishments: [...state.remoteEstablishments.filter(element => element.id != action.payload.id)]
      });
   
  }

  if (action.type === EDIT_ESTABLISHMENT) {
      return Object.assign({}, state, {
        remoteEstablishments: [...state.remoteEstablishments.filter(element => element.id != action.payload.id), action.payload]
      });
  }

    return state;

};


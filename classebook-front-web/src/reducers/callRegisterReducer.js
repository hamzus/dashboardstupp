
import { GET_ALL_CallRegister } from "../constants/ActionTypes";

const initialState = {
    calls: []
};

export default function (state = initialState, action) {

    switch (action.type) {
        case GET_ALL_CallRegister:
            return Object.assign({}, state, {
                calls: action.payload
            });
        default:
            return state;
    }
}
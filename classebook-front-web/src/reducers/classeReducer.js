import { ADD_CLASS, DATA_CLASSES_LOADED } from "../constants/ActionTypes";

const initialState = {
  classes: []
};

export default function classeReducer(state = initialState, action) {
  switch (action.type) {
    case 'FETECHED_ALL_CLASS':
      return action.classes;
    case 'ADD_CLASS':
      return [action.payload, ...state];
    case 'REMOVE_CLASS':
      return state.filter((classe) => classe.id !== action.payload.id);

    case 'UPDATE_CLASS':
      return state.map((classItem) => {
        if (classItem.id === action.payload.id) {
          return {
            ...classItem,
            name: action.payload.name,
            level_class: action.payload.level_class,
            student_id: action.payload.student_id,
            professor_id: action.payload.professor_id,
            establishment_id: action.payload.establishment_id,
            capacity: action.payload.capacity,
            start_date: action.payload.start_date,
            end_date: action.payload.end_date,
            description: action.payload.description,
            students_number: action.payload.students_number,
          }
        } else return classItem;
      })
    default:
      return state
  }
}


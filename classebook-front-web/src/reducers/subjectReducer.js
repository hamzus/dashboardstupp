import { DATA_LOADED_SUBJECT, ADD_SUBJECT, DELETE_SUBJECT, EDIT_SUBJECT } from "../constants/ActionTypes";

const initialState = {
    subjects: [],
    remoteSubjects: []
};

export default function (state = initialState, action) {
    
    if (action.type === DATA_LOADED_SUBJECT) {
        return Object.assign({}, state, {
            remoteSubjects: action.payload
        });
    }

    if (action.type === ADD_SUBJECT) {
        return Object.assign({}, state, {
            remoteSubjects: state.remoteSubjects.concat(action.payload)
        });
    }

    if (action.type === DELETE_SUBJECT) {
        return Object.assign({}, state, {
            remoteSubjects: [...state.remoteSubjects.filter(element => element.id != action.payload.id)]
        });
     
    }

    if (action.type === EDIT_SUBJECT) {
        return Object.assign({}, state, {
            remoteSubjects: [...state.remoteSubjects.filter(element => element.id != action.payload.id), action.payload]
        });
    }
    return state;

};


import React from 'react';
import IntlMessages from 'util/IntlMessages';
import ContainerHeader from 'components/ContainerHeader/index';
import PaymentList from './PaymentList';

class Payment extends React.Component {
  render() {

    return (
      <div className="app-wrapper">
        <ContainerHeader match={this.props.match} title={<IntlMessages id="payment.title" />} />
        <PaymentList />
      </div>
    );
  }
}

export default Payment;
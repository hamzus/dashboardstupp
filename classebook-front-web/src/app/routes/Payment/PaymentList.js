import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import IntlMessages from 'util/IntlMessages';
import CardBox from 'components/CardBox/index';
import {Badge} from 'reactstrap';
import img from '../../../assets/images/3m.ico';
import img2 from '../../../assets/images/visa.ico';
import img3 from '../../../assets/images/master.ico';

class PaymentList extends React.Component{
 constructor(props){
    super(props)
    this.state = {

    }
 }

 render(){
     return(
         <div className="row mb-md-3">
             <CardBox styleName="col-12" cardStyle="p-0" headerOutside>
                <div className="table-responsive-material">
                  <Table className="default-table table-unbordered table table-sm table-hover">
                    <TableHead className=" bg-primary " >
                      <TableRow >
                        <TableCell className="text-white">{<IntlMessages id="name.and.surname" />}</TableCell>
                        <TableCell className="text-white" >{<IntlMessages id="methode.of.payment" />}</TableCell>
                        <TableCell className="text-white">{<IntlMessages id="payment.amount" />}</TableCell>
                        <TableCell className="text-white">{<IntlMessages id="state.of.payment" />}</TableCell>
                        <TableCell className="text-white">{<IntlMessages id="service.to pay" />}</TableCell>
                        <TableCell className="text-white">{<IntlMessages id="payment.date" />}</TableCell>
                       
                      </TableRow>
                    </TableHead>
                    <TableBody>

                      <TableRow  >
                        <TableCell>Aymen ben slama</TableCell>
                        <TableCell ><img src={img2} /></TableCell>
                        <TableCell >50TND</TableCell>
                        <TableCell > <Badge color="success" pill>Payé</Badge></TableCell>
                        <TableCell >Cantine</TableCell>
                        <TableCell >25/06/2019</TableCell>
                      </TableRow>

                      <TableRow  >
                        <TableCell>Amel Bannour</TableCell>
                        <TableCell >  <img  src={img} /></TableCell>
                        <TableCell >20TND</TableCell>
                        <TableCell ><Badge color="danger" pill>Expiré</Badge></TableCell>
                        <TableCell >Bus</TableCell>
                        <TableCell >03/06/2019</TableCell>
                      </TableRow>

                      <TableRow  >
                        <TableCell>Karim Ouni</TableCell>
                        <TableCell ><img  src={img3} /></TableCell>
                        <TableCell >70TND</TableCell>
                        <TableCell ><Badge color="warning" pill>Expiré</Badge></TableCell>
                        <TableCell >Bus</TableCell>
                        <TableCell >16/06/2019</TableCell>
                      </TableRow>

                      <TableRow  >
                        <TableCell>Ahlem Allani</TableCell>
                        <TableCell ><img src={img2} /></TableCell>
                        <TableCell >50TND</TableCell>
                        <TableCell > <Badge color="success" pill>Payé</Badge></TableCell>
                        <TableCell >Cantine</TableCell>
                        <TableCell >07/06/2019</TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                </div>
              </CardBox>
         </div>
     )

 }

}

export default PaymentList
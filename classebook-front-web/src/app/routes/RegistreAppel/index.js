import React from 'react';
import Registre from './Registre';
import { Redirect, Route, Switch } from 'react-router-dom';


const RegistreAppel = ({ match }) => (
  <div className="app-wrapper">
    <Switch>
      {/* <Redirect exact from={`${match.url}/`} to={`${match.url}`} /> */}
      <Route path={`${match.url}`} component={Registre} />


    </Switch>
  </div>

);

export default RegistreAppel; 
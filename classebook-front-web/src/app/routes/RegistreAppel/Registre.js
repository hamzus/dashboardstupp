import React from 'react';
import CallRegister from './CallRegister';
import { Component } from 'react';
import { connect } from "react-redux";
import { getEvents } from "../../../actions/planningActions";
import ContainerHeader from 'components/ContainerHeader/index';
import IntlMessages from '../../../util/IntlMessages';

const mapStateToProps = (state) => {
  return {
    events: state.planning.events
  }
}

class Registre extends Component {
  constructor(props) {
    super(props);
    this.state = {
      events: []
    }
  }
  componentDidMount() {
    this.props.dispatch(getEvents());
   
  }

  componentWillUnmount() {

  }

  render() {
   
    return (
      <div >
      <ContainerHeader title={<IntlMessages id="sidebar.customViews.studentList" />} match={this.props.match} />
        <CallRegister eventsProps={this.props.events} />
      </div>

    );
  }

}
  export default connect(mapStateToProps)(Registre);
import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import IntlMessages from '../../../util/IntlMessages';
import baseUrl from '../../../config/config';
import axios from 'axios';
import _ from "lodash";
import { ON_MAIL_SEND } from '../../../constants/ActionTypes';
import moment from 'moment';

class ConsfirmAppel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      list_absent: [],
      list_present: [],
      call_register: [],
      listIdParentAbsStudent: [],
      event_id: 0,
      timeNow: moment().format()
    };

    this.sentNotificationAbsMail = this.sentNotificationAbsMail.bind(this);

  }

  componentDidMount() {
    this.setState({ open: true });

  }

  handleRequestClose = () => {
    this.props.cancelModalconfirm()
  };
  handleSave = (e) => {
    let event = this.props.event[0];
    let professorId = event.id_professor;
    let classId = event.id_class;

    var i = 0;
    this.props.cancelModalconfirm()

    const Lists_Absent = this.props.List_Absent;
    const List_Present = this.props.List_Present;

    Lists_Absent.map(student => {
      this.state.list_absent.push({
        "action": 'Absent',
        "id_student": student.id,
        "presence_id": 0
      })
    });
    List_Present.map(student => {
      this.state.list_present.push({
        "action": 'Present',
        "id_student": student.id,
        "presence_id": 0
      })
    });
    const call_register = this.state.list_absent.concat(this.state.list_present);

    let list_presences = [];
    list_presences = this.props.presences;
    let list_presenceIds = list_presences.map(presence => presence.student_id);

    call_register.map((studentItem) => {

      let updateStudent = list_presenceIds.find(idStudent => idStudent == studentItem.id_student)
      if (_.isFinite(updateStudent)) {
        axios.get(`${baseUrl.baseUrl}/presences/findOne?filter={"where":{"student_id":${studentItem.id_student}}}`)
          .then(response => {
            axios.patch(`${baseUrl.baseUrl}/presences/${response.data.id}`, {
              "type": studentItem.action,
              "student_id": studentItem.id_student
            })
              .then(response => {
                axios.post(`${baseUrl.baseUrl}/call_registers`, {
                  action: studentItem.action,
                  event_id: event.id,
                  id_student: studentItem.id_student,
                  dateHourStamp: this.state.timeNow,
                  presence_id: response.data.id
                })
                .then(res => {
                  i++;
                  if (i == call_register.length) {
                    alert("L'ajout de registre d'appel est effectué avec succès");
                    this.sentNotificationAbsMail();
                  }
                }).catch(function (error) {
                }).catch((err) => { console.log(err); })
              }).catch((err) => { console.log(err); })
          }).catch((err) => { console.log(err); })
      } else {
        axios.post(`${baseUrl.baseUrl}/presences`, {
          "type": studentItem.action,
          "student_id": studentItem.id_student
        })
          .then(response => {
            console.log(response)
            axios.post(`${baseUrl.baseUrl}/call_registers`, {
              action: studentItem.action,
              event_id: event.id,
              id_student: studentItem.id_student,
              dateHourStamp: this.state.timeNow,
              presence_id: response.data.id
            })
            .then(res => {
              i++;
              if (i == call_register.length) {
                alert("L'ajout de registre d'appel est effectué avec succès");
                this.sentNotificationAbsMail();
              }
            }).catch(function (error) {
            })
            .catch((err) => { console.log(err); })
          }).catch((err) => { console.log(err); })
      }
    })


  }


  sentNotificationAbsMail() {

    const listIdAbsStudent = this.state.list_absent.map((element) => element.id_student);
    listIdAbsStudent.map((element) => {
      axios.get(`${baseUrl.baseUrl}/profiles?filter[where][user_id]=` + localStorage.user_id)
        .then(res => {
          const senderProfileId = res.data[0].id;
          axios.get(`${baseUrl.baseUrl}/students/` + element)
            .then(res => {
              const receivedProfileId = res.data.profile_id;
              //////////////////////////////////////////////
              if (res.data.length !== 0) {
                axios.post(`${baseUrl.baseUrl}/mails`, {
                  sender_id: senderProfileId,
                  receiver_id: receivedProfileId,
                  subject: 'absence éléve!',
                  message: 'vous étés absent!',
                  profile_id: senderProfileId,
                })
              }
              ///////////////////////////////////////////////
              axios.get(`${baseUrl.baseUrl}/students/` + element + `/parent`)
                .then(res => {
                  if (res.data.length !== 0) {
                    axios.post(`${baseUrl.baseUrl}/mails`, {
                      sender_id: senderProfileId,
                      receiver_id: res.data.profile_id,
                      subject: 'absence de votre enfant!!',
                      message: 'On vous informe que votre enfant est absent!!',
                      profile_id: senderProfileId,
                    })
                  }
                })
              ///////////////////////////////////////////////////////////
            })
        })
    });

  }
  render() {
    return (
      <div>
        <Dialog open={this.state.open} TransitionComponent={Slide} onClose={this.handleRequestClose}>
          <DialogTitle>
            Nombre de présence est: {this.props.NumberPresent} <br />
            Nombre d'absence est: {this.props.NumberAbsent}
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              Voulez vous entregistrez la présence ?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleRequestClose} color="secondary">
              {<IntlMessages id="button.no" />}
            </Button>
            <Button onClick={this.handleSave} color="primary">
              {<IntlMessages id="button.yes" />}
            </Button>
          </DialogActions>
        </Dialog>
      </div >
    );
  }
}

export default ConsfirmAppel;
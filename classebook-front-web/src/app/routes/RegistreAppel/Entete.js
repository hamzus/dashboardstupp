import React, { Component } from 'react';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AppBar from '@material-ui/core/AppBar';
import IntlMessages from '../../../util/IntlMessages';
import moment from 'moment';
import 'moment/locale/fr';

class Entete extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new moment().format("LLL"),
            user: JSON.parse(localStorage.getItem('user'))
        }
    }
    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            60000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick = () => {
        this.setState({
            date: new moment().format("LLL")
        });
    }
    render() {
        return (
            <div>
                <AppBar className="bg-primary" position="static">
                    <Tabs onChange={this.handleChange} variant="scrollable" variant="fullWidth" scrollButtons="on">
                        <Tab className="tab" label={<IntlMessages id="components.class.formadd.name" />} />
                        <Tab className="tab" label={<IntlMessages id="components.establishments.formadd.number_students" />} />
                        <Tab className="tab" label={this.state.date} />
                        <Tab className="tab" label={<IntlMessages id="components.entete.textfields" />} />
                    </Tabs>
                </AppBar>
            </div>
        );
    }
}
export default Entete;
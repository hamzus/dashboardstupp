import React, { Component } from 'react';
import ContainerHeader from '../../../components/ContainerHeader/index';
import Entete from './Entete';
import StudentList from './StudentList';
import IntlMessages from '../../../util/IntlMessages';
import ConsfirmAppel from './ConfirmAppel';
import { UncontrolledAlert } from 'reactstrap';
import moment from 'moment';
import axios from 'axios';
import baseUrl from 'config/config';
import memoize from 'memoize-one';
import _ from 'lodash';
import { connect } from 'react-redux';
import { getPresence } from "../../../actions/presenceAction";

class CallRegister extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            alert_success: false,
            modal_save: false,
            event_id: 0,
            nbre_Absent: 0,
            nbre_Present: 0,
            list_absent: [],
            list_present: [],
            Props: [],
            event: [],
            timeNowStateUtc: moment().utc().format(),
            currentUser: 0,
            list_passes: [],
            list_presences: []

        };

        this.RequestSaveRegister = this.RequestSaveRegister.bind(this);
        this.CancelModalconfirm = this.CancelModalconfirm.bind(this);
        this.NumberAbsentPresent = this.NumberAbsentPresent.bind(this);
        this.filterMemoize = this.filterMemoize.bind(this);
    }

    filterMemoize = memoize(
        (eventsProps, currentUser) => eventsProps.filter(event => event.id_professor == currentUser)
    );

    componentDidMount() {

        this.props.getPresence();

        axios.get(`${baseUrl.baseUrl}/absent_pass`)
            .then(res => {
                const list_passes = res.data;
                this.setState({ list_passes: list_passes });
            })

        const currentProfileId = parseInt(localStorage.profileId);
        axios.get(`${baseUrl.baseUrl}/professors/findOne?filter={"where":{"profile_id":"${currentProfileId}"}}`)
            .then(res => {
                this.setState({ currentUser: res.data.id })
            })

    }


    NumberAbsentPresent(nbre_Absent, nbre_Present, list_absent, list_present) {
        this.setState({ nbre_Present, nbre_Absent, list_absent, list_present });
    }

    CancelModalconfirm() {
        this.setState({ modal_save: false, event_id: 0, nbre_Present: 0, nbre_Absent: 0 })
    }
    RequestSaveRegister(id) {
        this.setState({ modal_save: true, event_id: id });
    }
    render() {
        const eventsByProf = this.filterMemoize(this.props.eventsProps, this.state.currentUser);
        let currentEvent = [];
        _.isEmpty(eventsByProf) ? currentEvent = []
            : currentEvent = eventsByProf.filter(event => 
                moment(this.state.timeNowStateUtc).isBetween(moment(event.start_date_generic_event), moment(event.end_date_generic_event)));

        const roleId = parseInt(localStorage.roles_id); //RoleId de prof=3

        let currentEventProf_Id = 0;
        _.isEmpty(currentEvent) ? currentEventProf_Id = 0 : currentEventProf_Id = currentEvent[0].id_professor;

        return (
            <div>

                {(() => {
                    if (currentEventProf_Id === this.state.currentUser && currentEvent.length) {
                        return (
                            <div>
                                {this.state.alert_success ? <UncontrolledAlert className="alert-addon-card bg-success bg-success text-white shadow-lg">
                                    <span className="icon-addon alert-addon">
                                        <i className="zmdi zmdi-cloud-done zmdi-hc-fw zmdi-hc-lg" />
                                    </span>
                                    <span className="d-inline-block"> {<IntlMessages id="message.success" />} </span>
                                </UncontrolledAlert> : ''}
                                <Entete />
                                <br /> <br />

                                <StudentList
                                    currentEvent={currentEvent[0]}
                                    list_passes={this.state.list_passes}
                                    presences={this.props.presences}
                                    RequestSaveRegister={this.RequestSaveRegister}
                                    NumberAbsentPresent={this.NumberAbsentPresent}
                                />
                                {this.state.modal_save ?
                                    <ConsfirmAppel
                                        event={currentEvent}
                                        cancelModalconfirm={this.CancelModalconfirm}
                                        NumberAbsent={this.state.nbre_Absent}
                                        NumberPresent={this.state.nbre_Present}
                                        presences={this.props.presences}
                                        List_Absent={this.state.list_absent}
                                        List_Present={this.state.list_present} /> : ''}
                            </div>
                        )
                    } else if (roleId == 3) {

                        return (
                            <div>
                                <h2 >{`Accés Autorisée mais vous n'avez pas de séance pour maintenant`}</h2>
                            </div>
                        )
                    }
                })()}
                {(() => {
                    if (roleId != 3) {
                        return (
                            <div>
                                <h2 >{`Accés Refusé , seul le role Professeur peut faire l'appel`}</h2>
                            </div>
                        )
                    }
                })()}

            </div>
        )
    }

}

function mapStateToProps(state) {
    return {
        presences: state.presence.remotePresences
    }
}


export default connect(mapStateToProps, { getPresence })(CallRegister);
import React, { Component } from 'react';
import StudentItem from './StudentItem';
import Button from '@material-ui/core/Button';
import IntlMessages from '../../../util/IntlMessages';
import { connect } from 'react-redux';
import { getStudents } from "../../../actions/studentAction";
import 'react-confirm-alert/src/react-confirm-alert.css'
import _ from 'lodash';
import { getPresence } from "../../../actions/presenceAction";


class StudentList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nbre_Absent: 0,
            nbre_Present: 0,
            filterStudentsByClass: [],
            ListStudentAbsent: [],
            ListStudentPresent: [],
            modal: false,
            list_absent: [],
            list_present: []
        }
        this.handleClickAbsent = this.handleClickAbsent.bind(this);
        this.handleClickPresent = this.handleClickPresent.bind(this);
        this.handleRequestSave = this.handleRequestSave.bind(this);
    }
    handleRequestSave(e) {
        e.preventDefault();
        this.props.RequestSaveRegister(e.currentTarget.value);
        this.state.nbre_Absent = this.state.ListStudentAbsent.length;
        this.state.nbre_Present = this.state.ListStudentPresent.length;

        this.state.list_absent = this.state.ListStudentAbsent;
        this.state.list_present = this.state.ListStudentPresent;
        this.props.NumberAbsentPresent(this.state.nbre_Absent, this.state.nbre_Present, this.state.list_absent, this.state.list_present);
        this.setState({ ListStudentAbsent: [], ListStudentPresent: [] });
    }

    componentDidMount() {
        // L'appel de toute la liste des éleves du store 
        this.props.getStudents();
    }

    handleClickAbsent(data) {

        let found = this.state.ListStudentAbsent.find(element => element === data);
        let found2 = this.state.ListStudentPresent.find(element => element === data);
        if (found2) {
            this.setState({
                ListStudentPresent: [...this.state.ListStudentPresent.filter(element => element.id != data.id)],
            })
        }
        if (!found) {
            this.setState(state => {
                const ListStudentAbsent = state.ListStudentAbsent.push(data);
            });
        }
    }

    handleClickPresent(data) {
        let found2 = this.state.ListStudentAbsent.find(element => element === data);
        let found = this.state.ListStudentPresent.find(element => element === data);

        if (found2) {
            this.setState({
                ListStudentAbsent: [...this.state.ListStudentAbsent.filter(element => element.id != data.id)],
            })
        }

        if (!found) {
            this.setState(state => {
                const ListStudentPresent = state.ListStudentPresent.push(data);
            });

        }
    }
    render() {
        let currentEvent = this.props.currentEvent;
        let currentEventClass = 0;
        let filterStudentsByClass = [];

        let list_passes = [];
        list_passes = this.props.list_passes;

        let list_presences = [];
        list_presences = this.props.presences;

        _.isEmpty(currentEvent) ? currentEventClass = 0 : currentEventClass = currentEvent.id_class;

        _.isEmpty(currentEvent) ? console.log("filtrage INCOMPLETE (check student has class_id)")
            : filterStudentsByClass = this.props.students.filter(student => student.class_id === currentEventClass);

        let list_passesIds = list_passes.map(pass => pass.student_id);

        let filterStudentsByPresenceAndPass = filterStudentsByClass.map((studentItem) => {

            let studentPresence = list_presences.find(presence => presence.student_id == studentItem.id)
            let studentPass = list_passes.find(pass => pass.student_id == studentItem.id && pass.status)
            let absentEarlier = false;

            if (!(_.isEmpty(studentPresence)) && !(_.isEmpty(studentPass))) {
                (studentPresence.type == "Absent") ? absentEarlier = true : absentEarlier = false;
                return {
                    studentInfo: studentItem,
                    absentEarlier: absentEarlier,
                    studentPass: studentPass,
                    hasPass: studentPass.status
                }
            } else if (!(_.isEmpty(studentPresence))) {
                (studentPresence.type == "Absent") ? absentEarlier = true : absentEarlier = false;
                return {
                    studentInfo: studentItem,
                    absentEarlier: absentEarlier,
                    studentPass: [],
                    hasPass: false
                }
            } else {
                return {
                    studentInfo: studentItem,
                    absentEarlier: absentEarlier,
                    studentPass: [],
                    hasPass: false
                }
            }
        })

        let finalListStudents = filterStudentsByPresenceAndPass.filter(element => element)

        return (
            <div className="app-wrapper" >

                <div className="row" autoComplete="off">
                    <div className=" col-md-6 col-sm-6 col-xs-6 animated slideInUpTiny animation-duration-3">
                        {finalListStudents.map((data, index) => (

                            <StudentItem
                                key={data.id}
                                data={data}
                                handleClickAbsent={this.handleClickAbsent}
                                handleClickPresent={this.handleClickPresent}
                                styleName="card shadow "
                            />
                        ))}
                    </div>
                </div>

                <Button variant="contained" color="primary" className="jr-btn text-white" style={{ position: "relative", float: 'right', marginBottom: 10 + 'px' }}
                    onClick={this.handleRequestSave}
                ><IntlMessages id="button.save.registreAppel" /></Button>
            </div>
        )
    }
}
function mapStateToProps(state) {
    return {
        students: state.student.remoteStudents,

    }
}

export default connect(mapStateToProps, { getStudents, getPresence })(StudentList); 
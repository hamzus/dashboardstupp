import React, { Component } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import { ButtonGroup } from 'reactstrap';
import IntlMessages from '../../../util/IntlMessages';
import _ from 'lodash';
import { Badge } from 'reactstrap';
import baseUrl from '../../../config/config';
import axios from "axios";

function jsUpperCaseFirst(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

class StudentItem extends Component {
  constructor(props) {
    super(props);
    this.state = {}
    this.handeBilletNonValid = this.handeBilletNonValid.bind(this);
  }

  handeBilletNonValid(studentPass) {
    axios.patch(`${baseUrl.baseUrl}/absent_pass/${studentPass.id}`, {
      "status": false,
      "id": studentPass.id,
    })
  }

  render() {
    const { key, styleName, data, handleClickAbsent, handleClickPresent } = this.props;
    const { image, name, surname, id } = data.studentInfo;
    const { hasPass, studentPass, absentEarlier } = data;
    const absentLabel = absentEarlier ? hasPass ? "Cet éleve a un Billet" : "Cet éleve est Absent " : "Cet éleve est présent";
    const hasPassLabel = hasPass ? "Cet éleve a un Billet" : "Cet éleve est Absent ";
    const color = absentEarlier ? "danger" : "success";
    return (
      <div className={`user-list d-flex flex-row  ${styleName}`}>
        <Avatar
          alt='...'
          src={require('../../../assets/images/placeholder.jpg')}
          className="user-avatar avatar-shadow"
        />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <div className="description " >
          <h5>{name.toUpperCase()} {jsUpperCaseFirst(surname)}</h5>
          {(() => {
            if (data.absentEarlier && data.hasPass) {
              return (
                <Badge className="d-block font-weight-bold" color={color} >{absentLabel}</Badge>
              )
            } else if (data.absentEarlier && !(data.hasPass)) {
              return (
                <Badge className="d-block font-weight-bold" color={color} >{absentLabel}</Badge>
              )
            } else {
              return (
                <Badge className="d-block font-weight-bold" color={color} >{absentLabel}</Badge>
              )
            }
          })()}
          <div className="description">
            <div className="list-inline d-sm-flex flex-sm-row jr-mbtn-list">
              <Button variant="contained" color="primary" className="jr-btn text-white"
                onClick={((e) => {
                  if (data.absentEarlier && data.hasPass) {
                    this.handeBilletNonValid(studentPass)
                  }
                  handleClickPresent(data.studentInfo)
                })}
              >
                <IntlMessages id="button.present" />
              </Button>
              <Button className="jr-btn bg-secondary text-white"
                onClick={((e) => {
                  if (data.absentEarlier && data.hasPass) {
                    this.handeBilletNonValid(studentPass)
                  }
                  handleClickAbsent(data.studentInfo)
                })}
              >
                <IntlMessages id="button.absent" />
              </Button>
            </div>
            {(() => {
              if (data.hasPass) {
                const reasonPass = data.studentPass.reason
                return (
                  <Link
                    component="button"
                    variant="body2"
                    onClick={() => {
                      alert(`Motif d'absence : ${reasonPass}`);
                    }}
                  >
                    Voir Billet
                  </Link>
                )
              }
            })()}
          </div>

        </div>

      </div>
    );
  }
}


export default StudentItem;

import React from 'react';
import Input from '@material-ui/core/Input';
import IntlMessages from 'util/IntlMessages';
import CardBox from 'components/CardBox';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';
import axios from 'axios';
import baseUrl from 'config/config';
import { connect } from "react-redux";
import { getGrades } from "../../../../../actions/gradeAction"

import { Formik, Form, Field } from 'formik';

export class GradeList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      gradeList: [],
      studentList: [],

    };
  }

  componentWillMount() {
    axios.get(`${baseUrl.baseUrl}/students`)
      .then(res => {
        const ListStudents = res.data;
        this.setState({ studentList: ListStudents });
      });
  }

  render() {

    return (
      <CardBox styleName="col-lg-12" >
        <div className="col-sm-12">
          <h1>{<IntlMessages id="component.note.list.student" />}</h1>
        </div>
        <div className="table-responsive-material">
          <Formik
            initialValues={{}}
            onSubmit={(values) => {
              let array = [];
              for (let [key, value] of Object.entries(values)) {
                let object = {
                  "student_id": key,
                  "mark": value
                }
                array.push(object)
              }
              axios.post(`${baseUrl.baseUrl}/grades`, {
                "value": 0,
                "comment": "string",
                "grades_students": array,
                "class_id": this.props.classId,
                "exam_id": this.props.examId
              });
              setTimeout(() => {
                alert("Notes enregistrer avec Succés");
              }, 200);
            }}
            render={({ values }) => (
              <Form>
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell>{<IntlMessages id="components.note.student.id" />}</TableCell>
                      <TableCell align="right">{<IntlMessages id="components.note.student.name" />}</TableCell>
                      <TableCell align="right">{<IntlMessages id="components.note.class" />}</TableCell>
                      <TableCell align="right">{<IntlMessages id="components.note.student.markObtained" />}</TableCell>
                      <TableCell></TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {this.props.studentList.map(student => {
                      return (
                        <TableRow key={student.id} >
                          <TableCell align="right">{student.id}</TableCell>
                          <TableCell align="right">{student.name}</TableCell>
                          <TableCell align="right">{student.class_id}</TableCell>
                          <TableCell align="right">
                            <Field
                              key={student.id}
                              name={student.id}
                            />
                          </TableCell>
                        </TableRow>
                      )
                    }
                    )}

                  </TableBody>
                </Table>
                <Button size="large" color="primary" variant="text" type="submit" className="jr-btn bg-success text-white" >
                  {<IntlMessages id="note.button.save" />}
                </Button>
              </Form>
            )}
          />
        </div>
      </CardBox>
    )
  }
}

export default GradeList;

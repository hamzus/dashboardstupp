
import React from 'react';
import ContainerHeader from 'components/ContainerHeader/index';
import IntlMessages from 'util/IntlMessages';
import TextField from '@material-ui/core/TextField';
import CardBox from 'components/CardBox/index';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import ListGrade from './ListGrade';
import baseUrl from 'config/config';
import axios from 'axios';
import { connect } from "react-redux";
import { getGrades } from "../../../../../actions/gradeAction";
import { getSubject } from "../../../../../actions/subjectAction";
import { getExam } from "../../../../../actions/examAction";
// import { getStudent } from '../../../../../actions/studentAction';
import { getClasses } from "../../../../../actions/classeAction";

function mapStateToProps(state) {
	return {
		grades: state.grade.grades,
		subjects: state.subject.remoteSubjects,
		exams: state.exam.remoteExams,
		// students: state.
		classes: state.classes
	};
}

class Grades extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			gradesList: [],
			subjectsList: [],
			classesList: [],
			studentOrigin: [],
			studentList: [],
			examen: '',
			classe: '',
			niveau: '',
			subject: ''
		};
		this.handleChange = this.handleChange.bind(this);
		this.handleBlur = this.handleBlur.bind(this);
	}

	componentDidMount() {
		this.props.getGrades();
		this.props.getSubject();
		this.props.getExam();
		this.props.getClasses();
	};

	componentWillMount() {
		axios.get(`${baseUrl.baseUrl}/students`)
			.then(res => {
				const ListStudents = res.data;
				this.setState({
					studentOrigin: ListStudents,
					studentList: ListStudents
				});
			});
	}

	handleBlur = event => {
		this.setState({
			studentList: this.state.studentOrigin.filter(student =>
				student.class_id == this.state.classe
			)
		});
	}

	handleChange = name => event => {
		this.setState({
			[name]: event.target.value
		});
	};

	render() {
		const exams = Array.from(this.props.exams);
		const grades = Array.from(this.props.grades);
		const subs = Array.from(this.props.subjects);
		const classes = Array.from(this.props.classes)

		return (
			<div className="app-wrapper">
				<form noValidate autoComplete="off" onSubmit={this.handleBlur}>
					<div className="col-lg-12">
						<h1>{<IntlMessages id="component.note.info.general" />}</h1>
					</div>
					<div className="row">
						<CardBox heading={<IntlMessages id="component.grade.form.filter.class" />}>
							<div className="row">
								<div className="col-sm-3" align="right">
									<TextField
										required
										id="niveau"
										select
										label={<IntlMessages id="components.note.niveau" />}
										value={this.state.niveau}
										onChange={this.handleChange('niveau')}
										SelectProps={{}}
										margin="normal"
										fullWidth >
										{classes.map(option => (
											<MenuItem key={option.id} value={option.id}>
												{option.level_class}({option.id})
											</MenuItem>
										))}
									</TextField>
								</div>
								<div className="col-sm-3" align="right">
									<TextField
										required
										id="classe"
										select
										label={<IntlMessages id="components.note.class" />}
										value={this.state.classe}
										onChange={this.handleChange('classe')}
										onBlur={this.handleBlur}
										SelectProps={{}}
										margin="normal"
										fullWidth >
										{classes.map(option => (
											<MenuItem key={option.id} value={option.id}>
												{option.name}({option.id})
										</MenuItem>
										))}
									</TextField>
								</div>
							</div> 
						</CardBox>
						<CardBox
							heading={<IntlMessages id="component.grade.form.filter.exam" />}>
							<div className="row">
								<div className="col-sm-3" align="right">
									<TextField
										required
										id="examen"
										select
										label={<IntlMessages id="components.note.exam" />}
										value={this.state.examen}
										onChange={this.handleChange('examen')}
										SelectProps={{}}
										margin="normal"
										fullWidth >
										{exams.map(option => (
											<MenuItem key={option.id} value={option.id}>
												{option.type}({option.id})
										</MenuItem>
										))}
									</TextField>
								</div>
								<div className="col-sm-3" align="right" >
									<TextField
										required
										id="subject"
										select
										label={<IntlMessages id="components.note.subject" />}
										value={this.state.subject}
										onChange={this.handleChange('subject')}
										SelectProps={{}}
										margin="normal"
										fullWidth >
										{this.props.subjects.map(option => (
											<MenuItem key={option.id} value={option.id}>
												{option.name}
											</MenuItem>
										))}
									</TextField>
								</div>
							</div>
						</CardBox>
					</div>
				</form>
				<ListGrade
					studentList={this.state.studentList}
					classId={this.state.classe}
					examId={this.state.examen}
				/>

			</div>
		)
	}
}

export default connect(mapStateToProps, { getGrades, getSubject, getExam, getClasses })(Grades);
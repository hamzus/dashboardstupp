
import React from 'react';
import asyncComponent from '../../../util/asyncComponent';
import {Redirect, Route, Switch} from 'react-router-dom';

const GradesMenu = ({ match }) => (

  <div className="app-wrapper" >
    <Switch>
      <Redirect exact from={`${match.url}/`} to={`${match.url}/grades`} />
      <Route path={`${match.url}/grades`} component={asyncComponent(() => import('./routes/Grade/Grades') )} />
      <Route path={`${match.url}/exams`} component={asyncComponent(() => import('./routes/Exams/Exams') )} />
      <Route path={`${match.url}/report`} component={asyncComponent(() => import('./routes/Report'))} />
    </Switch>
  </div>
);

export default GradesMenu;
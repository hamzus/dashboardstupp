import React from 'react';
import IntlMessages from 'util/IntlMessages';

class Learning extends React.Component {

  render() {
    return (
      <div className="app-wrapper">
      <br/>
        <div className="d-flex justify-content-center">
          <h1><IntlMessages id="sidebar.components.e-learning"/></h1>
        </div>

      </div>
    );
  }
}

export default Learning;
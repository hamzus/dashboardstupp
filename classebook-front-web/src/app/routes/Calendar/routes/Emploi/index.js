
import React from 'react'
import events from '../Planning/events'
import HTML5Backend from 'react-dnd-html5-backend'
import { DragDropContext } from 'react-dnd'
import BigCalendar from 'react-big-calendar'
import withDragAndDrop from 'react-big-calendar/lib/addons/dragAndDrop';
import 'react-big-calendar/lib/addons/dragAndDrop/styles.less';
import moment from 'moment';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import { classService } from '../.././../../../_services/class.service';
import Select from '@material-ui/core/Select';
import IntlMessages from 'util/IntlMessages';
import { getClasses } from "../../../../../actions/classeAction";
import { connect } from "react-redux";
import ContainerHeader from '../../../../../components/ContainerHeader';
import { getEstablishment } from "../../../../../actions/establishmentAction";
import CardBox from 'components/CardBox/index';
import AddSupplies from './AddSupplies';
import baseUrl from 'config/config';
import axios from 'axios';



const roleIdSuperAdmin = 1;
const roleIdProfessor = 3;
const roleId = parseInt(localStorage.roles_id);
const localizer = BigCalendar.momentLocalizer(moment);

const DragAndDropCalendar = withDragAndDrop(BigCalendar);

class Emploi extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      events: [],
      establishment: '',
      establishmentClass: [],
      classe: '',
      supplieModal: false,
      eventId: ''
    };

    this.moveEvent = this.moveEvent.bind(this)
    this.handleChangeClasse = this.handleChangeClasse.bind(this)
    this.getEvents = this.getEvents.bind(this)
    this.addSupplies = this.addSupplies.bind(this);
    this.annuleModal = this.annuleModal.bind(this);
  }
  componentWillMount() {
    const roleId = parseInt(localStorage.roles_id);
    this.props.dispatch(getClasses())
    this.props.dispatch(getEstablishment());
    if (roleId === roleIdProfessor) {
      axios.get(`${baseUrl.baseUrl}/professors/findOne?filter={"where":{"profile_id":` + localStorage.profileId + `}}`)
        .then(res => {
          let professorId = res.data.id
          axios.get(`${baseUrl.baseUrl}/classes?filter={"where":{"establishment_id":` + localStorage.establishment_id + `}}`)
            .then(res => {
              let establishmentClassList = res.data;
              let professorList = [];
              let professorClassList = [];
              establishmentClassList.map(element => {
                professorList = element.professor_id;
                for (let id of professorList) {
                  if (id == professorId) {
                    professorClassList.push(element);
                    this.setState({ establishmentClass: professorClassList })
                  }
                }
              })

            }).catch(error => {
              console.log(error)
            });
        })
    }
  };


  annuleModal() {
    this.setState({
      supplieModal: false, eventId: ''
    })
  }

  addSupplies(event) {
    this.setState({ supplieModal: true, eventId: event.id });
  }

  moveEvent({ event, start, end }) {
    const { events } = this.state;

    const idx = events.indexOf(event);
    const updatedEvent = { ...event, start, end };

    const nextEvents = [...events];
    nextEvents.splice(idx, 1, updatedEvent);

    this.setState({
      events: nextEvents
    });

    alert(`${event.title} was dropped onto ${event.start}`);
  }
  handleChangeClasse = name => event => {
    this.getEvents(event.target.value)
    this.setState({ classe: event.target.value });
  };

  getEvents(id) {
    let filter = {
      where: {
        id_class: id,
      //  establishment_id: this.state.establishment
      }
    };
    console.log(filter)

    let apiEndpoint = '/contextual_events/timetable/' +id;
    classService.get(apiEndpoint)
      .then((response) => {
        console.log(response.data)
        const events = response.data.timetable.map(event => {
          const container = {};
          container['start'] = new Date(event.start_lesson);
          container['end'] = new Date(event.end_lesson);
          container['title'] = event.generic_events.title;
          container['id'] = event.id;
          console.log(container)
          return container;
        });

        this.setState({
          events: events
        })
      }).catch((err) => {
      })

  }
  handleChange = name => event => {
    // console.log(event.target.value)
    let establishmentClass = this.props.classes.filter(classe => classe.establishment_id == event.target.value)
    this.setState({ [name]: event.target.value, establishmentClass: establishmentClass, events: [], classe: '' });

  };

  render() {
  let now = moment()

    return (
      <div className="animated slideInUpTiny animation-duration-3">
        <ContainerHeader title={<IntlMessages id="model.timetable" />} match={this.props.match} />
        {this.state.supplieModal ?
          <AddSupplies isOpen={this.state.supplieModal} annuleModal={this.annuleModal} eventId={this.state.eventId} /> : ''}
        <CardBox styleName="col-12" heading={<IntlMessages id="autocomplete.emploidutemps.heading" />}>
          <div className="row">
            {(() => {
              if (roleId === roleIdSuperAdmin) {
                return (
                  <div className="col-lg-3 col-sm-6 col-12">

                    <FormControl className="w-100 mb-2">
                      <InputLabel htmlFor="age-simple">{<IntlMessages id="components.professor.formadd.etablissement_id" />}</InputLabel>
                      <Select
                        value={this.state.establishment}
                        onChange={this.handleChange('establishment')}
                        input={<Input id="establishment" />}
                      >
                        <MenuItem value="">
                          <em>{<IntlMessages id="autocomplete.emploidutemps.none" />}</em>
                        </MenuItem>
                        {this.props.establishments.map(establishment => (
                          <MenuItem key={establishment.id} value={establishment.id}>
                            {establishment.name}
                          </MenuItem>
                        ))}

                      </Select>
                    </FormControl>
                  </div>
                )
              }
            })()}
            <div className="col-lg-3 col-sm-6 col-12">
              <FormControl className="w-100 mb-2">
                <InputLabel htmlFor="age-simple">{<IntlMessages id="components.note.class" />}</InputLabel>
                <Select
                  value={this.state.classe}
                  onChange={this.handleChangeClasse('classe')}
                  input={<Input id="classe" />}
                >
                  <MenuItem value="">
                    <em>{<IntlMessages id="autocomplete.emploidutemps.none" />}</em>
                  </MenuItem>
                  {this.state.establishmentClass.map(establishment => (
                    <MenuItem key={establishment.id} value={establishment.id}>
                      {establishment.name}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </div>
          </div>

        </CardBox>
        <DragAndDropCalendar
          selectable
          localizer={localizer}
          events={this.state.events}
          onEventDrop={this.moveEvent}
          defaultView='week'
          defaultDate={now}
          onSelectEvent={event => this.addSupplies(event)}
        />

      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    classes: state.classes,
    establishments: state.establishment.remoteEstablishments
  }
}
Emploi = DragDropContext(HTML5Backend)(Emploi);

export default connect(mapStateToProps)(Emploi)

import React from "react";
import { Modal, ModalBody, ModalHeader } from "reactstrap";
import IntlMessages from 'util/IntlMessages';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Auxiliary from "util/Auxiliary";

const frequency = [
    {
        value: 'quotidien',
        label: 'Quotidien',
    },
    {
        value: 'hebdomadaire',
        label: 'Hebdomadaire',
    },
    {
        value: 'Quinzomadaire',
        label: 'Quinzomadaire',
    },
    {
        value: 'mensuelle',
        label: 'Mensuelle',
    },
    {
        value: 'trimestriel',
        label: 'Trimestriel',
    },
    {
        value: 'annuel',
        label: 'Annuel',
    },

];
const eventType = [
    {
        value: 'lesson',
        label: 'Séance',
    },
    {
        value: 'meeting',
        label: 'Réunion',
    },
    {
        value: 'party',
        label: 'Fête',
    },
    {
        value: 'exam',
        label: 'Examan',
    },
    {
        value: 'Class Council',
        label: 'Conseil de classe',
    },
    {
        value: 'Trip',
        label: 'Sortie',
    },
    {
        value: 'Rattrapage',
        label: 'catch up session',
    },
    {
        value: 'Rattrapage',
        label: 'detention',
    },
    {
        value: 'other',
        label: 'autre',
    },


];

class AddLesson extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            class: '',
            subject: '',
            establishment: '',
            professor: '',
            frequency: '',
            room: '',
            classList: [],
            event_type: '',
            establishmentRooms: [],
            establishmentSubjects: []
        };
        this.handleAnnule = this.handleAnnule.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChangeEstablishment = this.handleChangeEstablishment.bind(this)



    }
    componentDidMount() {

    }

    handleAnnule() {
        this.props.annuleModal();

    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };
    handleChangeEstablishment = name => event => {
        let establishmentClass = this.props.classes.filter(classe => classe.establishment_id == event.target.value)
        let establishmentRooms = this.props.classrooms.filter(room => room.id_establishment == event.target.value)
        let establishmentSubjects = this.props.subjects.filter(subject => subject.id_establishment == event.target.value)
        this.setState({ [name]: event.target.value, classList: establishmentClass, establishmentRooms: establishmentRooms, establishmentSubjects: establishmentSubjects });
    }
    handleToggle() {
        this.props.annuleModal();
    }
    handleSubmit = (e) => {

        e.preventDefault();
        this.props.handleSubmit(this.state);
        this.setState({
            title: '',
            class: '',
            subject: '',
            establishment: '',
            professor: '',
            frequency: '',
            room: '',
            event_type: ''
        });
    }

    render() {
        return (
            <Auxiliary>
                <Modal isOpen={this.props.modal} >
                    <ModalHeader toggle={this.handleToggle.bind(this)}>{<IntlMessages id="title.modal.lesson.add" />}</ModalHeader>
                    <ModalBody>
                        <form className="row" onSubmit={this.handleSubmit}>
                            <div className="col-sm-4">
                                <TextField

                                    required
                                    id="contract_start_date"
                                    type="string"
                                    onChange={this.handleChange}
                                    value={this.props.startDateLesson}
                                    helperText={<IntlMessages id="components.establishments.formadd.date.debut.sceances" />}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-4">
                                <TextField
                                    required
                                    id="contract_end_date"
                                    type="string"
                                    onChange={this.handleChange}
                                    value={this.props.endDateLesson}
                                    helperText={<IntlMessages id="components.establishments.formadd.date.fin.sceances" />}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-4">
                                <TextField
                                    required
                                    id="event_day"
                                    type="string"
                                    onChange={this.handleChange}
                                    value={this.props.day}
                                    helperText={<IntlMessages id="components.establishments.formadd.jour.sceances" />}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    required
                                    id="title"
                                    type="string"
                                    onChange={this.handleChange('title')}
                                    value={this.props.name}
                                    label={<IntlMessages id="components.event.planning.modal.add.name.field" />}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    required
                                    id="event_type"
                                    onChange={this.handleChange('event_type')}
                                    select
                                    label={<IntlMessages id="components.event.planning.modal.add.name.event_type" />}
                                    value={this.state.event_type}
                                    SelectProps={{}}
                                    margin="normal"
                                    fullWidth
                                >
                                    {eventType.map((option, index) => (
                                        <MenuItem key={index} value={option.value}>
                                            {option.label}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div>

                            <div className="col-sm-6">
                                <TextField
                                    required
                                    id="establishment"
                                    onChange={this.handleChangeEstablishment('establishment')}
                                    select
                                    label={<IntlMessages id="components.establishments.formadd.establishment" />}
                                    value={this.state.establishment}
                                    SelectProps={{}}
                                    margin="normal"
                                    fullWidth
                                >
                                    {this.props.establishments.map(option => (
                                        <MenuItem key={option.id} value={option.id}>
                                            {option.name}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    required
                                    id="class"
                                    onChange={this.handleChange('class')}
                                    select
                                    label={<IntlMessages id="components.establishments.formadd.class" />}
                                    value={this.state.class}
                                    SelectProps={{}}
                                    margin="normal"
                                    fullWidth
                                >
                                    {this.state.classList.map(option => (
                                        <MenuItem key={option.id} value={option.id}>
                                            {option.name}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div><div className="col-sm-6">
                                <TextField
                                    required
                                    id="room"
                                    onChange={this.handleChange('room')}
                                    select
                                    label={<IntlMessages id="components.establishments.formadd.room" />}
                                    value={this.state.room}
                                    SelectProps={{}}
                                    margin="normal"
                                    fullWidth
                                >
                                    {this.state.establishmentRooms.map(option => (
                                        <MenuItem key={option.id} value={option.id}>
                                            {option.name}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div><div className="col-sm-6">
                                <TextField
                                    required
                                    id="subject"
                                    onChange={this.handleChange('subject')}
                                    select
                                    label={<IntlMessages id="components.establishments.formadd.subject" />}
                                    value={this.state.subject}
                                    SelectProps={{}}
                                    margin="normal"
                                    fullWidth
                                >
                                    {this.state.establishmentSubjects.map(option => (
                                        <MenuItem key={option.id} value={option.id}>
                                            {option.name}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div><div className="col-sm-6">
                                <TextField
                                    required
                                    id="professor"
                                    onChange={this.handleChange('professor')}
                                    select
                                    label={<IntlMessages id="components.establishments.formadd.professor" />}
                                    value={this.state.professor}
                                    SelectProps={{}}
                                    margin="normal"
                                    fullWidth
                                >
                                    {this.props.professors.map(option => (
                                        <MenuItem key={option.id} value={option.id}>
                                            {option.name}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div>

                            <div className="col-sm-6">

                                <TextField
                                    required
                                    id="frequency"
                                    onChange={this.handleChange('frequency')}
                                    select
                                    label={<IntlMessages id="components.establishments.formadd.frequency" />}
                                    value={this.state.frequency}
                                    SelectProps={{}}
                                    margin="normal"
                                    fullWidth
                                >
                                    {frequency.map(option => (
                                        <MenuItem key={option.value} value={option.value}>
                                            {option.label}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div>
                            <div className="col-md-12 text-right ">
                                <br /><br />
                                <Button variant="contained" className="jr-btn bg-indigo text-white " type="submit" >{<IntlMessages id="components.establishments.formadd.buttonAdd" />}</Button>
                                <Button variant="contained" className="jr-btn bg-grey text-white " onClick={this.handleAnnule}>{<IntlMessages id="components.establishments.formadd.buttonCancel" />}</Button>
                            </div>
                        </form>
                    </ModalBody>
                </Modal>
            </Auxiliary>
        )
    }
}

export default AddLesson;
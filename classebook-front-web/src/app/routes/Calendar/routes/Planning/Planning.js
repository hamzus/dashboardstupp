import React from 'react'
import HTML5Backend from 'react-dnd-html5-backend'
import { DragDropContext } from 'react-dnd'
import BigCalendar from 'react-big-calendar'
import withDragAndDrop from 'react-big-calendar/lib/addons/dragAndDrop';
import 'react-big-calendar/lib/addons/dragAndDrop/styles.less';
import moment from 'moment';
import AddEvent from './AddEvent';
import PlanningCalendar from './PlanningCalendar';

//import 'moment/locale/fr';
import axios from 'axios';
import baseUrl from '../../../../../config/config';
import { getClasses } from "../../../../../actions/classeAction";
import { connect } from "react-redux";
import ContainerHeader from '../../../../../components/ContainerHeader';
import { getEstablishment } from "../../../../../actions/establishmentAction";
import IntlMessages from 'util/IntlMessages';
import { getSubject } from "../../../../../actions/subjectAction";
import { getData as getProfessors } from "../../../../../actions/professorAction";
import {
  getEvents,
  addEvent,
  archiverEvent,
  updateEvent
} from "../../../../../actions/planningActions";







const localizer = BigCalendar.momentLocalizer(moment);

const DragAndDropCalendar = withDragAndDrop(BigCalendar);

class Planning extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      events: [],
      modal: false,
      startHours: '',
      endHours: '',
      day: '',
      slotInfo: '',
      eventList: [],
      classrooms: []
    };

    this.moveEvent = this.moveEvent.bind(this)
    this.addLesson = this.addLesson.bind(this)
    this.annuleModal = this.annuleModal.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)



  }
  
  addLesson(slotInfo) {


    const startHours = moment(slotInfo.start).format("HH:mm ")
    const endHours = moment(slotInfo.end).format("HH:mm ")
    const day = moment(slotInfo.end).format("dddd", "fr")


    this.setState({
      modal: true,
      startHours: startHours,
      endHours: endHours,
      day: day,
      slotInfo: slotInfo,
      eventList: []
    })


  }
  componentWillMount() {
    this.props.dispatch(getClasses())
    this.props.dispatch(getEstablishment());
    this.props.dispatch(getSubject());
    this.props.dispatch(getProfessors());
    this.props.dispatch(getEvents());
    this.setState({
      events: this.props.events
    })





    axios.get(`${baseUrl.baseUrl}/rooms`)
      .then(res => {

        this.setState({ classrooms: res.data }, function () {
        });

      });
  }

  handleSubmit(data) {
     const event = {
      start_date_generic_event: new Date(this.state.slotInfo.start),
      end_date_generic_event: new Date(this.state.slotInfo.end),
      frequency: data.frequency,
      id_class: data.class,
      id_professor: data.professor,
      establishment_id: data.establishment,
      subject_id: data.subject,
      room_id: data.room,
      title:data.title,
      event_type:data.event_type
    }
     this.props.dispatch(addEvent(event))
    this.setState({
      modal: false
    })


  }
  moveEvent({ event, start, end }) {


    const { events } = this.state;

    const idx = events.indexOf(event);
    const updatedEvent = { ...event, start, end };

    const nextEvents = [...events];
    nextEvents.splice(idx, 1, updatedEvent);

    this.setState({
      events: nextEvents
    });

    alert(`${event.title} was dropped onto ${event.start}`);
  }
  annuleModal() {
    this.setState({
      modal: false
    })
  }


  render() {
    const events = this.props.events
 
    return (
      <div className="animated slideInUpTiny animation-duration-3">
        <PlanningCalendar match={this.props.match} events={this.props.events} addLesson={this.addLesson} classes={this.props.classes} establishments={this.props.establishments} />
        <AddEvent modal={this.state.modal} startDateLesson={this.state.startHours} endDateLesson={this.state.endHours} annuleModal={this.annuleModal} day={this.state.day} handleSubmit={this.handleSubmit} classes={this.props.classes} establishments={this.props.establishments} subjects={this.props.subjects} establishments={this.props.establishments} professors={this.props.professors} classrooms={this.state.classrooms} />
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    classes: state.classes,
    subjects: state.subject.remoteSubjects,
    establishments: state.establishment.remoteEstablishments,
    professors: state.professor.remoteProfessors,
    events: state.planning.events



  }
}
Planning = DragDropContext(HTML5Backend)(Planning);

export default connect(mapStateToProps)(Planning)

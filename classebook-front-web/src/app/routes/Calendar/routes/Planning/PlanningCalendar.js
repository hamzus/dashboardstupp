import React from 'react'
import BigCalendar from 'react-big-calendar'
import withDragAndDrop from 'react-big-calendar/lib/addons/dragAndDrop';
import 'react-big-calendar/lib/addons/dragAndDrop/styles.less';
import moment from 'moment';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import IntlMessages from 'util/IntlMessages';
import { classService } from '../.././../../../_services/class.service';
import Button from '@material-ui/core/Button';




import ContainerHeader from '../../../../../components/ContainerHeader';
import CardBox from 'components/CardBox/index';
const roleIdSuperAdmin = 1;
const roleIdProfessor = 3;
const roleId = parseInt(localStorage.roles_id);


const localizer = BigCalendar.momentLocalizer(moment);

const DragAndDropCalendar = withDragAndDrop(BigCalendar);

class PlanningCalendar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      events: [],
      modal: false,
      startHours: '',
      endHours: '',
      day: '',
      slotInfo: '',
      eventList: [],
      classrooms: [],
      classe: '',
      establishmentClass:[],
      establishment:''
    };

    this.handleChangeClasse = this.handleChangeClasse.bind(this)
    this.getEventsClass = this.getEventsClass.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }
  getEventsClass(id) {
    let filter = {
      where: {
        id_class: id
      }
    };
    let apiEndpoint = '/generic_events?filter=' + JSON.stringify(filter);
    classService.get(apiEndpoint)
      .then((response) => {
        const events = response.data.map(event => {
          const container = {};
          container['start'] = new Date(event.start_date_generic_event);
          container['end'] = new Date(event.end_date_generic_event);
          container['title'] = event.frequency;
          return container;
        });
        this.setState({
          events: events
        })
      }).catch((err) => {
      })

  }
  componentWillReceiveProps(props) {
    if (props.events.length != this.state.events.length) {
      const events = props.events.map(event => {
        const container = {}
        container['start'] = new Date(event.start_date_generic_event);
        container['end'] = new Date(event.end_date_generic_event);
        container['title'] = event.title;
        return container;
      });
      this.setState({
        events: events
      })
    }
  }
  handleChangeClasse = name => event => {
    this.getEventsClass(event.target.value)
    this.setState({ classe: event.target.value });
  };
  handleClick() {

    let apiEndpoint = '/contextual_events/generate-timetable/' + this.state.classe
    classService.get(apiEndpoint)
      .then((response) => {
      })
      .catch((err) => {
      })

  }
  handleChange = name => event => {
    let establishmentClass = this.props.classes.filter(classe => classe.establishment_id == event.target.value)
    this.setState({ [name]: event.target.value, establishmentClass: establishmentClass, events: [], classe: '' });

  };

  render() {
    let now = new Date();
    const ClassList = Array.from(this.props.classes);
    return (
      <div className="animated slideInUpTiny animation-duration-3">
        <ContainerHeader title={<IntlMessages id="model.timetable" />} match={this.props.match} />


        <CardBox styleName="col-12" heading={<IntlMessages id="autocomplete.emploidutemps.heading" />}>
          <div className="row">
            {(() => {
              if (roleId === roleIdSuperAdmin) {
                return (
                  <div className="col-lg-3 col-sm-6 col-12">

                    <FormControl className="w-100 mb-2">
                      <InputLabel htmlFor="age-simple">{<IntlMessages id="pages.establishementPage" />}</InputLabel>
                      <Select
                        value={this.state.establishment}
                        onChange={this.handleChange('establishment')}
                        input={<Input id="establishment" />}
                      >
                        <MenuItem value="">
                          <em>{<IntlMessages id="autocomplete.emploidutemps.none" />}</em>
                        </MenuItem>
                        {this.props.establishments.map(establishment => (
                          <MenuItem key={establishment.id} value={establishment.id}>
                            {establishment.name}
                          </MenuItem>
                        ))}

                      </Select>
                    </FormControl>
                  </div>
                )
              }
            })()}
            <div className="col-lg-3 col-sm-6 col-12">
              <FormControl className="w-100 mb-2">
                <InputLabel htmlFor="age-simple">{<IntlMessages id="components.note.class" />}</InputLabel>
                <Select
                  value={this.state.classe}
                  onChange={this.handleChangeClasse('classe')}
                  input={<Input id="classe" />}
                >
                  <MenuItem value="">
                    <em>{<IntlMessages id="autocomplete.emploidutemps.none" />}</em>
                  </MenuItem>
                  {this.state.establishmentClass.map(classe => (
                    <MenuItem key={classe.id} value={classe.id}>
                      {classe.name}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </div>
            <div>
              <Button variant="contained" color="primary" className="jr-btn text-white" style={{ position: "relative", float: 'right', marginBottom: 10 + 'px' }} onClick={this.handleClick} >
                <IntlMessages id="button.save.registreAppel" /></Button>
            </div>
          </div>
        </CardBox>
        <DragAndDropCalendar
          selectable
          localizer={localizer}
          events={this.state.events}
          onEventDrop={this.moveEvent}
          defaultView='week'
          // defaultDate={now}
          onSelectSlot={(slotInfo) => alert(
            `selected slot: \n\nstart ${slotInfo.start.toLocaleString()} ` +
            `\nend: ${slotInfo.end.toLocaleString()}` +
            `\naction: ${slotInfo.action}`
          )}
          onSelectSlot={(slotInfo) => this.props.addLesson(slotInfo)}
        />

      </div>
    )
  }
}

export default PlanningCalendar

import React from 'react';
import asyncComponent from '../../../util/asyncComponent';
import { Redirect, Route, Switch } from 'react-router-dom';

const roleIdSuperAdmin = 1;
const roleIdAdmin = 2;
const roleIdProfessor = 3;
const roleIdParent = 4;
const roleIdStudent = 5;
const roleId = parseInt(localStorage.roles_id);

const Calendar = ({ match }) => (



    <div className="app-wrapper">
        <Switch>
            <Redirect exact from={`${match.url}/`} to={`${match.url}/emploi`} />
            <Route path={`${match.url}/emploi`} component={asyncComponent(() => import('./routes/Emploi'))} />
            {(() => {
                if (roleId === roleIdSuperAdmin || roleId === roleIdAdmin) {
                    return (
                        <Route path={`${match.url}/planning`} component={asyncComponent(() => import('./routes/Planning/Planning'))} />
                    )
                }
            })()}
        </Switch>
    </div>

);

export default Calendar; 
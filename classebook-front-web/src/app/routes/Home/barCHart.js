import React from 'react';
import * as d3 from 'd3';
import { getClasses } from "/home/hamza/Documents/projects/classebook-front-web/src/actions/classeAction.js";
import { connect } from "react-redux";
import { getEstablishment } from "/home/hamza/Documents/projects/classebook-front-web/src/actions/establishmentAction.js";
class BarCHart extends React.Component {
    constructor(props) {
        super(props);


        this.create = this.create.bind(this);
    };


    create(props) {
        const sample = [];

        if (this.props.classes.length > 0) {
            for (var i = 0; i < this.props.classes.length; i++) {
                sample.push({
                    language: this.props.classes[i].name,
                    value: this.props.classes[i].students_number,
                    color: '#507dca'
                })
            }
        }
        const svgContainer = d3.select('#layout1');
        const svg = svgContainer.append('svg');

        const margin = 80;
        const width = 700 - 2 * margin;
        const height = 700 - 2 * margin;

        const chart = svg.append('g')
        // .attr('transform', `translate(${margin}, ${margin})`);
        const xScale = d3.scaleBand()
            .range([0, width])
            .domain(sample.map((s) => s.language))
            .padding(0.4)

        const yScale = d3.scaleLinear()
            .range([height, 0])
            .domain([0, 100]);

        // vertical grid lines
        // const makeXLines = () => d3.axisBottom()
        //   .scale(xScale)

        const makeYLines = () => d3.axisLeft()
            .scale(yScale)

        chart.append('g')
            .attr('transform', `translate(0, ${height})`)
            .call(d3.axisBottom(xScale));

        chart.append('g')
            .call(d3.axisLeft(yScale));

        // vertical grid lines
        // chart.append('g')
        //   .attr('class', 'grid')
        //   .attr('transform', `translate(0, ${height})`)
        //   .call(makeXLines()
        //     .tickSize(-height, 0, 0)
        //     .tickFormat('')
        //   )

        chart.append('g')
            .attr('class', 'grid')
            .call(makeYLines()
                .tickSize(-width, 0, 0)
                .tickFormat('')
            )

        const barGroups = chart.selectAll()
            .data(sample)
            .enter()
            .append('g')

        barGroups
            .append('rect')
            .attr('class', 'bar')
            .attr('x', (g) => xScale(g.language))
            .attr('y', (g) => yScale(g.value))
            .attr('height', (g) => height - yScale(g.value))
            .attr('width', xScale.bandwidth())
            .on('mouseenter', function (actual, i) {
                d3.selectAll('.value')
                    .attr('opacity', 0)

                d3.select(this)
                    .transition()
                    .duration(300)
                    .attr('opacity', 0.6)
                    .attr('x', (a) => xScale(a.language) - 5)
                    .attr('width', xScale.bandwidth() + 10)

                const y = yScale(actual.value)

                var line = chart.append('line')
                    .attr('id', 'limit')
                    .attr('x1', 0)
                    .attr('y1', y)
                    .attr('x2', width)
                    .attr('y2', y)

                barGroups.append('text')
                    .attr('class', 'divergence')
                    .attr('x', (a) => xScale(a.language) + xScale.bandwidth() / 2)
                    .attr('y', (a) => yScale(a.value) + 30)
                    .attr('fill', 'white')
                    .attr('text-anchor', 'middle')
                    .text((a, idx) => {
                        const divergence = (a.value - actual.value).toFixed(1)

                        let text = ''
                        if (divergence > 0) text += '+'
                        text += `${divergence}%`

                        return idx !== i ? text : '';
                    })

            })
            .on('mouseleave', function () {
                d3.selectAll('.value')
                    .attr('opacity', 1)

                d3.select(this)
                    .transition()
                    .duration(300)
                    .attr('opacity', 1)
                    .attr('x', (a) => xScale(a.language))
                    .attr('width', xScale.bandwidth())

                chart.selectAll('#limit').remove()
                chart.selectAll('.divergence').remove()
            })

        barGroups
            .append('text')
            .attr('class', 'value')
            .attr('x', (a) => xScale(a.language) + xScale.bandwidth() / 2)
            .attr('y', (a) => yScale(a.value) + 30)
            .attr('text-anchor', 'middle')
            .text((a) => `${a.value}%`)

        svg
            .append('text')
            .attr('class', 'label')
            .attr('x', -(height / 2) - margin)
            .attr('y', margin / 2.4 - 15)
            .attr('transform', 'rotate(-90)')
            .attr('text-anchor', 'middle')
            .text("taux d'absence (%)")

        svg.append('text')
            .attr('class', 'label')
            .attr('x', width / 2 + margin-50)
            .attr('y', height + margin * 1.7-100)
            .attr('text-anchor', 'middle')
            .text('toutes les classes')

        svg.append('text')
            .attr('class', 'title')
            .attr('x', width / 2 + margin)
            .attr('y', 40)
            .attr('text-anchor', 'middle')
            .text("taux d'absence de toutes les classes")


    }

    componentDidMount() {
        this.props.dispatch(getClasses())
        this.props.dispatch(getEstablishment());
        this.create(this.props);
    }
    render() {
        
        return (
            <div>
            </div>
        )



    }

}
const mapStateToProps = (state) => {
    return {
        classes: state.classes,

        establishments: state.establishment.remoteEstablishments,

    }
}
export default connect(mapStateToProps)(BarCHart);


import React from 'react';
import * as d3 from 'd3';
import dataMap from './dataMap'

import { getClasses } from "/home/hamza/Documents/projects/classebook-front-web/src/actions/classeAction.js";
import { connect } from "react-redux";
import { getEstablishment } from "/home/hamza/Documents/projects/classebook-front-web/src/actions/establishmentAction.js";
class Bubblemap extends React.Component {
    constructor(props) {
        super(props);



        this.create = this.create.bind(this);
    }
    create(props) {
        var data = dataMap;
        // Size ?
        var width = 400
        var height = 400

        // The svg
        var svg = d3.select("#layout1")
            .append("svg")
            .attr("width", width + 100)
            .attr("height", height + 100)

        // Map and projection
        var projection = d3.geoMercator()
            .center([1, 47])                // GPS of location to zoom on
            .scale(2600)                       // This is like the zoom
            .translate([width / 2 - 300, height / 2 - 700])

        // Create data for circles:
        var markers = [

            { long: 10.640525, lat: 35.828829, name: "sousse" }, // sousse
            { long: 10.79, lat: 35.73, name: "monastir" }, // monastir
            { long: 10.19, lat: 36.85, name: "tunis" }, // tunis
            { long: 8.70, lat: 36.16, name: "kef" }, // kef
            { long: 10.75, lat: 34.75, name: "sfax" }, // sfax
            { long: 9.64, lat: 31.55, name: "Tataouine" }, // Tataouine
            { long: 10.67, lat: 36.48, name: "nabeul" }, // nabeul
            { long: 8.83, lat: 35.16, name: "kasserine" }, // kasserine
            { long: 9.59, lat: 34.86, name: "sidi bouzid" }, // sidi bouzid

        ];
        var etab = []


        if (this.props.establishments.length > 0) {
            for (var i = 0; i < this.props.establishments.length; i++) {
                for (var j = 0; j < markers.length; j++) {
                    if (this.props.establishments[i].city === markers[j].name) {

                        etab.push({
                            name: this.props.establishments[i].name,
                            city: this.props.establishments[i].city,
                            addresse: this.props.establishments[i].address,
                            nameDir: this.props.establishments[i].name_director,
                            end_contrat: this.props.establishments[i].contract_end_date,
                            long: markers[j].long,
                            lat: markers[j].lat
                        })
                    }
                }
            }
        }
        console.log(etab);

        // Filter data
        var datafeatures = data.features.filter(function (d) { return d.properties.name == "Tunisia" })
        // Draw the map
        svg.append("g")
            .selectAll("path")
            .data(datafeatures)
            .enter()
            .append("path")
            .attr("fill", "#FC427B")
            .attr("d", d3.geoPath()
                .projection(projection)
            )
            .style("stroke", "black")
            .style("opacity", .3);

        // create a tooltip
        var Tooltip = d3.select("#layout")
            .append("text")
            .attr("class", "tooltip")
            .style("opacity", 1)
            .style("background-color", "white")
            .style("border", "solid")
            .style("border-width", "2px")
            .style("border-radius", "5px")
            .style("padding", "5px")


        // Three function that change the tooltip when user hover / move / leave a cell
        var mouseover = function (d) {
            Tooltip.style("opacity", 3)
        }
        var mousemove = function (d) {

         
            Tooltip
                // .text("nom établissement: " + d.name +
                //     " \n" +
                //     "city: " + d.city +
                //     " \n" +
                //     "address: " + d.addresse
                //     + "\n" +
                //     "name Directeur: " + d.nameDir
                //     + "\n" +
                //     "date fin contrat" + d.end_contrat
                // )
                .style(" text-align", "center")
                .attr("fill", "black")
                .attr("font-size", "15px")
                .html("Nom établissement: "+d.name + "<br/>"+
                        "City: "+ d.city + "<br/>" + 
                        "Address: " + d.addresse + "<br>" + 
                        "Name Directeur: " + d.nameDir + "<br>" + 
                        "Date fin contrat: " +d.end_contrat
                     )
                .style("left", (d3.mouse(this)[0] + 50) + "px")
                .style("top", (d3.mouse(this)[1]+50) + "px")
    }

        var mouseleave = function (d) {
            Tooltip.style("opacity", 0)
        }

        // Add circles:
        svg
            .selectAll("myCircles")
            .data(etab)
            .enter()
            .append("circle")
            .attr("cx", function (d) { return projection([d.long, d.lat])[0] })
            .attr("cy", function (d) { return projection([d.long, d.lat])[1] })
            .attr("r", 18)
            .attr("class", "circle")
            .style("fill", "#132ad4")
            .attr("stroke", "#8a9693")
            .attr("stroke-width", 3)
            .attr("fill-opacity", .4)
            .on("mouseover", mouseover)
            .on("mousemove", mousemove)
            .on("mouseleave", mouseleave);

        svg
            .selectAll("text")
            .data(etab)
            .enter()
            .append("text")
            .attr("x", function (d) { return projection([d.long, d.lat])[0] })
            .attr("y", function (d) { return projection([d.long, d.lat])[1] })
            .attr("text-anchor", "middle")
            .attr("font-size", "12px")
            .attr("fill", "black")
            .text(function (d) { return d.name });


        // })



    }

    componentDidMount() {
        this.props.dispatch(getClasses())
        this.props.dispatch(getEstablishment());
        this.create(this.props);
    }
    render() {
        return (
            <div>

            </div>
        )
    }


}

const mapStateToProps = (state) => {
    return {
        classes: state.classes,

        establishments: state.establishment.remoteEstablishments,

    }
}
export default connect(mapStateToProps)(Bubblemap);
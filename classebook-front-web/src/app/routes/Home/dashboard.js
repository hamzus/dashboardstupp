import React from 'react';
import * as d3 from 'd3';
import { getClasses } from "/home/hamza/Documents/projects/classebook-front-web/src/actions/classeAction.js";
import { connect } from "react-redux";
import { getEstablishment } from "/home/hamza/Documents/projects/classebook-front-web/src/actions/establishmentAction.js";
import { timingSafeEqual } from 'crypto';
class DashBoard extends React.Component {
    constructor(props) {
        super(props);



        this.create = this.create.bind(this);
    };

    create(props) {

        var round = (x, n) => n ? Math.round(x * (n = Math.pow(10, n))) / n : Math.round(x);
        /*
################ FORMATS ##################
-------------------------------------------
*/


        var formatAsPercentage = d3.format("%"),
            formatAsPercentage1Dec = d3.format(".1%"),
            formatAsInteger = d3.format(","),
            fsec = d3.timeFormat("%S s"),
            fmin = d3.timeFormat("%M m"),
            fhou = d3.timeFormat("%H h"),
            fwee = d3.timeFormat("%a"),
            fdat = d3.timeFormat("%d d"),
            fmon = d3.timeFormat("%b")
            ;



        /*
        ############# PIE CHART ###################
        -------------------------------------------
        */
        var dataset1 = [];
        if (this.props.establishments.length > 0) {
            for (var i = 0; i < this.props.establishments.length; i++) {
                dataset1.push({
                    etab_name: this.props.establishments[i].name,
                    measure: 0.25
                })

            }
        }






        function dsPieChart() {







            var width = 300,
                height = 300,
                outerRadius = Math.min(width, height) / 2,
                innerRadius = outerRadius * .999,
                // for animation
                innerRadiusFinal = outerRadius * .5,
                innerRadiusFinal3 = outerRadius * .45;
            //color = d3.schemeCategory10   ;//builtin range of colors

            var color = d3.scaleOrdinal()
                .domain(dataset1.map(d => d.etab_name))
                .range(["#25CCF7", "#1B9CFC", "#3B3B98", "#182C61", "#55E6C1"])

            var vis = d3.select("#layout1")
                .append("svg:svg")              //create the SVG element inside the <body>
                .data([dataset1])                   //associate our data with the document
                .attr("width", width)           //set the width and height of our visualization (these will be attributes of the <svg> tag
                .attr("height", height)
                .append("svg:g")                //make a group to hold our pie chart
                .attr("transform", "translate(" + 150 + "," + 150 + ")")    //move the center of the pie chart from 0, 0 to radius, radius
                ;

            var arc = d3.arc()              //this will create <path> elements for us using arc data
                .outerRadius(outerRadius).innerRadius(innerRadius);

            // for animation
            var arcFinal = d3.arc().innerRadius(innerRadiusFinal).outerRadius(outerRadius);
            var arcFinal3 = d3.arc().innerRadius(innerRadiusFinal3).outerRadius(outerRadius);

            var pie = d3.pie()           //this will create arc data for us given a list of values
                .value(function (d) { return d.measure; });    //we must tell it out to access the value of each element in our data array
            console.log(pie);
            var arcs = vis.selectAll("g.slice")     //this selects all <g> elements with class slice (there aren't any yet)
                .data(pie)                          //associate the generated pie data (an array of arcs, each having startAngle, endAngle and value properties) 
                .enter()                            //this will create <g> elements for every "extra" data element that should be associated with a selection. The result is creating a <g> for every object in the data array
                .append("svg:g")                //create a group to hold each slice (we will have a <path> and a <text> element associated with each slice)
                .attr("class", "slice")    //allow us to style things in the slices (like text)
                //.on("mouseover", mouseover)
                //.on("mouseout", mouseout)
                .on("click", up)
                ;

            arcs.append("svg:path")
                .attr("fill", function (d) { return color(d.data.etab_name); }) //set the color for each slice to be chosen from the color function defined above
                .attr("d", arc)     //this creates the actual SVG path using the associated data (pie) with the arc drawing function
                .append("svg:title") //mouseover title showing the figures
                .text(function (d) { return d.data.etab_name + ": " + formatAsPercentage(d.data.measure); });

            d3.selectAll("g.slice").selectAll("path").transition()
                .duration(750)
                .delay(10)
                .attr("d", arcFinal)
                ;

            // Add a label to the larger arcs, translated to the arc centroid and rotated.
            // source: http://bl.ocks.org/1305337#index.html
            arcs.filter(function (d) { return d.endAngle - d.startAngle > .2; })
                .append("svg:text")
                .attr("dy", ".35em")
                .attr("text-anchor", "middle")
                .attr("transform", function (d) { return "translate(" + arcFinal.centroid(d) + ")rotate(" + angle(d) + ")"; })
                //.text(function(d) { return formatAsPercentage(d.value); })
                .text(function (d) { return d.data.etab_name; })
                ;

            // Computes the label angle of an arc, converting from radians to degrees.
            function angle(d) {
                var a = (d.startAngle + d.endAngle) * 90 / Math.PI - 90;
                return a > 90 ? a - 180 : a;
            }


            // Pie chart title			
            vis.append("svg:text")
                .attr("dy", ".35em")
                .attr("text-anchor", "middle")
                .text("les établissements ")
                .attr("class", "title")
                ;



            function mouseover() {
                d3.select(this).select("path").transition()
                    .duration(750)
                    //.attr("stroke","red")
                    //.attr("stroke-width", 1.5)
                    .attr("d", arcFinal3)
                    ;
            }

            function mouseout() {
                d3.select(this).select("path").transition()
                    .duration(750)
                    //.attr("stroke","blue")
                    //.attr("stroke-width", 1.5)
                    .attr("d", arcFinal)
                    ;
            }

            function up(d, i) {

                /* update bar chart when user selects piece of the pie chart */
                //updateBarChart(dataset[i].class_name);
                updateBarChart(d.data.etab_name, color(d.data.etab_name));
                //updateLineChart(d.data.class_name, color[i]);

            }
        }

        dsPieChart();

        /*
        ############# BAR CHART ###################
        -------------------------------------------
        */



        var datasetBarChart = [
            { group: "All", class_name: "A", measure: 0 },
            { group: "All", class_name: "B", measure: 0 },
            { group: "All", class_name: "C", measure: 0 },
            { group: "All", class_name: "D", measure: 0 },
            { group: "All", class_name: "E", measure: 0 },
        ]
            ;
        if (this.props.classes.length > 0) {
            for (var i = 0; i < this.props.classes.length; i++) {
                for (var j = 0; j < this.props.establishments.length; j++) {
                    if (this.props.classes[i].establishment_id === this.props.establishments[j].id) {
                        datasetBarChart.push({
                            group: this.props.establishments[j].name,
                            class_name: this.props.classes[i].name,
                            measure: this.props.classes[i].students_number
                        })
                    }

                }



            };
        }



        // set initial group value
        var group = "All";
        function datasetBarChosen(group) {
            var ds = [];
            for (var x in datasetBarChart) {
                if (datasetBarChart[x].group == group) {
                    ds.push(datasetBarChart[x]);
                }
            }
            return ds;
        }


        function dsBarChartBasics() {

            var margin = { top: 20, right: 150, bottom: 20, left: 20 },
                width = 500 - margin.left - margin.right,
                height = 250 - margin.top - margin.bottom,
                colorBar = d3.schemeCategory10,
                barPadding = 1
                ;

            return {
                margin: margin,
                width: width,
                height: height,
                colorBar: colorBar,
                barPadding: barPadding
            }
                ;
        }

        function dsBarChart() {






            var firstDatasetBarChart = datasetBarChosen(group);


            var basics = dsBarChartBasics();

            var margin = basics.margin,
                width = basics.width,
                height = basics.height,
                colorBar = basics.colorBar,
                barPadding = basics.barPadding

            var xScale = d3.scaleLinear()
                .domain([0, firstDatasetBarChart.length])
                .range([0, width])
                ;

            // Create linear y scale 
            // Purpose: No matter what the data is, the bar should fit into the svg area; bars should not
            // get higher than the svg height. Hence incoming data needs to be scaled to fit into the svg area.  
            var yScale = d3.scaleLinear()
                // use the max funtion to derive end point of the domain (max value of the dataset)
                // do not use the min value of the dataset as min of the domain as otherwise you will not see the first bar
                .domain([0, d3.max(firstDatasetBarChart, function (d) { return d.measure; })])
                // As coordinates are always defined from the top left corner, the y position of the bar
                // is the svg height minus the data value. So you basically draw the bar starting from the top. 
                // To have the y position calculated by the range function
                .range([height, 0])
                ;

            //Create SVG element

            var svg = d3.select("#layout1")
                .append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .attr("id", "barChartPlot")
                .attr("transform", "translate(" + 15 + "," + 20 + ")")


                ;

            var plot = svg
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")")

                ;

            plot
                .data(firstDatasetBarChart)
                .enter()
                .append("g")
                .append("rect")
                .attr("x", function (d, i) {
                    return xScale(i);
                })
                .attr("width", width / firstDatasetBarChart.length - barPadding)
                .attr("y", function (d) {
                    return yScale(d.measure);
                })
                .attr("height", function (d) {
                    return height - yScale(d.measure);
                })
                .attr("fill", "lightgrey")
                .style("border", "2px solid")
                .style("border-width", "2px")
                .style("border-radius", "350px")
                .style("padding", "20px")
                ;


            // Add y labels to plot	

            plot.selectAll("text")
                .data(firstDatasetBarChart)
                .enter()
                .append("text")
                .text(function (d) {
                    return formatAsInteger(round(d.measure));
                })
                .attr("text-anchor", "middle")
                // Set x position to the left edge of each bar plus half the bar width
                .attr("x", function (d, i) {
                    return (i * (width / firstDatasetBarChart.length)) + ((width / firstDatasetBarChart.length - barPadding) / 2);
                })
                .attr("y", function (d) {
                    return yScale(d.measure) + 14;
                })
                .attr("class", "yAxis")
                /* moved to CSS			   
                .attr("font-family", "sans-serif")
                .attr("font-size", "11px")
                .attr("fill", "white")
                */
                ;

            // Add x labels to chart	

            var xLabels = svg
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + (margin.top + height) + ")")
                ;

            xLabels.selectAll("text.xAxis")
                .data(firstDatasetBarChart)
                .enter()
                .append("text")
                .text(function (d) { return d.class_name; })
                .attr("text-anchor", "middle")
                // Set x position to the left edge of each bar plus half the bar width
                .attr("x", function (d, i) {
                    return (i * (width / firstDatasetBarChart.length)) + ((width / firstDatasetBarChart.length - barPadding) / 2);
                })
                .attr("y", 15)
                .attr("class", "xAxis")
                //.attr("style", "font-size: 12; font-family: Helvetica, sans-serif")
                ;

            // Title

            svg.append("text")
                .attr("x", (width + margin.left + margin.right) / 2 + 120)
                .attr("y", 10)
                .attr("class", "title")
                .attr("text-anchor", "middle")
                .text("les classes et nombre des étudiants")
                ;
        }

        dsBarChart();

        /* ** UPDATE CHART ** */

        /* updates bar chart on request */

        function updateBarChart(group, colorChosen) {

            var currentDatasetBarChart = datasetBarChosen(group);
            console.log(currentDatasetBarChart)
            var basics = dsBarChartBasics();

            var margin = basics.margin,
                width = basics.width,
                height = basics.height,
                colorBar = basics.colorBar,
                barPadding = basics.barPadding
                ;

            var xScale = d3.scaleLinear()
                .domain([0, currentDatasetBarChart.length])
                .range([0, width])
                ;


            var yScale = d3.scaleLinear()
                .domain([0, d3.max(currentDatasetBarChart, function (d) { return d.measure; })])
                .range([height, 0])
                ;

            var svg = d3.select("#barChart");

            var plot = d3.select("#barChartPlot")
                .data(currentDatasetBarChart);

            

            /* Note that here we only have to select the elements - no more appending! */
            plot.selectAll("title").remove();
            plot.selectAll("rect").remove();
            plot.selectAll("rect")
                .data(currentDatasetBarChart)
                .enter()
                .append("rect")
                .transition()
                .duration(750)
                .attr("x", function (d, i) {
                    return xScale(i);
                })
                .attr("width", width / currentDatasetBarChart.length - barPadding)
                .attr("y", function (d) {
                    return yScale(d.measure);
                })
                .attr("height", function (d) {
                    return height - yScale(d.measure);
                })
                .attr("fill", colorChosen)
                .style("border", "2px solid")
                .style("border-width", "2px")
                .style("border-radius", "35px")
                ;

                // plot.selectAll("title")
                // .data(currentDatasetBarChart)
                // .enter()
                // .append("title")
                // .attr("font-size", "25px")
                // .text(function (d) {
                //     return ("classe" +
                //         formatAsInteger(round(d.measure))
                //     )
                // })


            // plot.append("rect:title") //mouseover title showing the figures
            //     .text(function (d) {
            //     return ("classe"+ 
            //     formatAsInteger(round(d.measure))



            //     )
            // })
            // plot.selectAll("title")
            //     .data(currentDatasetBarChart)
            //     .enter()
            //     .append("title") //mouseover title showing the figures
            //     .text(function (d) {
            //         return ("classe"+ 
            //         formatAsInteger(round(d.measure))



            //         )
            //     })





            plot.selectAll("text.yAxis").remove();// target the text element(s) which has a yAxis class defined
            plot.selectAll("text.yAxis")
                .data(currentDatasetBarChart)
                .enter()
                .append("text")
                .transition()
                .duration(750)
                .attr("text-anchor", "middle")
                .attr("font-size", "15px")
                .attr("x", function (d, i) {
                    return (i * (width / currentDatasetBarChart.length)) + ((width / currentDatasetBarChart.length - barPadding) / 2);
                })
                .attr("y", function (d) {
                    return yScale(d.measure) + 14;
                })
                .text(function (d) {
                    return formatAsInteger(round(d.measure));
                })
                .attr("class", "yAxis")
                ;



            plot.selectAll("text.xAxis").remove();
            plot.selectAll("text.xAxis")
                .data(currentDatasetBarChart)
                .enter()
                .append("text")
                .text(function (d) { return d.class_name; })
                .attr("text-anchor", "middle")
                // Set x position to the left edge of each bar plus half the bar width
                .attr("x", function (d, i) {
                    return (i * (width / currentDatasetBarChart.length)) + ((width / currentDatasetBarChart.length - barPadding) / 2);
                })
                .attr("y", 222)
                .attr("class", "xAxis")
                //.attr("style", "font-size: 12; font-family: Helvetica, sans-serif")
                ;
            


            // svg.selectAll("text.title") // target the text element(s) which has a title class defined
            //     .attr("x", (width + margin.left + margin.right) / 2)
            //     .attr("y", 15)
            //     .attr("class", "title")
            //     .attr("text-anchor", "middle")
            //     .text("new students in section " + group)
            //     ;
        }
    }


    /*
    ############# LINE CHART ##################
    -------------------------------------------
    */

    componentDidMount() {

        this.props.dispatch(getClasses())
        this.props.dispatch(getEstablishment());
        this.create(this.props);

        console.log("ajout1");

    }


    render() {

        return (
            <div>



            </div>
        )

    }
}


const mapStateToProps = (state) => {
    return {
        classes: state.classes,

        establishments: state.establishment.remoteEstablishments,

    }
}
export default connect(mapStateToProps)(DashBoard);

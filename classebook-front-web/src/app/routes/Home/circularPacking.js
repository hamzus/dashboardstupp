import React from 'react';
import * as d3 from 'd3';
import { getClasses } from "/home/hamza/Documents/projects/classebook-front-web/src/actions/classeAction.js";
import { connect } from "react-redux";
import { getEstablishment } from "/home/hamza/Documents/projects/classebook-front-web/src/actions/establishmentAction.js";
class CircularPack extends React.Component {
  constructor(props) {
    super(props);



    this.create = this.create.bind(this);
  }
  create(props) {
    
    // set the dimensions and margins of the graph
    var width = 450
    var height = 450

    // append the svg object to the body of the page
    var svg = d3.select("#layout")
      .append("svg")
      .attr("width", 1000)
      .attr("height", 450)

    // create dummy data -> just one element per circle
    var data = []
    if (this.props.classes.length > 0) {
      for (var i = 0; i < this.props.classes.length; i++) {
        for (var j = 0; j < this.props.establishments.length; j++) {
          if (this.props.classes[i].establishment_id === this.props.establishments[j].id) {
            data.push({
              name: this.props.classes[i].name,
              value: this.props.classes[i].students_number,
              group: this.props.establishments[j].name,
              capacity:this.props.classes[i].capacity
            })
          }

        }
      };
    }
    console.log(this.props.classes);

    // A scale that gives a X target position for each group
    var x = d3.scaleOrdinal()
      .domain(data.map(d => d.group))
      .range([50, 250, 400, 580,720])
    // A color scale
    var color = d3.scaleOrdinal()
      .domain(data.map(d => d.group))
      .range(["#F8766D", "#00BA38", "#619CFF", "#d41cc7","#f2e202"])

    // Initialize the circle: all located at the center of the svg area
    var group = svg
      .selectAll("circle")
      .data(data)
      .enter()
      .append("g");

    var node = group.append("circle")
      .attr("r", function (d) { return d.value+5 })
      .attr("cx", width / 2)
      .attr("cy", height / 2)
      .style("fill", function (d) { return color(d.group) })
      .style("fill-opacity", 0.8)
      .attr("stroke", "black")
      .style("stroke-width", 2)
      .call(d3.drag() // call specific function when circle is dragged
        .on("start", dragstarted)
        .on("drag", dragged)
        .on("end", dragended))
      ;
      // les noms des classes
    var cirLabel = group.append("text")
      .attr("x", width / 2)
      .attr("y", height / 2)
      .attr("text-anchor", "middle")
      .attr("font-size", "12px")
      .attr("fill", "black")
      .text(function (d) { return  d.name });
      // les noms des établissements
    var etabLabel = group.append("text")
      .attr("x", width)
      .attr("y", height)
      .attr("text-anchor", "middle")
      .attr("font-size", "17px")
      .attr("fill", "#0d176e")
      .text(function (d) { return d.group });

    // Features of the forces applied to the nodes:
    var simulation = d3.forceSimulation()
      .force("x", d3.forceX().strength(0.5).x(function (d) { return x(d.group) }))
      .force("y", d3.forceY().strength(0.1).y(height / 2))
      .force("center", d3.forceCenter().x(width / 2+140).y(height / 2)) // Attraction to the center of the svg area
      .force("charge", d3.forceManyBody().strength(1)) // Nodes are attracted one each other of value is > 0
      .force("collide", d3.forceCollide().strength(.1).radius(32).iterations(1)) // Force that avoids circle overlapping
    // Apply these forces to the nodes and update their positions.
    // Once the force algorithm is happy with positions ('alpha' value is low enough), simulations will stop.
    simulation
      .nodes(data)
      .on("tick", function (d) {
        node
          .attr("cx", function (d) {return d.x;})
          .attr("cy", function (d) { return d.y; })
          .append("title")
          .text(function (d) { return d.value +
             " étudiants"+
             "\n"+
              "capacity: "+
             d.capacity});
        cirLabel
          .attr("x", function (d) { return d.x; })
          .attr("y", function (d) { return d.y; });
        etabLabel
          .attr("x", function (d) { return x(d.group) + 55 })
          .attr("y", height - 80)

      });

    // What happens when a circle is dragged?
    function dragstarted(d) {
      if (!d3.event.active) simulation.alphaTarget(.03).restart();
      d.fx = d.x;
      d.fy = d.y;
    }
    function dragged(d) {
      d.fx = d3.event.x;
      d.fy = d3.event.y;
    }
    function dragended(d) {
      if (!d3.event.active) simulation.alphaTarget(.03);
      d.fx = null;
      d.fy = null;
    }



  }
  componentDidMount() {
    this.props.dispatch(getClasses())
    this.props.dispatch(getEstablishment());
    this.create(this.props);
  }
  render() {
    return (
      <div>

      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    classes: state.classes,

    establishments: state.establishment.remoteEstablishments,

  }
}
export default connect(mapStateToProps)(CircularPack);
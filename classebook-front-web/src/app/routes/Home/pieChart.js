import React from 'react';
import * as d3 from 'd3';
import { getClasses } from "/home/hamza/Documents/projects/classebook-front-web/src/actions/classeAction.js";
import { connect } from "react-redux";
import { getEstablishment } from "/home/hamza/Documents/projects/classebook-front-web/src/actions/establishmentAction.js";
class PieChart extends React.Component {
    constructor(props) {
        super(props);



        this.create = this.create.bind(this);
    }
    create(props) {
        var data = [
            // { name: "7éme", value: "19912018" },
            // { name: "5-9", value: "20501982" },
            // { name: "10-14", value: "20679786" },
            // { name: "15-19", value: "21354481" },
            // { name: "20-24", value: "22604232" },
            // { name: "25-29", value: "21698010" },
            // { name: "30-34", value: "21183639" },
            // { name: "35-39", value: "19855782" },
            // { name: "40-44", value: "20796128" },
            // { name: "45-49", value: "21370368" },
            // { name: "50-54", value: "22525490" },
            // { name: "55-59", value: "21001947" },
            // { name: "60-64", value: "18415681" },
            // { name: "65-69", value: "14547446" },
            // { name: "70-74", value: "10587721" },
            // { name: "75-79", value: "7730129" },
            // { name: "80-84", value: "5811429" },
            // { name: "≥85", value: "5938752" }
        ]
        
        if (this.props.classes.length > 0) {
            for (var i = 0; i < this.props.classes.length; i++) {
                for (var j = 0; j < this.props.establishments.length; j++) {
                    if (this.props.classes[i].establishment_id === this.props.establishments[j].id) {
                        data.push({
                            name: this.props.classes[i].name,
                            value: this.props.classes[i].students_number,
                            etab: this.props.establishments[j].name
                        })
                    }

                }



            };
        }

        const width = 200;
        const height = 150;
        function color(x) {
            return d3.scaleOrdinal()
                .domain(data.map(d => d.name))
                .range(d3.quantize(t => d3.interpolateSpectral(t * 0.8 + 0.1), data.length).reverse())(x);

        }
        function arcLabel() {
            const radius = Math.min(width, height) / 2 * 0.7;
            return d3.arc().innerRadius(radius).outerRadius(radius);
        }


        var arc = d3.arc()
            .innerRadius(0)
            .outerRadius(Math.min(width, height) / 2 - 1)

        var arcs = d3.pie()
            .sort(null)
            .value(function (d) { return d.value; })(data);



        var svg = d3.select("svg")
            .attr("viewBox", [-width / 2, -height / 2, width, height]);

        var plot = svg.append("g")
            .attr("stroke", "white");
        plot.selectAll("path")
            .data(arcs)
            .enter().append('path')
            .attr("fill", d => color(d.data.name))
            .attr("d", arc)
            .append("title")
            .text(d => `établissement :  ${d.data.etab} 
classe :  ${ d.data.name}
nombre d'éleves : ${ d.data.value.toLocaleString()}`);

        svg.append("g")
            .attr("font-family", "sans-serif")
            .attr("font-size", 10)
            .attr("text-anchor", "middle")
            .selectAll("text")
            .data(arcs)
            .join("text")
            .attr("transform", d => `translate(${arcLabel().centroid(d)})`)
            .call(text => text.append("tspan")
                .attr("y", "-0.4em")
                .attr("font-weight", "bold")
                .attr("font-size", "5px")
                .text(d => d.data.name))
            .call(text => text.filter(d => (d.endAngle - d.startAngle) > 0.25).append("tspan")
                .attr("x", 0)
                .attr("y", "0.7em")
                .attr("font-size", "5px")
                .attr("fill-opacity", 0.7)
                .text(d => d.data.value.toLocaleString()+"\n"+" éleves"));


        return svg.node();



    }
    componentDidMount() {
        this.props.dispatch(getClasses())
        this.props.dispatch(getEstablishment());
        console.log("ajout1");
        this.create(this.props);


    }

    render() {
        console.log("ajout2");

        this.create(this.props);



        return (
            <div></div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        classes: state.classes,

        establishments: state.establishment.remoteEstablishments,

    }
}
export default connect(mapStateToProps)(PieChart);


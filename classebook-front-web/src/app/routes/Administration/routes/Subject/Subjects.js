
import React from 'react';
import ContainerHeader from 'components/ContainerHeader/index';
import IntlMessages from 'util/IntlMessages';
import AddSubject from './AddSubject';
import SubjectList from './SubjectList';
import EditSubject from './EditSubject';
import DeleteSubject from './DeleteSubject';
import { getSubject } from "../../../../../actions/subjectAction";
import { getEstablishment } from "../../../../../actions/establishmentAction";
import { connect } from "react-redux";
import Input from '@material-ui/icons/Input';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

function mapStateToProps(state) {
    return {
        subjects: state.subject.remoteSubjects,
        establishments: state.establishment.remoteEstablishments
    };
};

class Subjects extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            addSubjectModal: false,
            subjectsList: [],
            subjectItem: [],
            edit: false,
            modalDelete: false,
            itemId: 0
        };
        this.editItemSubject = this.editItemSubject.bind(this);
        this.requestDeleteSubject = this.requestDeleteSubject.bind(this);
        this.handleCancelModalDelete = this.handleCancelModalDelete.bind(this);
        this.handleCancelModal = this.handleCancelModal.bind(this);
        this.addSubject = this.addSubject.bind(this);
    }

    requestDeleteSubject(id) {
        this.setState({ modalDelete: true, itemId: id });
    };

    editItemSubject(id) {
        const subjectItem = this.props.subjects.find(element => element.id == id);
        this.setState({ subjectItem: subjectItem, edit: true });
    };

    componentDidMount() {
        this.props.getSubject();
        this.props.getEstablishment();
    };

    handleCancelModal() {
        this.setState({ edit: false , addSubjectModal: false })
    };

    handleCancelModalDelete() {
        this.setState({ modalDelete: false, itemId: 0 })
    };
    addSubject(){
        this.setState({ addSubjectModal: true });
    }

    render() {

        return (
            <div className="app-wrapper">
                <ContainerHeader match={this.props.match} title={<IntlMessages id="sidebar.subjects" />} />
                <div className="col-md-12 text-right ">
                    <Fab size="small" color="primary" aria-label="Add" onClick={this.addSubject}>
                        <AddIcon />
                    </Fab>
                    &nbsp;&nbsp;&nbsp;
                    <Fab size="small" color="primary" >
                        <Input />
                    </Fab>
                </div>
               {this.state.addSubjectModal ? <AddSubject listEstablishment={this.props.establishments} cancelModal={this.handleCancelModal}/>:" "}
                <SubjectList listEstablishment={this.props.establishments} editSubject={this.editItemSubject} requestDeletesubject={this.requestDeleteSubject} list={this.props.subjects} />
                {this.state.edit ? <EditSubject subjectItem={this.state.subjectItem} listEstablishment={this.props.establishments} cancelModal={this.handleCancelModal} /> : ''}
                {this.state.modalDelete ? <DeleteSubject cancelModalDelete={this.handleCancelModalDelete} itemId={this.state.itemId} /> : ''}
            </div>
        )
    }
}
export default connect(mapStateToProps, { getSubject, getEstablishment })(Subjects);
import React from 'react';
import IntlMessages from 'util/IntlMessages';
import TextField from '@material-ui/core/TextField';
import CardBox from 'components/CardBox/index';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import { connect } from "react-redux";
import { addData } from "../../../../../actions/professorAction";
import Auxiliary from "util/Auxiliary";
import { Modal, ModalBody, ModalHeader } from "reactstrap";


const gender = [
    {
        value: 'Masculin',
        label: 'Masculin',
    },
    {
        value: 'Féminin',
        label: 'Féminin',
    }
];
class AddProfessor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            previewVisible: true,
            name: '',
            surname: '',
            gender: '',
            date_of_birth: '',
            date_start_contract: '',
            date_end_contract: '',
            address_mail: '',
            phone: '',
            cin: '',
            status: true,
            establishment_id: 0,
            subject_id: 0,
            subjectListFiltered: []
        };
        this.handleCancel = this.handleCancel.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleCancel() {
        this.setState({
            previewVisible: false,
            name: '',
            surname: '',
            gender: '',
            date_of_birth: '',
            date_start_contract: '',
            date_end_contract: '',
            address_mail: '',
            phone: '',
            cin: '',
            status: true,
            establishment_id: 0,
            subject_id: 0
        });
        this.props.cancelModal();
    };


    handleChange = name => event => {
        this.setState({ [name]: event.target.value });
        if (name == 'establishment_id') {
            const listReturned = this.props.listSubject.filter(subject => (event.target.value == subject.id_establishment));
            this.setState({ subjectListFiltered: listReturned });
        }
    };

    handleSubmit = (e) => {
        e.preventDefault();
        const name = this.state.name;
        const surname = this.state.surname;
        const gender = this.state.gender;
        const date_of_birth = this.state.date_of_birth;
        const date_start_contract = this.state.date_start_contract;
        const date_end_contract = this.state.date_end_contract;
        const address_mail = this.state.address_mail;
        const phone = this.state.phone;
        const cin = this.state.cin;
        const status = true;
        const establishment_id = this.state.establishment_id;
        const subject_id = this.state.subject_id;

        const data = { name, surname, gender, date_of_birth, date_start_contract, date_end_contract, address_mail, phone, cin, establishment_id, status, subject_id };

        this.props.dispatch(addData(data));
        this.setState({
            previewVisible: false,
            name: '',
            surname: '',
            gender: '',
            date_of_birth: '',
            date_start_contract: '',
            date_end_contract: '',
            address_mail: '',
            phone: '',
            cin: '',
            status: true,
            establishment_id: 0,
            subject_id: 0,
        });
        this.props.cancelModal();
    };


    render() {
        return (
            <div className="app-wrapper">
                <Auxiliary>

                    <Modal isOpen={this.state.previewVisible} >
                        <ModalHeader toggle={this.handleCancel} className="modal-box-header bg-primary text-white" >{<IntlMessages id="pages.professorPage" />}</ModalHeader>
                        <ModalBody>
                            <form className="row" autoComplete="off" onSubmit={this.handleSubmit}>
                                <div className="col-sm-6">
                                    <TextField
                                        required
                                        id="establishment"
                                        select
                                        label={<IntlMessages id="components.student.formadd.establishment" />}
                                        value={this.state.establishment_id}
                                        onChange={this.handleChange('establishment_id')}
                                        SelectProps={{}}
                                        margin="normal"
                                        fullWidth >
                                        {this.props.listEstablishment.map(establishment => (
                                            <MenuItem key={establishment.id} value={establishment.id}>
                                                {establishment.name}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="col-sm-6">
                                    <TextField
                                        required
                                        id="subject"
                                        select
                                        label={<IntlMessages id="components.professor.formadd.subject_id" />}
                                        value={this.state.subject_id}
                                        onChange={this.handleChange('subject_id')}
                                        SelectProps={{}}
                                        margin="normal"
                                        fullWidth >
                                        {this.state.subjectListFiltered.map(subject => (
                                            <MenuItem key={subject.id} value={subject.id}>
                                                {subject.name}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="col-sm-6">
                                    <TextField
                                        required
                                        name='name'
                                        id="name"
                                        label={<IntlMessages id="components.professor.formadd.name" />}
                                        onChange={this.handleChange('name')}
                                        value={this.state.name}
                                        margin="normal"
                                        fullWidth
                                    />
                                </div>
                                <div className="col-sm-6">
                                    <TextField
                                        required
                                        name='surname'
                                        id="surname"
                                        label={<IntlMessages id="components.professor.formadd.surname" />}
                                        onChange={this.handleChange('surname')}
                                        value={this.state.surname}
                                        margin="normal"
                                        fullWidth
                                    />
                                </div>
                                <div className="col-sm-6">
                                    <TextField
                                        required
                                        name='address_mail'
                                        id="address_mail"
                                        type="email"
                                        label={<IntlMessages id="components.professor.formadd.address_mail" />}
                                        onChange={this.handleChange('address_mail')}
                                        value={this.state.address_mail}
                                        margin="normal"
                                        fullWidth
                                    />
                                </div>
                                <div className="col-sm-6">
                                    <TextField

                                        name="gender"
                                        id="gender"
                                        select
                                        label={<IntlMessages id="components.student.formadd.gender" />}
                                        SelectProps={{}}
                                        onChange={this.handleChange('gender')}
                                        value={this.state.gender}
                                        margin="normal"
                                        fullWidth >
                                        {gender.map(option => (
                                            <MenuItem key={option.value} value={option.value}>
                                                {option.label}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="col-sm-6">
                                    <TextField
                                        name="date_of_birth"
                                        id="date_of_birth"
                                        type="date"
                                        helperText={<IntlMessages id="components.student.formadd.date_of_birth" />}
                                        onChange={this.handleChange('date_of_birth')}
                                        value={this.state.date_of_birth}
                                        margin="normal"
                                        fullWidth
                                    />
                                </div>

                                <div className="col-sm-6">
                                    <TextField
                                        required
                                        id="phone"
                                        onChange={this.handleChange('phone')}
                                        value={this.state.phone}
                                        label={<IntlMessages id="components.student.formadd.phone" />}
                                        type="number"
                                        margin="normal"
                                        fullWidth
                                    />
                                </div>
                                <div className="col-sm-6">
                                    <TextField
                                        name="date_start_contract"
                                        id="date_start_contract"
                                        type="date"
                                        helperText={<IntlMessages id="components.professor.formadd.date_start_contract" />}
                                        onChange={this.handleChange('date_start_contract')}
                                        value={this.state.date_start_contract}
                                        margin="normal"
                                        fullWidth
                                    />
                                </div>
                                <div className="col-sm-6">
                                    <TextField
                                        name="date_end_contract"
                                        id="date_end_contract"
                                        type="date"
                                        helperText={<IntlMessages id="components.professor.formadd.date_end_contract" />}
                                        onChange={this.handleChange('date_end_contract')}
                                        value={this.state.date_end_contract}
                                        margin="normal"
                                        fullWidth
                                    />
                                </div>
                                <div className="col-sm-6">
                                    <TextField
                                        required
                                        id="cin"
                                        onChange={this.handleChange('cin')}
                                        value={this.state.cin}
                                        label={<IntlMessages id="components.student.formadd.cin" />}
                                        type="number"
                                        margin="normal"
                                        fullWidth
                                    />
                                </div>
                                <div className="col-md-12 text-right ">
                                    <br /><br />
                                    <Button variant="contained" className="jr-btn bg-indigo text-white " type="submit" >{<IntlMessages id="components.establishments.formadd.buttonAdd" />}</Button>
                                    <Button variant="contained" className="jr-btn bg-grey text-white " onClick={this.handleCancel}>{<IntlMessages id="components.establishments.formadd.buttonCancel" />}</Button>
                                </div>
                            </form>
                        </ModalBody>
                    </Modal>
                </Auxiliary>
            </div >
        )
    }
} export default connect()(AddProfessor); 

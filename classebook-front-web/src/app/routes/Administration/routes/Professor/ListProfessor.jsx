
import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import CardBox from 'components/CardBox/index';
import IntlMessages from 'util/IntlMessages';
import Button from '@material-ui/core/Button';
import { connect } from "react-redux";
import { getData } from "../../../../../actions/professorAction";
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';

var dateFormat = require('dateformat');

function mapStateToProps(state) {
    return {
        professors: state.professor.remoteProfessors

    };
}

export class ProfessorList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            anchorEl: undefined,
            menuState: false,
            establishment_id: 0,
            subject_id: 0,
            subjectListFiltered: [],
            professorListFilteredBySubject: []
        };
        this.handleEdit = this.handleEdit.bind(this);
        this.handleRequestDelete = this.handleRequestDelete.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange = name => event => {
        this.setState({ [name]: event.target.value });
        if (name == 'establishment_id') {
            const listReturned = this.props.listSubject.filter(subject => (event.target.value == subject.id_establishment));
            this.setState({ subjectListFiltered: listReturned });
        }
        if (name == 'subject_id') {
            const listReturned = this.props.professors.filter(professor => (event.target.value == professor.subject_id));
            this.setState({ professorListFilteredBySubject: listReturned });
        }
    };

    onOptionMenuSelect = event => {
        this.setState({ menuState: true, anchorEl: event.currentTarget });
    };
    handleRequestClose = () => {
        this.setState({ menuState: false });
    };

    componentDidMount() {
        this.props.getData();
    }

    handleRequestDelete(e) {
        e.preventDefault();
        this.props.requestDeleteProfessor(e.currentTarget.value);
        this.setState({ menuState: false });
    };

    handleEdit(e) {
        e.preventDefault();
        this.props.editProfessor(e.currentTarget.value);
        this.setState({ menuState: false });
    };

    render() {
        const { anchorEl, menuState } = this.state;
        return (
            <div className="app-wrapper">
                <CardBox styleName="col-lg-12" >
                    <div className="row">
                        <div className="col-sm-6">
                            <TextField
                                required
                                id="establishment"
                                select
                                label={<IntlMessages id="components.student.formadd.establishment" />}
                                value={this.state.establishment_id}
                                onChange={this.handleChange('establishment_id')}
                                SelectProps={{}}
                                margin="normal"
                                fullWidth >
                                {this.props.listEstablishment.map(establishment => (
                                    <MenuItem key={establishment.id} value={establishment.id}>
                                        {establishment.name}
                                    </MenuItem>
                                ))}
                            </TextField>
                        </div>
                        <div className="col-sm-6">
                            <TextField
                                required
                                id="subject"
                                select
                                label={<IntlMessages id="components.professor.formadd.subject_id" />}
                                value={this.state.subject_id}
                                onChange={this.handleChange('subject_id')}
                                SelectProps={{}}
                                margin="normal"
                                fullWidth >
                                {this.state.subjectListFiltered.map(subject => (
                                    <MenuItem key={subject.id} value={subject.id}>
                                        {subject.name}
                                    </MenuItem>
                                ))}
                            </TextField>
                        </div>
                    </div>
                </CardBox>
                <CardBox styleName="col-lg-12" >
                    <div className="table-responsive-material">
                        <Table className="default-table table-unbordered table table-sm table-hover">
                            <TableHead className="th-border-b">
                                <TableRow>
                                    <TableCell>{<IntlMessages id="components.professor.formadd.name" />}</TableCell>
                                    <TableCell >{<IntlMessages id="components.professor.formadd.surname" />}</TableCell>
                                    {/* <TableCell >{<IntlMessages id="components.professor.formadd.etablissement_id" />}</TableCell> */}
                                    <TableCell >{<IntlMessages id="components.professor.formadd.subject_id" />}</TableCell>
                                    <TableCell >{<IntlMessages id="components.student.formadd.phone" />}</TableCell>
                                    <TableCell >{<IntlMessages id="components.student.formadd.cin" />}</TableCell>
                                    <TableCell >{<IntlMessages id="components.professor.formadd.address_mail" />}</TableCell>
                                    <TableCell >{<IntlMessages id="components.professor.formadd.date_start_contract" />}</TableCell>
                                    <TableCell >{<IntlMessages id="components.professor.formadd.date_end_contract" />}</TableCell>
                                    <TableCell ></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.professorListFilteredBySubject.map(professor => {
                                    return (
                                        <TableRow key={professor.id}>
                                            <TableCell>{professor.name}</TableCell>
                                            <TableCell >{professor.surname}</TableCell>
                                            {/* {this.props.listEstablishment.map((establishment, index) => {
                                                if (establishment.id == professor.establishment_id) {
                                                    return (
                                                        <TableCell key={index}>{establishment.name}</TableCell>
                                                    );
                                                }
                                            })} */}

                                            {this.props.listSubject.map((subject, index) => {
                                                if (subject.id === professor.subject_id) {
                                                    return (
                                                        <TableCell key={index}>{subject.name}</TableCell>
                                                    )
                                                }
                                            })}
                                            <TableCell >{professor.phone}</TableCell>
                                            <TableCell >{professor.cin}</TableCell>
                                            <TableCell >{professor.address_mail}</TableCell>
                                            <TableCell >{dateFormat(professor.date_start_contract, "fullDate")}</TableCell>
                                            <TableCell >{dateFormat(professor.date_end_contract, "fullDate")}</TableCell>

                                            <TableCell>
                                                <IconButton onClick={this.onOptionMenuSelect.bind(this)}>
                                                    <i className="zmdi zmdi-more-vert" /></IconButton>
                                                <Menu id="long-menu"
                                                    anchorEl={anchorEl}
                                                    open={menuState}
                                                    onClose={this.handleRequestClose.bind(this)}
                                                    MenuListProps={{
                                                        style: {
                                                            width: 150,
                                                            paddingTop: 0,
                                                            paddingBottom: 0
                                                        },
                                                    }}>
                                                    <MenuItem onClick={this.handleEdit} value={professor.id} >{<IntlMessages id="button.modify" />}</MenuItem>
                                                    <MenuItem onClick={this.handleRequestDelete} value={professor.id}>{<IntlMessages id="button.delete" />}</MenuItem>
                                                </Menu>

                                            </TableCell>

                                        </TableRow>
                                    );
                                })}
                            </TableBody>
                        </Table>
                    </div>
                </CardBox>
            </div>
        );
    }
}

export default connect(mapStateToProps, { getData })(ProfessorList);
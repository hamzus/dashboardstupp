
import React from 'react';
import { Modal, ModalBody, ModalHeader } from "reactstrap";
import IntlMessages from 'util/IntlMessages';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Auxiliary from "util/Auxiliary";
import MenuItem from '@material-ui/core/MenuItem';
import { connect } from "react-redux";
import { editData } from "../../../../../actions/professorAction";

const gender = [
    {
        value: 'Masculin',
        label: 'Masculin',
    },
    {
        value: 'Féminin',
        label: 'Féminin',
    }
];

class EditProfessor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            previewVisible: true,
            name: '',
            surname: '',
            gender: '',
            date_of_birth: '',
            date_start_contract: '',
            date_end_contract: '',
            address_mail: '',
            phone: '',
            cin: '',
            status: true,
            subject_id: 0,
            id: 0
        };
        this.handleCancel = this.handleCancel.bind(this);
        this.handleChange = this.handleChange.bind(this);
    };


    componentDidMount() {

        const date_of_birth = this.props.professorItem.date_of_birth;
        const modifDateOfBirth = date_of_birth.split('T');
        const date_start_contract = this.props.professorItem.date_start_contract;
        const modifDateStartContract = date_start_contract.split('T');
        const date_end_contract = this.props.professorItem.date_end_contract;
        const modiDateEndContract = date_end_contract.split('T');



        this.setState({
            name: this.props.professorItem.name,
            surname: this.props.professorItem.surname,
            gender: this.props.professorItem.gender,
            date_of_birth: modifDateOfBirth[0],
            date_start_contract: modifDateStartContract[0],
            date_end_contract: modiDateEndContract[0],
            address_mail: this.props.professorItem.address_mail,
            phone: this.props.professorItem.phone,
            cin: this.props.professorItem.cin,
            subject_id: this.props.professorItem.subject_id,
            id: this.props.professorItem.id
        })
    };

    handleCancel() {
        this.props.cancelModal();
    };
    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    };


    handleToggle() {
        this.props.cancelModal();
    };

    handleSubmit = (e) => {
        e.preventDefault();
        const name = this.state.name;
        const surname = this.state.surname;
        const gender = this.state.gender;
        const date_of_birth = this.state.date_of_birth;
        const date_start_contract = this.state.date_start_contract;
        const date_end_contract = this.state.date_end_contract;
        const address_mail = this.state.address_mail;
        const phone = this.state.phone;
        const cin = this.state.cin;
        const subject_id = this.state.subject_id;
        const id = this.state.id;

        const data = { name, surname, gender, date_of_birth, date_start_contract, date_end_contract, address_mail, phone,cin,subject_id, id };
        this.props.dispatch(editData(data));
        this.props.cancelModal();
    };

    render() {

        return (
            <Auxiliary>
                <Modal isOpen={this.state.previewVisible} toggle={this.handleToggle.bind(this)}>
                    <ModalHeader className="modal-box-header bg-primary text-white" >
                        {<IntlMessages id="modal.modif.professor" />}
                    </ModalHeader>
                    <ModalBody>
                        <form className="row" onSubmit={this.handleSubmit}>

                            <div className="col-sm-6">
                                <TextField
                                    required
                                    id="subject"
                                    select
                                    label={<IntlMessages id="components.professor.formadd.subject_id" />}
                                    value={this.state.subject_id}
                                    onChange={this.handleChange}
                                    SelectProps={{}}
                                    margin="normal"
                                    fullWidth >
                                    {this.props.listSubject.map(subject => (
                                        <MenuItem key={subject.id} value={subject.id}>
                                            {subject.name}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    required
                                    name='name'
                                    id="name"
                                    label={<IntlMessages id="components.professor.formadd.name" />}
                                    onChange={this.handleChange}
                                    value={this.state.name}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    required
                                    name='surname'
                                    id="surname"
                                    label={<IntlMessages id="components.professor.formadd.surname" />}
                                    onChange={this.handleChange}
                                    value={this.state.surname}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    required
                                    name="gender"
                                    id="gender"
                                    select
                                    label={<IntlMessages id="components.student.formadd.gender" />}
                                    SelectProps={{}}
                                    onChange={this.handleChange}
                                    value={this.state.gender}
                                    margin="normal"
                                    fullWidth >
                                    {gender.map(option => (
                                        <MenuItem key={option.value} value={option.value}>
                                            {option.label}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    name='address_mail'
                                    id="address_mail"
                                    type="email"
                                    label={<IntlMessages id="components.professor.formadd.address_mail" />}
                                    onChange={this.handleChange}
                                    value={this.state.address_mail}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>

                            <div className="col-sm-6">
                                <TextField
                                    required
                                    id="phone"
                                    onChange={this.handleChange}
                                    value={this.state.phone}
                                    label={<IntlMessages id="components.student.formadd.phone" />}
                                    type="number"
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    required
                                    id="cin"
                                    onChange={this.handleChange}
                                    value={this.state.cin}
                                    label={<IntlMessages id="components.student.formadd.cin" />}
                                    type="number"
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    name="date_of_birth"
                                    id="date_of_birth"
                                    type="date"
                                    helperText={<IntlMessages id="components.student.formadd.date_of_birth" />}
                                    onChange={this.handleChange}
                                    value={this.state.date_of_birth}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    name="date_start_contract"
                                    id="date_start_contract"
                                    type="date"
                                    helperText={<IntlMessages id="components.professor.formadd.date_start_contract" />}
                                    onChange={this.handleChange}
                                    value={this.state.date_start_contract}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    name="date_end_contract"
                                    id="date_end_contract"
                                    type="date"
                                    helperText={<IntlMessages id="components.professor.formadd.date_end_contract" />}
                                    onChange={this.handleChange}
                                    value={this.state.date_end_contract}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>

                            <br /><br /><br /><br /><br />
                            <div className="col-sm-12">
                                <h4><font color="red">*</font> {<IntlMessages id="component.required_fields" />}</h4>
                            </div>
                            <div className="col-md-12 text-right ">
                                <br /><br />
                                <Button variant="contained" className="jr-btn bg-indigo text-white " type="submit" >{<IntlMessages id="components.establishments.formModify.buttonModify" />}</Button>
                                <Button variant="contained" className="jr-btn bg-grey text-white " onClick={this.handleCancel}>{<IntlMessages id="components.establishments.formadd.buttonCancel" />}</Button>
                            </div>

                        </form>
                    </ModalBody>
                </Modal>
            </Auxiliary>
        )
    }
} export default connect()(EditProfessor); 
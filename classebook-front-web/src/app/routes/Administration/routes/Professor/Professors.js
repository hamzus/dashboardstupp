import React from 'react';
import ContainerHeader from 'components/ContainerHeader/index';
import IntlMessages from 'util/IntlMessages';
import ListProfessor from './ListProfessor.jsx';
import AddProfessor from './AddProfessor';
import DeleteProfessor from './DeleteProfessor'
import EditProfessor from './EditProfessor';
import axios from 'axios';
import baseUrl from 'config/config';
import { connect } from "react-redux";
import { getData } from "../../../../../actions/professorAction";
import Input from '@material-ui/icons/Input';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

function mapStateToProps(state) {
    return {
        professors: state.professor.remoteProfessors,
        establishments: state.establishment.remoteEstablishments

    };
}
class Professors extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            addProfessorModal: false,
            professorsList: [],
            listEstablishment: [],
            listSubject: [],
            professorItem: [],
            edit: false,
            modalDelete: false,
            itemId: 0
        };
        this.requestDeleteProfessor = this.requestDeleteProfessor.bind(this);
        this.handleCancelModalDelete = this.handleCancelModalDelete.bind(this);
        this.handleCancelModal = this.handleCancelModal.bind(this);
        this.editItemProfessor = this.editItemProfessor.bind(this);
        this.addProfessor = this.addProfessor.bind(this);
    }

    addProfessor() {
        this.setState({ addProfessorModal: true });
    }

    requestDeleteProfessor(id) {
        this.setState({ modalDelete: true, itemId: id });
    };
    componentDidMount() {
        this.props.getData();
        this.props.getEstablishment();
    };

    handleCancelModal() {
        this.setState({ edit: false ,addProfessorModal: false })
    };
    componentDidMount() {
        this.props.getData();
        axios.get(`${baseUrl.baseUrl}/establishments`)
            .then(res => {
                const establishmentList = res.data;
                const listEstablishment = establishmentList.filter(element => element.status);
                this.setState({ listEstablishment });
            });

        axios.get(`${baseUrl.baseUrl}/subjects`)
            .then(res => {
                const subjectList = res.data;
                const listSubject = subjectList.filter(element => element.status);
                this.setState({ listSubject });
            });
    };
    handleCancelModalDelete() {
        this.setState({ modalDelete: false, itemId: 0 })
    };


    editItemProfessor(id) {
        const professorItem = this.props.professors.find(element => element.id == id);
        this.setState({ professorItem: professorItem, edit: true });
    };

    render() {
        return (
            <div className="app-wrapper">
                <ContainerHeader match={this.props.match} title={<IntlMessages id="pages.professorPage" />} />
                <div className="col-md-12 text-right ">
                    <Fab size="small" color="primary" aria-label="Add" onClick={this.addProfessor}>
                        <AddIcon />
                    </Fab>
                    &nbsp;&nbsp;&nbsp;
                        <Fab size="small" color="primary" >
                        <Input />
                    </Fab>
                </div>
                <ListProfessor listEstablishment={this.state.listEstablishment} listSubject={this.state.listSubject} editProfessor={this.editItemProfessor} requestDeleteProfessor={this.requestDeleteProfessor} />
                {this.state.addProfessorModal ?  <AddProfessor listEstablishment={this.state.listEstablishment} listSubject={this.state.listSubject} cancelModal={this.handleCancelModal}/>: ''}
                {this.state.edit ? <EditProfessor professorItem={this.state.professorItem} listEstablishment={this.state.listEstablishment} listSubject={this.state.listSubject} cancelModal={this.handleCancelModal} /> : ''}
                {this.state.modalDelete ? <DeleteProfessor cancelModalDelete={this.handleCancelModalDelete} itemId={this.state.itemId} /> : ''}

            </div>
        )
    }
}

export default connect(mapStateToProps, { getData })(Professors);
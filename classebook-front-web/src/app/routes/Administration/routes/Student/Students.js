import React from 'react';
import ContainerHeader from 'components/ContainerHeader/index';
import IntlMessages from 'util/IntlMessages';
import StudentList from './StudentList';
import AddStudent from './AddStudent';
import EditStudent from './EditStudent';
import axios from 'axios';
import ModalDeleteStudent from './ModalDeleteStudent';
import baseUrl from 'config/config';
import { UncontrolledAlert } from 'reactstrap';
import { connect } from "react-redux";
import { getEstablishment } from "../../../../../actions/establishmentAction";
import Input from '@material-ui/icons/Input';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

class Students extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            addStudentModal: false,
            edit: false,
            modal_delete: false,
            item_id: 0,
            Students_list: [],
            student_Item: [],
            dataParent: [],
            id_parent: '',
            alert_success: false,
            estab: []
        };

        this.EditItemStudent = this.EditItemStudent.bind(this);
        this.handleCancelModal = this.handleCancelModal.bind(this);
        this.RequestDeleteStudent = this.RequestDeleteStudent.bind(this);
        this.handleAnnuleModalDelete = this.handleAnnuleModalDelete.bind(this);
        this.AddItemStudent = this.AddItemStudent.bind(this);
        this.deleteItemStudent = this.deleteItemStudent.bind(this);
        this.handleSubmitEdit = this.handleSubmitEdit.bind(this);
        this.addStudent = this.addStudent.bind(this);
        this.sendMailActivation = this.sendMailActivation.bind(this);
    };

    addStudent() {
        this.setState({ addStudentModal: true });
    };

    handleSubmitEdit(student) {
        axios.put(`${baseUrl.baseUrl}/students/` + student.id, {
            name: student.name,
            surname: student.surname,
            gender: student.gender,
            cin: student.cin,
            zip_code: student.zip_code,
            date_of_birth: student.date_of_birth,
            phone: student.phone,
            id: student.id,
            parent_id: student.parent_id,
            address_mail: student.address_mail,
            address: student.addressStudent
        })
            .then(res => {
                this.setState({
                    Students_list: [...this.state.Students_list.filter(element => element.id != student.id), res.data]
                })
            }).catch(function (error) {
                alert('error')
            });

        axios.put(`${baseUrl.baseUrl}/parents/` + student.parent_id, {
            name: student.name_parent,
            surname: student.surname_parent,
            gender: student.gender_parent,
            date_of_birth: student.date_of_birth_parent,
            phone: student.phone_parent,
            zip_code: student.zip_code_parent,
            cin: student.cin_parent,
            id: student.parent_id,
            address_mail: student.address_mail_parent,
            address: student.addressParent
        })
            .then(res => {
                this.setState({
                    edit: false
                })
            }).catch(function (error) {
                alert('error')
            });

    }
      sendMailActivation(id){
    
        axios.get(`${baseUrl.baseUrl}/users/mailcreation/`+ id).then(res=>{
         })
    
    }
    AddItemStudent(student) {
        /////////////////////////////////add user parent//////////
        axios.post(`${baseUrl.baseUrl}/Users`, {
            realm: student.name_parent,
            username: student.surname_parent,
            password: student.surname_parent + '123',
            email: student.address_mail_parent,
            emailVerified: true
        })
            .then(res => {
                this.sendMailActivation(res.data.id)
                //////////////////////add profile parent///////
                axios.post(`${baseUrl.baseUrl}/profiles`, {
                    user_id: res.data.id,
                    role_id: 4,
                    establishment_id: student.establishment
                })
                    .then(res => {
                        axios.post(`${baseUrl.baseUrl}/parents`, {
                            name: student.name_parent,
                            surname: student.surname_parent,
                            gender: student.gender_parent,
                            phone: student.phone_parent,
                            zip_code: student.zip_code_parent,
                            cin: student.cin_parent,
                            date_of_birth: student.date_of_birth_parent,
                            profile_id: res.data.id,
                            address_mail: student.address_mail_parent,
                            address: student.addressParent
                        })
                            .then(res => {
                                axios.get(`${baseUrl.baseUrl}/parents`)
                                    .then(res => {
                                        const length_list = res.data.length;
                                        const id_parent = res.data[length_list - 1].id;
                                        this.setState({ id_parent });
                                        /////////////////////////////////add user student//////////
                                        axios.post(`${baseUrl.baseUrl}/Users`, {
                                            realm: student.name,
                                            username: student.surname,
                                            password: student.surname + '123',
                                            email: student.address_mail,
                                            emailVerified: true
                                        })
                                            .then(res => {
                                                this.sendMailActivation(res.data.id)

                                                //////////////////////add profile student///////
                                                axios.post(`${baseUrl.baseUrl}/profiles`, {
                                                    user_id: res.data.id,
                                                    role_id: 5,
                                                    establishment_id: student.establishment
                                                })
                                                    .then(res => {
                                                        axios.post(`${baseUrl.baseUrl}/students`, {
                                                            name: student.name,
                                                            surname: student.surname,
                                                            gender: student.gender,
                                                            cin: student.cin,
                                                            zip_code: student.zip_code,
                                                            date_of_birth: student.date_of_birth,
                                                            phone: student.phone,
                                                            parent_id: id_parent,
                                                            class_id: student.class_id,
                                                            profile_id: res.data.id,
                                                            address_mail: student.address_mail,
                                                            address: student.addressStudent
                                                        })
                                                            .then(res => {

                                                                this.setState({
                                                                    Students_list: [...this.state.Students_list, res.data],
                                                                    alert_success: true
                                                                })
                                                            }).catch(function (error) {
                                                                alert('Merci de vérifier votre champ obligatoire')
                                                            });
                                                    }).catch(function (error) {

                                                    });
                                            }).catch(function (error) {

                                            });
                                    }).catch(function (error) {
                                        alert('error')
                                    });
                                ///////////////////////////////////////////
                            }).catch(function (error) {
                                alert('Merci de vérifier votre champ obligatoire')

                            });
                    }).catch(function (error) {

                    });
            }).catch(function (error) {

            });


    }
    deleteItemStudent = (idItem) => {
        axios.get(`${baseUrl.baseUrl}/students/` + idItem)
            .then(res => {
                const data_student = res.data;
                axios.put(`${baseUrl.baseUrl}/students/` + data_student.id, {
                    name: data_student.name,
                    surname: data_student.surname,
                    gender: data_student.gender,
                    cin: data_student.cin,
                    zip_code: data_student.zip_code,
                    date_of_birth: data_student.date_of_birth,
                    phone: data_student.phone,
                    status: false,
                    parent_id: data_student.parent_id,
                    id: data_student.id,
                    address_mail: data_student.address_mail,
                    address: data_student.addressStudent

                })
                    .then(res => {
                        this.setState({
                            Students_list: [...this.state.Students_list.filter(element => element.id != data_student.id)],
                        })
                    }).catch(function (error) {
                        alert('error')
                    });
            });

    }

    handleCancelModal() {
        this.setState({ edit: false, addStudentModal: false })
    };

    EditItemStudent(id) {
        const student_Item = this.state.Students_list.find(element => element.id == id);
        this.setState({ student_Item }, function () {
            axios.get(`${baseUrl.baseUrl}/parents/` + this.state.student_Item.parent_id)
                .then(res => {
                    const dataParent = res.data;
                    this.setState({ dataParent, edit: true })
                });

        });
    };

    componentDidMount() {
        this.props.dispatch(getEstablishment());
        this.setState({
            estab: this.props.establishments
        })

        axios.get(`${baseUrl.baseUrl}/students`)
            .then(res => {
                const Students_lists = res.data;
                const Students_list = Students_lists.filter(element => element.status);
                this.setState({ Students_list: Students_list, estab: this.props.establishments });
            });
    };

    RequestDeleteStudent(id) {
        this.setState({ modal_delete: true, item_id: id }, function () {
        });
    };

    handleAnnuleModalDelete() {
        this.setState({ modal_delete: false, item_id: 0 })
    };

    render() {
        return (
            <div className="app-wrapper">
                <ContainerHeader match={this.props.match} title={<IntlMessages id="pages.studentPage" />} />
                <div className="col-md-12 text-right ">
                    <Fab size="small" color="primary" aria-label="Add" onClick={this.addStudent}>
                        <AddIcon />
                    </Fab>
                    &nbsp;&nbsp;&nbsp;
                        <Fab size="small" color="primary" >
                        <Input />
                    </Fab>
                </div>
                <StudentList estab={this.props.establishments} students={this.state.Students_list} editStudent={this.EditItemStudent} RequestDeleteStudent={this.RequestDeleteStudent} />
                {this.state.addStudentModal ? <AddStudent handleSubmit={this.AddItemStudent} cancelModal={this.handleCancelModal} /> : ''}
                {this.state.alert_success ? <UncontrolledAlert className="alert-addon-card bg-success bg-success text-white shadow-lg">
                    <span className="icon-addon alert-addon">
                        <i className="zmdi zmdi-cloud-done zmdi-hc-fw zmdi-hc-lg" />
                    </span>
                    <span className="d-inline-block"> {<IntlMessages id="message.success" />} </span>
                </UncontrolledAlert> : ''}
                {this.state.edit ? <EditStudent student={this.state.student_Item} parent={this.state.dataParent} cancelModal={this.handleCancelModal} handleSubmit={this.handleSubmitEdit} /> : ""}
                {this.state.modal_delete ? <ModalDeleteStudent item_id={this.state.item_id} annuleModalDelete={this.handleAnnuleModalDelete} handleDelete={this.deleteItemStudent} /> : ''}
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        establishments: state.establishment.remoteEstablishments,
    }
}
export default connect(mapStateToProps)(Students);
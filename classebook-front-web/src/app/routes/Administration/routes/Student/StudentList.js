import React from 'react';
import IntlMessages from 'util/IntlMessages';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import CardBox from 'components/CardBox/index';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import baseUrl from 'config/config';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';

function jsUpperCaseFirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
var dateFormat = require('dateformat');
class StudentList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            previewVisible: false,
            rechercheListProfile: [],
            listEtab: [],
            anchorEl: undefined,
            menuState: false,
            establishment_id: 0,
            class_id: 0,
            classListFiltered: [],
            studentListFilteredByClass: [],
            establishmentList: [],
            classesList: [],
        };
        this.handleEdit = this.handleEdit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleAnnule = this.handleAnnule.bind(this);
        this.handleRequestDelete = this.handleRequestDelete.bind(this);
        this.getEstablishmentName = this.getEstablishmentName.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange = name => event => {
        this.setState({ [name]: event.target.value });
        if (name == 'establishment_id') {
            const listReturned = this.state.classesList.filter(classe => (event.target.value == classe.establishment_id));
            this.setState({ classListFiltered: listReturned });
        }
        if (name == 'class_id') {
            const listReturned = this.props.students.filter(student => (event.target.value == student.class_id));
            this.setState({ studentListFilteredByClass: listReturned });
        }
    };

    onOptionMenuSelect = event => {
        this.setState({ menuState: true, anchorEl: event.currentTarget });
    };
    handleRequestClose = () => {
        this.setState({ menuState: false });
    };

    handleEdit(e) {
        e.preventDefault();
        this.props.editStudent(e.currentTarget.value);
        this.setState({ menuState: false });
    }

    handleDelete(e) {
        e.preventDefault();
    }

    handleRequestDelete(e) {
        e.preventDefault();
        this.props.RequestDeleteStudent(e.currentTarget.value);
        this.setState({ menuState: false });
    }

    handleAnnule() {
        this.props.annuleModal();
    }

    handleToggle() {
        this.props.annuleModal();
    }

    componentWillMount() {
        axios.get(`${baseUrl.baseUrl}/establishments`)
            .then(res => {
                const establishmentList = res.data;
                const listEstablishment = establishmentList.filter(element => element.status);
                this.setState({ establishmentList: listEstablishment });
            });
        axios.get(`${baseUrl.baseUrl}/profiles`)
            .then(res => {
                const rechercheListProfile = res.data;
                this.setState({ rechercheListProfile });
            });

        axios.get(`${baseUrl.baseUrl}/classes`)
            .then(res => {
                const classList = res.data;
                const classesList = classList.filter(element => element.status);
                this.setState({ classesList : classesList });
            });
    }

    getEstablishmentName(id) {
        const list = this.state.rechercheListProfile.map(profile => {

            if (profile.id == id) {
                let estabFilter;
                return estabFilter = this.props.estab.map(establishment => {

                    if (establishment.id === profile.establishment_id) {
                        return establishment.name
                    }
                })
            }
        }
        )
        return list
    }

    render() {
        const students = this.state.studentListFilteredByClass;
        const { anchorEl, menuState } = this.state;
        return (
            <div className="app-wrapper">
                <CardBox styleName="col-lg-12" >
                    <div className="row">
                        <div className="col-sm-6">
                            <TextField
                                required
                                id="establishment"
                                select
                                label={<IntlMessages id="components.student.formadd.establishment" />}
                                value={this.state.establishment_id}
                                onChange={this.handleChange('establishment_id')}
                                SelectProps={{}}
                                margin="normal"
                                fullWidth >
                                {this.state.establishmentList.map(establishment => (
                                    <MenuItem key={establishment.id} value={establishment.id}>
                                        {establishment.name}
                                    </MenuItem>
                                ))}
                            </TextField>
                        </div>
                        <div className="col-sm-6">
                            <TextField
                                required
                                id="class_id"
                                select
                                label={<IntlMessages id="sidebar.classes" />}
                                value={this.state.class_id}
                                onChange={this.handleChange('class_id')}
                                SelectProps={{}}
                                margin="normal"
                                fullWidth >
                                {this.state.classListFiltered.map(element => (
                                    <MenuItem key={element.id} value={element.id}>
                                        {element.name}
                                    </MenuItem>
                                ))}
                            </TextField>
                        </div>
                    </div>
                </CardBox>
                <CardBox styleName="col-lg-12" >
                    <div className="table-responsive-material">
                        <Table className="default-table table-unbordered table table-sm table-hover">
                            <TableHead className="th-border-b">
                                <TableRow>
                                    <TableCell>{<IntlMessages id="components.student.formadd.name" />}</TableCell>
                                    <TableCell >{<IntlMessages id="components.student.formadd.surname" />}</TableCell>
                                    <TableCell >{<IntlMessages id="components.student.formadd.date_of_birth" />}</TableCell>
                                    <TableCell >{<IntlMessages id="components.student.formadd.cin" />}</TableCell>
                                    <TableCell >{<IntlMessages id="components.student.formadd.zip_code" />}</TableCell>
                                    <TableCell >{<IntlMessages id="components.student.formadd.addressStudent" />}</TableCell>
                                    <TableCell >{<IntlMessages id="components.student.formadd.etablissement" />}</TableCell>
                                    <TableCell >{<IntlMessages id="components.profile.email" />}</TableCell>
                                    <TableCell></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {students.map(student => {
                                    return (
                                        <TableRow key={student.id}>
                                            <TableCell >{student.name.toUpperCase()}</TableCell>
                                            <TableCell >{jsUpperCaseFirst(student.surname)}</TableCell>
                                            <TableCell >{dateFormat(student.date_of_birth, "fullDate")}</TableCell>
                                            <TableCell >{student.cin}</TableCell>
                                            <TableCell >{student.zip_code}</TableCell>
                                            <TableCell >{student.address}</TableCell>
                                            <TableCell >{this.getEstablishmentName(student.profile_id)}
                                            </TableCell>
                                            <TableCell >{student.address_mail}</TableCell>


                                            <TableCell>
                                                <IconButton onClick={this.onOptionMenuSelect.bind(this)}>
                                                    <i className="zmdi zmdi-more-vert" /></IconButton>
                                                <Menu id="long-menu"
                                                    anchorEl={anchorEl}
                                                    open={menuState}
                                                    onClose={this.handleRequestClose.bind(this)}
                                                    MenuListProps={{
                                                        style: {
                                                            width: 150,
                                                            paddingTop: 0,
                                                            paddingBottom: 0
                                                        },
                                                    }}>
                                                    <MenuItem onClick={this.handleEdit} value={student.id}>{<IntlMessages id="button.modify" />}</MenuItem>
                                                    <MenuItem onClick={this.handleRequestDelete} value={student.id}>{<IntlMessages id="button.delete" />}</MenuItem>
                                                </Menu>

                                            </TableCell>

                                        </TableRow>
                                    );

                                })}
                            </TableBody>
                        </Table>
                    </div>
                </CardBox>

            </div>
        );
    }
}

export default StudentList;
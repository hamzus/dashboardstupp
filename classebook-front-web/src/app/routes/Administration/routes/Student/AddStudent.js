import React from 'react';
import IntlMessages from 'util/IntlMessages';
import TextField from '@material-ui/core/TextField';
import CardBox from 'components/CardBox/index';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import baseUrl from 'config/config';
import axios from 'axios';
import Auxiliary from "util/Auxiliary";
import { Modal, ModalBody, ModalHeader } from "reactstrap";

const gender = [
    {
        value: 'Masculin',
        label: 'Masculin',
    },
    {
        value: 'Féminin',
        label: 'Féminin',
    }
];
class AddStudent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            previewVisible: true,
            name: '',
            surname: '',
            gender: '',
            cin: '',
            zip_code: '',
            addressParent: '',
            addressStudent: '',
            date_of_birth: '',
            phone: '',
            name_parent: '',
            surname_parent: '',
            gender_parent: '',
            phone_parent: '',
            zip_code_parent: '',
            cin_parent: '',
            date_of_birth_parent: '',
            ListEstablishment: [],
            establishment: '',
            class_id: '',
            ListClasse: [],
            address_mail: '',
            address_mail_parent: ''

        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }
    handleCancel() {

        this.setState({
            previewVisible: false,
            name: '',
            surname: '',
            gender: '',
            cin: '',
            zip_code: '',
            date_of_birth: '',
            phone: '',
            name_parent: '',
            surname_parent: '',
            gender_parent: '',
            phone_parent: '',
            zip_code_parent: '',
            cin_parent: '',
            date_of_birth_parent: '',
            class_id: '',
            address_mail: '',
            address_mail_parent: '',
            addressParent: '',
            addressStudent: ''
        });

        this.props.cancelModal();
    };
    handleChange = name => event => {

        this.setState({
            [name]: event.target.value,
        });

    };
    handleSubmit = (e) => {
        e.preventDefault();
        const establishment = this.state.establishment;
        const name = this.state.name;
        const surname = this.state.surname;
        const gender = this.state.gender;
        const cin = this.state.cin;
        const zip_code = this.state.zip_code;
        const date_of_birth = this.state.date_of_birth;
        const phone = this.state.phone;
        const name_parent = this.state.name_parent;
        const surname_parent = this.state.surname_parent;
        const gender_parent = this.state.gender_parent;
        const phone_parent = this.state.phone_parent;
        const zip_code_parent = this.state.zip_code_parent;
        const cin_parent = this.state.cin_parent;
        const date_of_birth_parent = this.state.date_of_birth_parent;
        const class_id = this.state.class_id;
        const address_mail = this.state.address_mail;
        const address_mail_parent = this.state.address_mail_parent;
        const addressParent = this.state.addressParent;
        const addressStudent = this.state.addressStudent;

        const data = {
            establishment, name, surname, gender, zip_code_parent, zip_code, addressParent,
            addressStudent, cin, date_of_birth, phone, phone, name_parent, surname_parent, gender_parent,
            phone_parent, date_of_birth_parent, cin_parent, class_id, address_mail, address_mail_parent
        }
        this.props.handleSubmit(data);
        this.setState({
            previewVisible: false,
            establishment: '',
            name: '',
            surname: '',
            gender: '',
            cin: '',
            zip_code: '',
            date_of_birth: '',
            phone: '',
            name_parent: '',
            surname_parent: '',
            gender_parent: '',
            phone_parent: '',
            zip_code_parent: '',
            cin_parent: '',
            date_of_birth_parent: '',
            class_id: '',
            address_mail: '',
            address_mail_parent: '',
            addressParent: '',
            addressStudent: ''

        });
        this.props.cancelModal();
    }
    componentWillMount() {
        axios.get(`${baseUrl.baseUrl}/establishments`)
            .then(res => {
                const ListEstablishments = res.data;
                const ListEstablishment = ListEstablishments.filter(element => element.status);
                this.setState({ ListEstablishment });
            });

        axios.get(`${baseUrl.baseUrl}/classes`)
            .then(res => {
                const ListClasses = res.data;
                const ListClasse = ListClasses.filter(element => element.status);
                this.setState({ ListClasse });
            });
    }
    render() {
        const ListEstablishment = this.state.ListEstablishment;
        const ListClasse = this.state.ListClasse;
        return (
            <div className="app-wrapper">
                <Auxiliary>

                    <Modal isOpen={this.state.previewVisible} >
                        <ModalHeader toggle={this.handleCancel} className="modal-box-header bg-primary text-white" >{<IntlMessages id="pages.studentPage" />}</ModalHeader>
                        <ModalBody>
                            <form className="row" autoComplete="off" onSubmit={this.handleSubmit}>
                                <div className="col-sm-12">
                                    <h1>{<IntlMessages id="component.student.info.general" />}</h1>
                                </div>
                                <div className="col-sm-6">
                                    <TextField
                                        required
                                        name='establishment'
                                        id="establishment"
                                        select
                                        label={<IntlMessages id="components.student.formadd.establishment" />}
                                        value={this.state.establishment}
                                        onChange={this.handleChange('establishment')}
                                        SelectProps={{}}
                                        margin="normal"
                                        fullWidth >
                                        {ListEstablishment.map(establishment => (
                                            <MenuItem key={establishment.id} value={establishment.id}>
                                                {establishment.name}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                </div>

                                <div className="col-sm-6">
                                    <TextField
                                        required
                                        name='class_id'
                                        id="class_id"
                                        select
                                        label={<IntlMessages id="components.student.formadd.classe" />}
                                        value={this.state.class_id}
                                        onChange={this.handleChange('class_id')}
                                        SelectProps={{}}
                                        margin="normal"
                                        fullWidth >
                                        {ListClasse.map(classe => (
                                            <MenuItem key={classe.id} value={classe.id}>
                                                {classe.name}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="col-sm-6">
                                    <TextField
                                        required
                                        name='name'
                                        id="name"
                                        label={<IntlMessages id="components.student.formadd.name" />}
                                        onChange={this.handleChange('name')}
                                        value={this.state.name}
                                        margin="normal"
                                        fullWidth
                                    />
                                </div>
                                <div className="col-sm-6">
                                    <TextField
                                        required
                                        name='surname'
                                        id="surname"
                                        label={<IntlMessages id="components.student.formadd.surname" />}
                                        onChange={this.handleChange('surname')}
                                        value={this.state.surname}
                                        margin="normal"
                                        fullWidth
                                    />
                                </div>
                                <div className="col-sm-6">
                                    <TextField
                                        required
                                        name="address_mail"
                                        id="address_mail"
                                        type="email"
                                        label={<IntlMessages id="components.profile.email" />}
                                        onChange={this.handleChange('address_mail')}
                                        value={this.state.address_mail}
                                        margin="normal"
                                        fullWidth >
                                    </TextField>
                                </div>
                                <div className="col-sm-6">
                                    <TextField

                                        name="gender"
                                        id="gender"
                                        select
                                        label={<IntlMessages id="components.student.formadd.gender" />}
                                        SelectProps={{}}
                                        onChange={this.handleChange('gender')}
                                        value={this.state.gender}
                                        margin="normal"
                                        fullWidth >
                                        {gender.map(option => (
                                            <MenuItem key={option.value} value={option.value}>
                                                {option.label}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="col-sm-6">
                                    <TextField
                                        name='cin'
                                        id="cin"
                                        label={<IntlMessages id="components.student.formadd.cin" />}
                                        onChange={this.handleChange('cin')}
                                        value={this.state.cin}
                                        type="number"
                                        margin="normal"
                                        fullWidth
                                    />
                                </div>
                                <div className="col-sm-6">
                                    <TextField
                                        name='zip_code'
                                        id="zip_code"
                                        label={<IntlMessages id="components.student.formadd.zip_code" />}
                                        onChange={this.handleChange('zip_code')}
                                        value={this.state.zip_code}
                                        type="number"
                                        margin="normal"
                                        fullWidth
                                    />
                                </div>
                                <div className="col-sm-6">
                                    <TextField
                                        name='addressStudent'
                                        id="addressStudent"
                                        label={<IntlMessages id="components.student.formadd.addressStudent" />}
                                        onChange={this.handleChange('addressStudent')}
                                        value={this.state.addressStudent}
                                        margin="normal"
                                        fullWidth
                                    />
                                </div>
                                <div className="col-sm-6">
                                    <TextField
                                        name="phone"
                                        id="phone"
                                        label={<IntlMessages id="components.student.formadd.phone" />}
                                        type="number"
                                        onChange={this.handleChange('phone')}
                                        value={this.state.phone}
                                        margin="normal"
                                        fullWidth
                                    />
                                </div>
                                <div className="col-sm-6">
                                    <TextField
                                        name="date_of_birth"
                                        id="date_of_birth"
                                        type="date"
                                        helperText={<IntlMessages id="components.student.formadd.date_of_birth" />}
                                        onChange={this.handleChange('date_of_birth')}
                                        value={this.state.date_of_birth}
                                        margin="normal"
                                        fullWidth
                                    />
                                </div>

                                {/* //////////////// data parents///////////// */}
                                <br /><br /><br /><br /><br />
                                <div className="col-sm-12">
                                    <h1>{<IntlMessages id="component.student.info.parents" />}</h1>
                                </div>
                                <div className="col-sm-6">
                                    <TextField
                                        required
                                        name='name_parent'
                                        id="name_parent"
                                        label={<IntlMessages id="components.student.formadd.name" />}
                                        onChange={this.handleChange('name_parent')}
                                        value={this.state.name_parent}
                                        margin="normal"
                                        fullWidth
                                    />
                                </div>


                                <div className="col-sm-6">
                                    <TextField
                                        required
                                        name='surname_parent'
                                        id="surname_parent"
                                        label={<IntlMessages id="components.student.formadd.surname" />}
                                        onChange={this.handleChange('surname_parent')}
                                        value={this.state.surname_parent}
                                        margin="normal"
                                        fullWidth
                                    />
                                </div>
                                <div className="col-sm-6">
                                    <TextField
                                        required
                                        name="address_mail_parent"
                                        id="address_mail_parent"
                                        type="email"
                                        label={<IntlMessages id="components.profile.email" />}
                                        onChange={this.handleChange('address_mail_parent')}
                                        value={this.state.address_mail_parent}
                                        margin="normal"
                                        fullWidth >
                                    </TextField>
                                </div>
                                <div className="col-sm-6">
                                    <TextField

                                        name="gender_parent"
                                        id="gender_parent"
                                        select
                                        label={<IntlMessages id="components.student.formadd.gender" />}
                                        SelectProps={{}}
                                        onChange={this.handleChange('gender_parent')}
                                        value={this.state.gender_parent}
                                        margin="normal"
                                        fullWidth >
                                        {gender.map(option => (
                                            <MenuItem key={option.value} value={option.value}>
                                                {option.label}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="col-sm-6">
                                    <TextField
                                        name="phone_parent"
                                        id="phone_parent"
                                        label={<IntlMessages id="components.student.formadd.phone" />}
                                        type="number"
                                        onChange={this.handleChange('phone_parent')}
                                        value={this.state.phone_parent}
                                        margin="normal"
                                        fullWidth
                                    />
                                </div>
                                <div className="col-sm-6">
                                    <TextField
                                        name="zip_code_parent"
                                        id="zip_code_parent"
                                        label={<IntlMessages id="components.student.formadd.zip_code" />}
                                        type="number"
                                        onChange={this.handleChange('zip_code_parent')}
                                        value={this.state.zip_code_parent}
                                        margin="normal"
                                        fullWidth
                                    />
                                </div>
                                <div className="col-sm-6">
                                    <TextField
                                        name="addressParent"
                                        id="addressParent"
                                        label={<IntlMessages id="components.student.formadd.addressStudent" />}
                                        onChange={this.handleChange('addressParent')}
                                        value={this.state.addressParent}
                                        margin="normal"
                                        fullWidth
                                    />
                                </div>
                                <div className="col-sm-6">
                                    <TextField
                                        name="cin_parent"
                                        id="cin_parent"
                                        label={<IntlMessages id="components.student.formadd.cin" />}
                                        type="number"
                                        onChange={this.handleChange('cin_parent')}
                                        value={this.state.cin_parent}
                                        margin="normal"
                                        fullWidth
                                    />
                                </div>
                                <div className="col-sm-6">
                                    <TextField
                                        name="date_of_birth_parent"
                                        id="date_of_birth_parent"
                                        type="date"
                                        helperText={<IntlMessages id="components.student.formadd.date_of_birth" />}
                                        onChange={this.handleChange('date_of_birth_parent')}
                                        value={this.state.date_of_birth_parent}
                                        margin="normal"
                                        fullWidth
                                    />
                                </div>



                                {/* //////////////// end///////////////////////// */}
                                <br /><br /><br /><br /><br />
                                <div className="col-sm-12">
                                    <h4><font color="red">*</font> {<IntlMessages id="component.required_fields" />}</h4>
                                </div>
                                <div className="col-md-12 text-right ">
                                    <br /><br />
                                    <Button variant="contained" className="jr-btn bg-indigo text-white " type="submit" >{<IntlMessages id="components.establishments.formadd.buttonAdd" />}</Button>
                                    <Button variant="contained" className="jr-btn bg-grey text-white " onClick={this.handleCancel}>{<IntlMessages id="components.establishments.formadd.buttonCancel" />}</Button>
                                </div>
                            </form>

                        </ModalBody>
                    </Modal>
                </Auxiliary>
            </div>
        );
    }
}

export default connect()(AddStudent);
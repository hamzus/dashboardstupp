import React from 'react';
import { Modal, ModalBody, ModalHeader } from "reactstrap";
import IntlMessages from 'util/IntlMessages';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Auxiliary from "util/Auxiliary";
import MenuItem from '@material-ui/core/MenuItem';


const gender = [
    {
        value: 'Masculin',
        label: 'Masculin',
    },
    {
        value: 'Féminin',
        label: 'Féminin',
    }
];

class EditStudent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            previewVisible: false,
            name: '',
            surname: '',
            gender: '',
            cin: '',
            zip_code: '',
            addressParent: '',
            addressStudent: '',
            date_of_birth: '',
            phone: '',
            id: 0,
            parent_id: 0,
            name_parent: '',
            gender_parent: '',
            surname_parent: '',
            phone_parent: '',
            zip_code_parent: '',
            cin_parent: '',
            date_of_birth_parent: '',
            address_mail: '',
            address_mail_parent: ''
        };
        this.handleAnnule = this.handleAnnule.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    componentDidMount() {
        const dateOfBirth = this.props.student.date_of_birth;
        const dateBirth = dateOfBirth.split('T');
        const dateOfBirthParent = this.props.parent.date_of_birth;
        const dateBirthParent = dateOfBirthParent.split('T');
        console.log(dateBirth);

        this.setState({ previewVisible: true });
        this.setState({
            name: this.props.student.name,
            surname: this.props.student.surname,
            gender: this.props.student.gender,
            cin: this.props.student.cin,
            zip_code: this.props.student.zip_code,
            date_of_birth: dateBirth[0],
            phone: this.props.student.phone,
            parent_id: this.props.student.parent_id,
            id: this.props.student.id,
            name_parent: this.props.parent.name,
            gender_parent: this.props.parent.gender,
            surname_parent: this.props.parent.surname,
            phone_parent: this.props.parent.phone,
            zip_code_parent: this.props.parent.zip_code,
            cin_parent: this.props.parent.cin,
            date_of_birth_parent: dateBirthParent[0],
            address_mail: this.props.student.address_mail,
            address_mail_parent: this.props.parent.address_mail,
            addressParent: this.props.parent.address,
            addressStudent: this.props.student.address
        });

    };

    handleAnnule() {
        this.props.cancelModal();
    };

    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    };

    handleToggle() {
        this.props.cancelModal();
    };

    handleSubmit = (e) => {
        e.preventDefault();
        const data = {
            name: this.state.name,
            surname: this.state.surname,
            gender: this.state.gender,
            cin: this.state.cin,
            zip_code: this.state.zip_code,
            date_of_birth: this.state.date_of_birth,
            phone: this.state.phone,
            id: this.state.id,
            parent_id: this.state.parent_id,
            name_parent: this.state.name_parent,
            gender_parent: this.state.gender_parent,
            surname_parent: this.state.surname_parent,
            phone_parent: this.state.phone_parent,
            zip_code_parent: this.state.zip_code_parent,
            cin_parent: this.state.cin_parent,
            date_of_birth_parent: this.state.date_of_birth_parent,
            address_mail: this.state.address_mail,
            address_mail_parent: this.state.address_mail_parent,
            addressParent: this.state.addressParent,
            addressStudent: this.state.addressStudent

        }

        this.props.handleSubmit(data);
        this.setState({ previewVisible: false });
    };

    render() {

        return (
            <Auxiliary>
                <Modal isOpen={this.state.previewVisible} toggle={this.handleToggle.bind(this)}>
                    <ModalHeader toggle={this.handleToggle.bind(this)} className="modal-box-header bg-primary text-white">{<IntlMessages id="pages.studentPage" />}</ModalHeader>
                    <ModalBody>
                        <form className="row" onSubmit={this.handleSubmit}>
                            <div className="col-sm-12">
                                <h1>{<IntlMessages id="component.student.info.general" />}</h1>
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    required
                                    name='name'
                                    id="name"
                                    label={<IntlMessages id="components.student.formadd.name" />}
                                    onChange={this.handleChange}
                                    value={this.state.name}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>

                            <div className="col-sm-6">
                                <TextField
                                    required
                                    name='surname'
                                    id="surname"
                                    label={<IntlMessages id="components.student.formadd.surname" />}
                                    onChange={this.handleChange}
                                    value={this.state.surname}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    required
                                    name="address_mail"
                                    id="address_mail"
                                    label={<IntlMessages id="components.profile.email" />}
                                    onChange={this.handleChange}
                                    value={this.state.address_mail}
                                    margin="normal"
                                    fullWidth >
                                </TextField>
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    required
                                    name='gender'
                                    select
                                    id="gender"
                                    label={<IntlMessages id="components.student.formadd.gender" />}
                                    SelectProps={{}}
                                    onChange={this.handleChange}
                                    value={this.state.gender}
                                    margin="normal"
                                    fullWidth >
                                    {gender.map(option => (
                                        <MenuItem key={option.value} value={option.value}>
                                            {option.label}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    name='cin'
                                    id="cin"
                                    label={<IntlMessages id="components.student.formadd.cin" />}
                                    onChange={this.handleChange}
                                    value={this.state.cin}
                                    type="number"
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    name='zip_code'
                                    id="zip_code"
                                    label={<IntlMessages id="components.student.formadd.zip_code" />}
                                    onChange={this.handleChange}
                                    value={this.state.zip_code}
                                    type="number"
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    name='addressStudent'
                                    id="addressStudent"
                                    label={<IntlMessages id="components.student.formadd.addressStudent" />}
                                    onChange={this.handleChange}
                                    value={this.state.addressStudent}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>

                            <div className="col-sm-6">
                                <TextField
                                    name="date_of_birth"
                                    id="date_of_birth"
                                    helperText={<IntlMessages id="components.student.formadd.date_of_birth" />}
                                    onChange={this.handleChange}
                                    value={this.state.date_of_birth}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    name="phone"
                                    id="phone"
                                    label={<IntlMessages id="components.student.formadd.phone" />}
                                    type="number"
                                    onChange={this.handleChange}
                                    value={this.state.phone}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            {/* //////////////// data parents///////////// */}
                            <br /><br /><br /><br /><br />
                            <div className="col-sm-12">
                                <h1>{<IntlMessages id="component.student.info.parents" />}</h1>
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    required
                                    name='name_parent'
                                    id="name_parent"
                                    label={<IntlMessages id="components.student.formadd.name" />}
                                    onChange={this.handleChange}
                                    value={this.state.name_parent}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    required
                                    name='surname_parent'
                                    id="surname_parent"
                                    label={<IntlMessages id="components.student.formadd.surname" />}
                                    onChange={this.handleChange}
                                    value={this.state.surname_parent}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    required
                                    name="address_mail_parent"
                                    id="address_mail_parent"
                                    label={<IntlMessages id="components.profile.email" />}
                                    onChange={this.handleChange}
                                    value={this.state.address_mail_parent}
                                    margin="normal"
                                    fullWidth >
                                </TextField>
                            </div>
                            <div className="col-sm-6">
                                <TextField

                                    name="gender_parent"
                                    id="gender_parent"
                                    select
                                    label={<IntlMessages id="components.student.formadd.gender" />}
                                    SelectProps={{}}
                                    onChange={this.handleChange}
                                    value={this.state.gender_parent}
                                    margin="normal"
                                    fullWidth >
                                    {gender.map(option => (
                                        <MenuItem key={option.value} value={option.value}>
                                            {option.label}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    name="phone_parent"
                                    id="phone_parent"
                                    label={<IntlMessages id="components.student.formadd.phone" />}
                                    type="number"
                                    onChange={this.handleChange}
                                    value={this.state.phone_parent}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    name="addressParent"
                                    id="addressParent"
                                    label={<IntlMessages id="components.student.formadd.addressParent" />}
                                    onChange={this.handleChange}
                                    value={this.state.addressParent}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    name="cin_parent"
                                    id="cin_parent"
                                    label={<IntlMessages id="components.student.formadd.cin" />}
                                    type="number"
                                    onChange={this.handleChange}
                                    value={this.state.cin_parent}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    name="date_of_birth_parent"
                                    id="date_of_birth_parent"
                                    helperText={<IntlMessages id="components.student.formadd.date_of_birth" />}
                                    onChange={this.handleChange}
                                    value={this.state.date_of_birth_parent}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>

                            <br /><br /><br /><br /><br />
                            <div className="col-sm-12">
                                <h4><font color="red">*</font> {<IntlMessages id="component.required_fields" />}</h4>
                            </div>
                            <div className="col-md-12 text-right ">
                                <br /><br />
                                <Button variant="contained" className="jr-btn bg-indigo text-white " type="submit" >{<IntlMessages id="components.establishments.formModify.buttonModify" />}</Button>
                                <Button variant="contained" className="jr-btn bg-grey text-white " onClick={this.handleAnnule}>{<IntlMessages id="components.establishments.formadd.buttonCancel" />}</Button>
                            </div>
                        </form>
                    </ModalBody>
                </Modal>
            </Auxiliary>
        )
    }

}
export default EditStudent;
import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import Button from '@material-ui/core/Button';
import IntlMessages from 'util/IntlMessages';
class ModalDeleteStudent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false
        };
    }
    componentDidMount() {
        this.setState({ open: true });
    }

    handleRequestClose = () => {
        this.props.annuleModalDelete()
    };
    handleDelete = (e) => {
        this.props.handleDelete(this.props.item_id);
        this.props.annuleModalDelete()
    }

    render() {
        return (
            <div>
                <Dialog open={this.state.open} TransitionComponent={Slide} onClose={this.handleRequestClose}>
                    <DialogTitle>
                        {<IntlMessages id="title.message.confirm.modal.archifage" />}
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            {<IntlMessages id="message.confirm.modal" />}
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleRequestClose} color="secondary">
                            {<IntlMessages id="button.no" />}
                        </Button>
                        <Button onClick={this.handleDelete} color="primary">
                            {<IntlMessages id="button.yes" />}

                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}
export default ModalDeleteStudent;
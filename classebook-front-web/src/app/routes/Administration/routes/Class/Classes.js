
import React from 'react';
import Classlist from './ClassList.jsx';
import AddClasse from './AddClass';
import { getClasses } from "../../../../../actions/classeAction";
import { connect } from "react-redux";
import { getEstablishment } from "../../../../../actions/establishmentAction";
import { getData } from "../../../../../actions/professorAction";
import { getSubject } from "../../../../../actions/subjectAction";
import Input from '@material-ui/icons/Input';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import ContainerHeader from 'components/ContainerHeader/index';
import IntlMessages from 'util/IntlMessages';

class Classe extends React.Component {
    constructor() {
        super();
        this.state = {
            modal: false,
            classItem: '',
            classes: [],
            addClassModal: false
        }
        this.cancelModal = this.cancelModal.bind(this)
        this.editCLass = this.editCLass.bind(this)
        this.addClass = this.addClass.bind(this);
    }
    addClass(){
        this.setState({ addClassModal: true });
    }

    componentDidMount() {
        this.props.dispatch(getClasses())
        this.props.dispatch(getEstablishment());
        this.props.dispatch(getData());
        this.props.dispatch(getSubject());
    }

    cancelModal() {
        this.setState({ modal: false , addClassModal: false})
    }

    editCLass(element) {
        this.setState({
            modal: true,
            classItem: element
        })
    }

    render() {
        return (
            <div className="app-wrapper">
             <ContainerHeader match={this.props.match} title={<IntlMessages id="sidebar.classes" />} />
                <div className="col-md-12 text-right ">
                    <Fab size="small" color="primary" aria-label="Add" onClick={this.addClass}>
                        <AddIcon />
                    </Fab>
                    &nbsp;&nbsp;&nbsp;
                    <Fab size="small" color="primary" >
                        <Input />
                    </Fab>
                </div>
                {this.state.addClassModal ? <AddClasse establishments={this.props.establishments} professors={this.props.professors} classes={this.props.classes} subjects={this.props.subjects} cancelModal={this.cancelModal}/> : ''}
                <Classlist editCLass={this.editCLass} classes={this.props.classes} professors={this.props.professors} establishments={this.props.establishments} />

            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        classes: state.classes,
        professors: state.professor.remoteProfessors,
        establishments: state.establishment.remoteEstablishments,
        subjects: state.subject.remoteSubjects,

    }
}

export default connect(mapStateToProps)(Classe);

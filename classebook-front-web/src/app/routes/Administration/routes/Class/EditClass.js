
import React from 'react';
import TextField from '@material-ui/core/TextField';
import IntlMessages from 'util/IntlMessages';
import CardBox from 'components/CardBox/index';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import baseUrl from 'config/config';
import axios from 'axios';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Auxiliary from "util/Auxiliary";
import { Modal, ModalBody, ModalHeader } from "reactstrap";
import { connect } from "react-redux";
import { array } from 'prop-types';
import Chip from '@material-ui/core/Chip';
import Avatar from '@material-ui/core/Avatar';
import { getData } from "../../../../../actions/professorAction";
import { getEstablishment } from "../../../../../actions/establishmentAction";
import { updateClass } from "../../../../../actions/classeAction";
import moment from 'moment';
import { DatePicker } from 'material-ui-pickers';




const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;

const names = [
    'Oliver Hansen',
    'Van Henry',
    'April Tucker',
    'Ralph Hubbard',
    'Omar Alexander',
    'Carlos Abbott',
    'Miriam Wagner',
    'Bradley Wilkerson',
    'Virginia Andrews',
    'Kelly Snyder',
];

const niveau = [
    {
        value: 'bac',
        label: 'bac',
    },
    {
        value: '3éme',
        label: '3éme',
    },
    {
        value: '2éme',
        label: '2éme',
    }
];



class EditClass extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: this.props.modal,
            ListEstablishment: [],
            ListStudent: [],
            ListProfessor: [],
            professors: [],
            students: [],
            name: '',
            level_class: '',
            establishment: '',
            capacity: {},
            start_date: '',
            end_date: '',
            description: '',
            id: '',
            students_number: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleRequestDeleteProfessor = this.handleRequestDeleteProfessor.bind(this)
        this.handleChangeProfessor = this.handleChangeProfessor.bind(this)
        this.handleChangeStudents = this.handleChangeStudents.bind(this)


    };
    handleChangeProfessor = event => {
        this.setState({ professors: event.target.value });

    };
    handleChangeStudents = event => {
        this.setState({
            students: event.target.value
        });
    };


    componentDidMount() {
        let startDate = moment(this.props.classItem.start_date).format('MM-DD-YYYY');
        let endDate = moment(this.props.classItem.end_date).format('MM-DD-YYYY');


        startDate = new Date(startDate).toISOString().substr(0, 10);
        endDate = new Date(endDate).toISOString().substr(0, 10);
 
        axios.get(`${baseUrl.baseUrl}/students`)
            .then(res => {
                const ListStudent = res.data;
                this.setState({ ListStudent });
                this.setState({
                    id: this.props.classItem.id,
                    name: this.props.classItem.name,
                    level_class: this.props.classItem.level_class,
                    establishment: this.props.classItem.establishment_id,
                    capacity: this.props.classItem.capacity,
                    start_date: startDate,
                    end_date: endDate,
                    description: this.props.classItem.description,
                    students: Array.from(this.props.classItem.student_id),
                    professors: Array.from(this.props.classItem.professor_id),
                    students_number: this.props.classItem.students_number




                })

            });

    };

    handleCancel(e) {
        this.props.cancelModal()
    };

    handleChange = name => event => {
        this.setState({ [name]: event.target.value });
    };
    handleToggle() {
        this.props.cancelModal();
    };
    
    handleSubmit = (e) => {

        e.preventDefault();

        const data = {
            id: this.state.id,
            name: this.state.name,
            level_class: this.state.level_class,
            capacity: this.state.capacity,
            students_number: this.state.students_number,
            description: this.state.description,
            start_date: this.state.start_date,
            end_date: this.state.end_date,
            professor_id: this.state.professors,
            student_id: this.state.students,
            establishment_id: this.state.establishment,


        }
        this.props.updateClass(data);
        this.props.cancelModal();
    };
    handleRequestDeleteProfessor = data => () => {
        const chipData = [...this.state.professors];
        const chipToDelete = chipData.indexOf(data);
        chipData.splice(chipToDelete, 1);
        this.setState({ professors: chipData });
    };
    handleRequestDeleteStudent = data => () => {
        const chipData = [...this.state.students];
        const chipToDelete = chipData.indexOf(data);
        chipData.splice(chipToDelete, 1);
        this.setState({ students: chipData });
    };

    render() {
        return (
            <Auxiliary>

                <Modal isOpen={this.props.modal}  >
                    <ModalHeader toggle={this.handleToggle.bind(this)}>{<IntlMessages id="pages.establishementPage" />}</ModalHeader>
                    <ModalBody>
                        <form className="row" noValidate autoComplete="off" onSubmit={this.handleSubmit}>
                            <div className="col-sm-12">
                                <h1>{<IntlMessages id="component.classe.info.general" />}</h1>
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    required
                                    id="establishment"
                                    name="establishment"
                                    select
                                    label={<IntlMessages id="components.student.formadd.etablissement" />}
                                    value={this.state.establishment}
                                    onChange={this.handleChange('establishment')}
                                    SelectProps={{}}
                                    margin="normal"
                                    fullWidth >
                                    {this.props.establishments.map(establishment => (
                                        <MenuItem key={establishment + establishment.name} value={establishment.id}>
                                            {establishment.name}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div>

                            <div className="col-sm-6">
                                <TextField
                                    required
                                    name='name'
                                    id="name"
                                    value={this.state.name}
                                    onChange={this.handleChange('name')}
                                    label={<IntlMessages id="components.class.formadd.name" />}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>


                            <div className="col-sm-6">
                                <FormControl className="w-100">
                                    <InputLabel htmlFor="name-multiple">{<IntlMessages id="sidebar.professor" />}</InputLabel>
                                    <Select
                                        multiple
                                        name="professors"
                                        value={this.state.professors}
                                        onChange={this.handleChangeProfessor}
                                        input={<Input id="name-multiple" />}
                                        MenuProps={{
                                            PaperProps: {
                                                style: {
                                                    maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                                                    width: 200,
                                                },
                                            },
                                        }}
                                    >
                                        {this.props.professors.map((professor, index) => (
                                            <MenuItem
                                                key={professor.id + professor.name}
                                                value={professor.id}
                                                style={{
                                                    fontWeight: this.state.ListProfessor.indexOf(professor) !== -1 ? '500' : '400',
                                                }}
                                            >
                                                {professor.name}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                                <div className="manage-margin d-flex flex-wrap">
                                    {this.state.professors.map(data => {
                                        return (
                                            <Chip
                                                avatar={<Avatar src='data:image/png;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wCEAAUDBAQEAwUEBAQFBQUGBwwIBwcHBw8LCwkMEQ8SEhEPERETFhwXExQaFRERGCEYGh0dHx8fExciJCIeJBweHx4BBQUFBwYHDggIDh4UERQeHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHv/AABEIAIAAgAMBIgACEQEDEQH/xACUAAACAgMBAQAAAAAAAAAAAAAGBwQFAgMIAQAQAAECBAQDBQYEBAYDAAAAAAECAwAEBREGEiExB0FREyJhcYEUFTJSkaEII7HBFiQzQlNicoLR8KKy4QEAAgMBAQAAAAAAAAAAAAAAAwUCBAYBABEAAgIBBAICAgIDAAAAAAAAAQIAEQMEEiExBRMiQTNRI4EyYXH/2gAMAwEAAhEDEQA/AARpv2d4A7QZ0BtLjQIsTaBWZKVkHx+kTqJUXZebbaBOUqtCioQwhq0tlPwwU0Xhlies0BuflZFgMuJzIDrwSpQ6gQK1ibX2jQt8RsYdvD3FWJl4Yl5JiRkHCy3kbddUoFSRtcDSDYaU2YFr+pzFi6n+7ak7T5yX7CZaUUrQTsYG5hs2ISi/SGnxcwDiearUziKozcu868rMtDKCkAdBC4fTI05r2mrThlGGzZSgnMtR+VI5q/SDllq5NASalXKUmZmXlXQtASL6JJMXlGw7JqqTbc5ONqAUM7LZNwN+8oAgel4GsQ8SUKlBTsP0hMlLAWLjqs7qz1PIfeK/DWKa5LzKXpeVS8Um91D9/wDiKz+w8ngS7iTH13HNQmaTNJepdNrLtPqKSptaZhoOBsp1ulJsSCNbxYzdKdkZGTmmnZd1YcUmbea+DlZxQuSAdQcul/pCGxZUKtUKx72LKpWZABBZUQUkC1wd7+Zj6m4yrLboDk0829e+dC8pJ+Ycr6a9YH67F9yyH2HaOI/K5iii4feAfUUPOCy1ISHQkEDkdCNTqCYo2eIFMdW5IvOJMs8fynAgjJfYEGE5X5+bqEyJ3tO0WvRRSmyVkcyOR62EQWZp1HcWhYCtSg6g+IjoxgixAuFIoiHU6ke93MpGXtDYxKrvcpY/0mKGhziXHGW1OhSSQEEn7GC2pS6F04hz5Y4RU7iFETZw2W0mkvqJBBzfpFHIOZ8WMWN0A6CLDBYS3SJrKea7W8opqMs/xMxfwg2L8ZM75DnUCEqbZAbxskgfb2Tb+8RXturAsYnyCrzbJ/zCAgygYWVdFiwq3OHRwpp9ReorTrcwltCk6AozGE5XrJZZV0MNThpi+UpWHUom3CEtp+K4skeN9ou6RUY/OVM5cC0mrj5UWcJYGmqvWJ9tTi/ypZnIAp1wjYfqY4TxBVZut1IuPLK1E2Q2PhQOgEHv4kOJszxFx8++w6sUan3l6c0T3SB8bluqj9rQI4BpLk/V2z2ZKUnUmJZvWlsB1LelR2AU9mXuEsGLf7N2ZQe8ActrQz6VhaVQwG0MoSbctIu6NRVsy6CpoJFuYghkpMDVIEZvUalnPJmo02nRAKEWeI8IvIZU400Vjw3hbVmldm4ULaKVDw1jqVEoFpCVoChFTiXAFPrMqrI2G3QNCBrePYdSV7hM2nVh1OX2pgyYUzMNlxlehN9jy/8Ah3EYpL6XAlhXtLClXAOhvufJX2gi4gYYqGHZxcvOsHs1Xyr5KH/MBsvNO059KzdbKiArTTwPmId4HGQWIi1GM4zRhHJhpbanZVV97tqFjmH9pHX/ALsYM6dVW53DpSslTrbep+Ycj+0K52oqRPe0N2RewWE8/H/vKL/DlUQ1NpC/6bg110F9FDy2P1ibpxBYslMLhzgpQNGmL/5v0ijoYvidmxsL6RbYSze6pvs07Z7RRUN5xOJmApOUXteI4/xmE1x/nFwgmFdmekTqQrM+0ofMIrbh494xaUtsIcby66iKw6lNhDHEZIkmcouSoQvuKU+5KUFa0OLSVgIFidSYPcTry05og2IIN+kKfjA4VScmm/cGoF91WiziMHtthFi22orQhIzLWrKkdVEx0Jwxwe5S6SzOPMKLik50ptqrxPQeMA/4dcG/xXxFa9obK5OmtB9zTQqJskfW59IevGejYspsipmjrZYlli6i2hSlpT0t0itrMo3BL4jbR4yBurkwZqNaxYid7GWTSnW03s1nObyvBDhyuOTKRLVCnuSM4BfKoXSvxSeYhNSDePKfMJmKdPSc+rNcpcCLeRvYg+sNGnYlmqnSm3alIGQqDKg1MNDVOYgWIPMEEEHzHKKGfEAu5aMZad23BSCP+xgSbrakoOh01i0lC2vuk5T5wPUptxNN9ocSdRvA/V6vWEzANKnZdspOqXW8wPhFIcmpefgS14qUWWrGHnmnGULI1CiNRHKNakhIzD0o4M7YJTHTE3i2uM01KcR0VHYLICZ2Supv/ckm6fOEjxbkG5eqe1SwBlpgZkqG2sMtEzY32noxXrFGTGSPqLJxJaum5ITseojfKTBR3eQFxbpGtKgSW1gXF40pBbWLjRJ08ukO+4h6nYHAmSotf4dSk/2aPaAFMTAsL50m1/UWPrEHFmEKVJzvtLDQStJ0sIUvBHFNRpVPqlPk37d32hCT5WP7Rf0/HFVqFWblZ3KoK53hY+TZkKRqmmOXH7ZStrskX/WCCikKCD4iBkKyoAgmwz3kJg5EUwjxU4fdrYvtaE/xXmGzMybIUVOdnmNzokbbeJvDaxeSKWn0hGY/XnxEoqVeyU38tIninE7j7/DFLih4ImqyAkP1ObUrMdD2bXcSPrnMMx+qTVWmQZp4KQNEpG1oV1AcFHwLhunXykSLTjltLlQzm/j3oKpWrS8uyHXVpSLc4R6pizkzX6HGi4xLqsYbw+/eYnJNkuWvmy2P1gRXJMPVZiVkWimWaWFq1JKiL2HkLxjNYrTVZ96TaeTLtMp1Wd1G2wgownJSq1NGXcDpKLrWdLnnaAc1DEC5azjLi6c1KNaKyHXbW0IvF0riVmoOy8lVVSMwFflJdQEoUBuMxBzE/wDRD7fcQxMAOEBI0JO31iHNykpOktvMocA2uARHMOUo1gSL4t4qJaRxfianyjMni2mpbYNkCcl9Wl35KGwPjsYj8QqMJjDjrssLsghxofL1A8Ie8jhinGWX2kkwli1j3dPK20D2KqDKGmPyTQCUKQQi2yTB21PyDAVAjTFVKk2JxtNpKHcwFik2VHlu0TYHfaJGIEKYxBPMFNuzcII6dYiIVkIKdUk6HoY0qG1BmZyCmIlvg+celKyyWlBK1pKBfZQt8J8xBfQHFHETBvrfaAKXI9tat3bqBSRyIMHeF15sQsZtbmKeqUbgwjHQOThZJPQu9oKcH2UkQKJKbbawWYEGb6mJH7ik9S+xfb3em/hCEx338QPi2vZ6fQx0XU6U7UmUtpHd0hH8ZaWqj4ubQoJCXpdDg9bgj7feO4jzU8vDRt3E8mmqa1bMkwU26dmmI+I3pamVlqVn3Q2VouzmVodBf9Yq+GtVE1hukuqIK2GzLL63bOX/ANcsS+MNERiKRl51pYS8woBFzYHTUHpe0JmQDNtbgTVYnJwgpI87hs1BvtpAh5R1GVYOvhaCDB/8YyrBk0SCAsaJU86EpPjprHuC+H7GIZJl/Dtd9inkryuyqycyMqRmVbf4iADtaCJqlcSsONMTblKZqjalrRlastZCQTmtuAQPOIsG6hcbrdbqP6MkUx3F60vyFbk6f2ShZD7LihYHkQrW8bMJVFTE07T5pedTCsuu5HKIZ4iUppLTdblnqO66jOn2lBSlY2uLgG3jFPNvNTGJ2arSHUvS0w1lcUg3SVDY/tAWUjmoUPRqNhyqByW7EEJRvtAXjqrtSFHmZl5eVLaCo+FtYyl35pxICQbK0I6GFj+IeqOS2HBT0OfnTi7EX2QNSf0HrHMKnLkVTI58gx4iREYt01CeqFQdSLuhTlj4q/4iA0QlxTSvhO3hEuQVaSmhzyAHyvEZaf5hBG99RGrUTIMbNmegltxs3sUmD7By+0rEqoje0AEwPzjbTXSDLA7qzNytuSrfQwDUi1BlnRttJH+oQU2WdmnkoQCQYZOG6amTYTpqYhUimNSYTZIzQRyaRb1gBazxKtUKl7SCA1qNusKb8TlIDtNp1aaHel1di7/oVsfQ6esNanaN+sVuO6UxWsOzElMC7brZSdNR4jy3jittcGD7ac/8IKz2M2/SHl2D35zN+a0iyh6p1/2w3FIVOU9TRzLQTqB06whG6JUaRjqSpjwU3MImkFC07LTe4UD0t+8Pqg1BuXmcjlghew6eEV/IoA4dfuPfG5CV2n6mVKXO0iYQ+w4tKwgpS82SlwA8rjX02g4peOa17vS0KhOrcbZLLSDLBd72GYq+YAaHXxj2Ubpk60kLQg3Glt4smqVIspBYcsAOSoWe4iPGyK4p1B/qUuJ6fN4ypyEYnZaXLNZeza7MJUcosCSNQNSbeJjGiUmSplMak5ZlCEN6JSkaCLifdaaayhd0gawLVavSsg2olwZuVtTeBs7vxAAqnNVLyoT0rSpJyYmHUISlJUoqNrRynxLxw9ibEEy+22gyqTklyr4so5+u/wBIsuL2PahWJx6jMqWzKtqs7rqs9PAfrC3Fydof+P0PrX2P2Yh8hry59eM8DuWckr+Re11KYzQMzyN9r/aNcucssvTlf9I3s7tL6IF4Y3UWia327pzc9/vBTw+URU2U5v7tuUDLoASoA26QV8OpVx6qS62xqDe3hpAM5+Esac08d6fjiykNdPGK7++LGm7xSB5kHltIHueseVV5HsWUnkRGlDobQb6bwL4kqpbZcUpxLbabkqJ0SI9VmoIcNNNYp0jOCXmnJdDkxKErZdt3kcjr0N9oqVthZIN9Om4g6msNmjYJlqhUHHfetQSHSwo6S7J1Skj5zoojSwsN4DOzNyRFPUNb1+o70n47/c0S1YqFMX3FlaByPSLI8Q32msqpdZNthEOUkfbXg2TkHMmCSnYYpTSM5ZLq/mWRb0io5QdiMFJI7gu7iav1ZfZysuptKj8SjpE+VoTrMs5P1BxT8xlJBVsnyHKDin0iUQcyGkgW0yiK7H8xKUnDsy884hCA2T8Q6QMPuYKo7nmoCyZyNiBZdrk86dSp9Z+8RGgc4uIzm3O1mXHfnWVfUxi0rviNiopQJk35YmT2T/LuAf4Z/URsbUlLYNr3SIjy6hldG35Zj1CiWRyITESJIG5vdULhfUXOkMLg4U+8kkhNuyUkeeh/aF2ClzuXsSRbyMHHC9Yaq6GrkZHgm3oRr94Bm/whsXDxzL+O8T6cpKT3jbWKWo1GSlD/ADEwhJ+Uan6QG4g4gJbmE0+lBHbLNu0c2T4+fhFVMbMeBIsRD6vVaXkkKU66lOulzvC/os8rGXFfDOGQgiRmKk12yTu4hJzqBAB5JItYwLVmrPKC1PvqceXupRvBR+FiXl5vjnTZl4ZzKyk3NIzAFOZLKrXvy1P22iwMQQFjBdniPDHk69V5NM3dS/aVrdBPIFRyj0AEAwp6w2oka7wcvJLtHku6AC0DYHbeKlMoUOrBBsraM4GJuaJRQAlZQJRhxV3WyFA7jSCyVlJcJ7jeY9SbxGpMm3fvCxMWoaSyyopUbRBgSYa5jMK7FtS3VBDaUkkbAC28cwcbMaDENUNNkFn2CWUe9f8Aqr6+Q5QwON+OVMSTtJpzhCnBZ1YP/iI57mRZGZVypStPGG3jNJz7W/qLvIan4+tZHI01HhHyE9/UXtraJbst2ZZQValNz59IjrBS5ZOhFxDuJZtZy9ucpJQoHUiPkBTfccSUqtsRyMYS6lJVpe1u9bpGU67MOzOdxRWdBfwAsB9hHKnbnyXkpeYve98pP2g3oz3u3EEpOI/pTAQ7bfUGyvW4P1gHS0lxhwAnOg5reHUQW0RKalSEyi1ETEuouNK5nTUD7KHrEMo4hcR5n//Z' />}
                                                label={data}
                                                key={data.id}
                                                onDelete={this.handleRequestDeleteProfessor(data)}
                                            />
                                        );
                                    })}
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <FormControl className="w-100">
                                    <InputLabel htmlFor="name-multiple">{<IntlMessages id="sidebar.eleve" />}</InputLabel>
                                    <Select
                                        multiple
                                        name="students"
                                        value={this.state.students}
                                        onChange={this.handleChangeStudents}
                                        input={<Input id="name-multiple" />}
                                        MenuProps={{
                                            PaperProps: {
                                                style: {
                                                    maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                                                    width: 200,
                                                },
                                            },
                                        }}
                                    >
                                        {this.state.ListStudent.map(student => (
                                            <MenuItem
                                                key={student.id + student.name}
                                                value={student.id}
                                                style={{
                                                    fontWeight: this.state.ListStudent.indexOf(student) !== -1 ? '500' : '400',
                                                }}
                                            >
                                                {student.name}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                                <div className="manage-margin d-flex flex-wrap">
                                    {this.state.students.map(data => {
                                        return (
                                            <Chip
                                                avatar={<Avatar src='data:image/png;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wCEAAUDBAQEAwUEBAQFBQUGBwwIBwcHBw8LCwkMEQ8SEhEPERETFhwXExQaFRERGCEYGh0dHx8fExciJCIeJBweHx4BBQUFBwYHDggIDh4UERQeHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHv/AABEIAIAAgAMBIgACEQEDEQH/xACUAAACAgMBAQAAAAAAAAAAAAAGBwQFAgMIAQAQAAECBAQDBQYEBAYDAAAAAAECAwAEBREGEiExB0FREyJhcYEUFTJSkaEII7HBFiQzQlNicoLR8KKy4QEAAgMBAQAAAAAAAAAAAAAAAwUCBAYBABEAAgIBBAICAgIDAAAAAAAAAQIAEQMEEiExBRMiQTNRI4EyYXH/2gAMAwEAAhEDEQA/AARpv2d4A7QZ0BtLjQIsTaBWZKVkHx+kTqJUXZebbaBOUqtCioQwhq0tlPwwU0Xhlies0BuflZFgMuJzIDrwSpQ6gQK1ibX2jQt8RsYdvD3FWJl4Yl5JiRkHCy3kbddUoFSRtcDSDYaU2YFr+pzFi6n+7ak7T5yX7CZaUUrQTsYG5hs2ISi/SGnxcwDiearUziKozcu868rMtDKCkAdBC4fTI05r2mrThlGGzZSgnMtR+VI5q/SDllq5NASalXKUmZmXlXQtASL6JJMXlGw7JqqTbc5ONqAUM7LZNwN+8oAgel4GsQ8SUKlBTsP0hMlLAWLjqs7qz1PIfeK/DWKa5LzKXpeVS8Um91D9/wDiKz+w8ngS7iTH13HNQmaTNJepdNrLtPqKSptaZhoOBsp1ulJsSCNbxYzdKdkZGTmmnZd1YcUmbea+DlZxQuSAdQcul/pCGxZUKtUKx72LKpWZABBZUQUkC1wd7+Zj6m4yrLboDk0829e+dC8pJ+Ycr6a9YH67F9yyH2HaOI/K5iii4feAfUUPOCy1ISHQkEDkdCNTqCYo2eIFMdW5IvOJMs8fynAgjJfYEGE5X5+bqEyJ3tO0WvRRSmyVkcyOR62EQWZp1HcWhYCtSg6g+IjoxgixAuFIoiHU6ke93MpGXtDYxKrvcpY/0mKGhziXHGW1OhSSQEEn7GC2pS6F04hz5Y4RU7iFETZw2W0mkvqJBBzfpFHIOZ8WMWN0A6CLDBYS3SJrKea7W8opqMs/xMxfwg2L8ZM75DnUCEqbZAbxskgfb2Tb+8RXturAsYnyCrzbJ/zCAgygYWVdFiwq3OHRwpp9ReorTrcwltCk6AozGE5XrJZZV0MNThpi+UpWHUom3CEtp+K4skeN9ou6RUY/OVM5cC0mrj5UWcJYGmqvWJ9tTi/ypZnIAp1wjYfqY4TxBVZut1IuPLK1E2Q2PhQOgEHv4kOJszxFx8++w6sUan3l6c0T3SB8bluqj9rQI4BpLk/V2z2ZKUnUmJZvWlsB1LelR2AU9mXuEsGLf7N2ZQe8ActrQz6VhaVQwG0MoSbctIu6NRVsy6CpoJFuYghkpMDVIEZvUalnPJmo02nRAKEWeI8IvIZU400Vjw3hbVmldm4ULaKVDw1jqVEoFpCVoChFTiXAFPrMqrI2G3QNCBrePYdSV7hM2nVh1OX2pgyYUzMNlxlehN9jy/8Ah3EYpL6XAlhXtLClXAOhvufJX2gi4gYYqGHZxcvOsHs1Xyr5KH/MBsvNO059KzdbKiArTTwPmId4HGQWIi1GM4zRhHJhpbanZVV97tqFjmH9pHX/ALsYM6dVW53DpSslTrbep+Ycj+0K52oqRPe0N2RewWE8/H/vKL/DlUQ1NpC/6bg110F9FDy2P1ibpxBYslMLhzgpQNGmL/5v0ijoYvidmxsL6RbYSze6pvs07Z7RRUN5xOJmApOUXteI4/xmE1x/nFwgmFdmekTqQrM+0ofMIrbh494xaUtsIcby66iKw6lNhDHEZIkmcouSoQvuKU+5KUFa0OLSVgIFidSYPcTry05og2IIN+kKfjA4VScmm/cGoF91WiziMHtthFi22orQhIzLWrKkdVEx0Jwxwe5S6SzOPMKLik50ptqrxPQeMA/4dcG/xXxFa9obK5OmtB9zTQqJskfW59IevGejYspsipmjrZYlli6i2hSlpT0t0itrMo3BL4jbR4yBurkwZqNaxYid7GWTSnW03s1nObyvBDhyuOTKRLVCnuSM4BfKoXSvxSeYhNSDePKfMJmKdPSc+rNcpcCLeRvYg+sNGnYlmqnSm3alIGQqDKg1MNDVOYgWIPMEEEHzHKKGfEAu5aMZad23BSCP+xgSbrakoOh01i0lC2vuk5T5wPUptxNN9ocSdRvA/V6vWEzANKnZdspOqXW8wPhFIcmpefgS14qUWWrGHnmnGULI1CiNRHKNakhIzD0o4M7YJTHTE3i2uM01KcR0VHYLICZ2Supv/ckm6fOEjxbkG5eqe1SwBlpgZkqG2sMtEzY32noxXrFGTGSPqLJxJaum5ITseojfKTBR3eQFxbpGtKgSW1gXF40pBbWLjRJ08ukO+4h6nYHAmSotf4dSk/2aPaAFMTAsL50m1/UWPrEHFmEKVJzvtLDQStJ0sIUvBHFNRpVPqlPk37d32hCT5WP7Rf0/HFVqFWblZ3KoK53hY+TZkKRqmmOXH7ZStrskX/WCCikKCD4iBkKyoAgmwz3kJg5EUwjxU4fdrYvtaE/xXmGzMybIUVOdnmNzokbbeJvDaxeSKWn0hGY/XnxEoqVeyU38tIninE7j7/DFLih4ImqyAkP1ObUrMdD2bXcSPrnMMx+qTVWmQZp4KQNEpG1oV1AcFHwLhunXykSLTjltLlQzm/j3oKpWrS8uyHXVpSLc4R6pizkzX6HGi4xLqsYbw+/eYnJNkuWvmy2P1gRXJMPVZiVkWimWaWFq1JKiL2HkLxjNYrTVZ96TaeTLtMp1Wd1G2wgownJSq1NGXcDpKLrWdLnnaAc1DEC5azjLi6c1KNaKyHXbW0IvF0riVmoOy8lVVSMwFflJdQEoUBuMxBzE/wDRD7fcQxMAOEBI0JO31iHNykpOktvMocA2uARHMOUo1gSL4t4qJaRxfianyjMni2mpbYNkCcl9Wl35KGwPjsYj8QqMJjDjrssLsghxofL1A8Ie8jhinGWX2kkwli1j3dPK20D2KqDKGmPyTQCUKQQi2yTB21PyDAVAjTFVKk2JxtNpKHcwFik2VHlu0TYHfaJGIEKYxBPMFNuzcII6dYiIVkIKdUk6HoY0qG1BmZyCmIlvg+celKyyWlBK1pKBfZQt8J8xBfQHFHETBvrfaAKXI9tat3bqBSRyIMHeF15sQsZtbmKeqUbgwjHQOThZJPQu9oKcH2UkQKJKbbawWYEGb6mJH7ik9S+xfb3em/hCEx338QPi2vZ6fQx0XU6U7UmUtpHd0hH8ZaWqj4ubQoJCXpdDg9bgj7feO4jzU8vDRt3E8mmqa1bMkwU26dmmI+I3pamVlqVn3Q2VouzmVodBf9Yq+GtVE1hukuqIK2GzLL63bOX/ANcsS+MNERiKRl51pYS8woBFzYHTUHpe0JmQDNtbgTVYnJwgpI87hs1BvtpAh5R1GVYOvhaCDB/8YyrBk0SCAsaJU86EpPjprHuC+H7GIZJl/Dtd9inkryuyqycyMqRmVbf4iADtaCJqlcSsONMTblKZqjalrRlastZCQTmtuAQPOIsG6hcbrdbqP6MkUx3F60vyFbk6f2ShZD7LihYHkQrW8bMJVFTE07T5pedTCsuu5HKIZ4iUppLTdblnqO66jOn2lBSlY2uLgG3jFPNvNTGJ2arSHUvS0w1lcUg3SVDY/tAWUjmoUPRqNhyqByW7EEJRvtAXjqrtSFHmZl5eVLaCo+FtYyl35pxICQbK0I6GFj+IeqOS2HBT0OfnTi7EX2QNSf0HrHMKnLkVTI58gx4iREYt01CeqFQdSLuhTlj4q/4iA0QlxTSvhO3hEuQVaSmhzyAHyvEZaf5hBG99RGrUTIMbNmegltxs3sUmD7By+0rEqoje0AEwPzjbTXSDLA7qzNytuSrfQwDUi1BlnRttJH+oQU2WdmnkoQCQYZOG6amTYTpqYhUimNSYTZIzQRyaRb1gBazxKtUKl7SCA1qNusKb8TlIDtNp1aaHel1di7/oVsfQ6esNanaN+sVuO6UxWsOzElMC7brZSdNR4jy3jittcGD7ac/8IKz2M2/SHl2D35zN+a0iyh6p1/2w3FIVOU9TRzLQTqB06whG6JUaRjqSpjwU3MImkFC07LTe4UD0t+8Pqg1BuXmcjlghew6eEV/IoA4dfuPfG5CV2n6mVKXO0iYQ+w4tKwgpS82SlwA8rjX02g4peOa17vS0KhOrcbZLLSDLBd72GYq+YAaHXxj2Ubpk60kLQg3Glt4smqVIspBYcsAOSoWe4iPGyK4p1B/qUuJ6fN4ypyEYnZaXLNZeza7MJUcosCSNQNSbeJjGiUmSplMak5ZlCEN6JSkaCLifdaaayhd0gawLVavSsg2olwZuVtTeBs7vxAAqnNVLyoT0rSpJyYmHUISlJUoqNrRynxLxw9ibEEy+22gyqTklyr4so5+u/wBIsuL2PahWJx6jMqWzKtqs7rqs9PAfrC3Fydof+P0PrX2P2Yh8hry59eM8DuWckr+Re11KYzQMzyN9r/aNcucssvTlf9I3s7tL6IF4Y3UWia327pzc9/vBTw+URU2U5v7tuUDLoASoA26QV8OpVx6qS62xqDe3hpAM5+Esac08d6fjiykNdPGK7++LGm7xSB5kHltIHueseVV5HsWUnkRGlDobQb6bwL4kqpbZcUpxLbabkqJ0SI9VmoIcNNNYp0jOCXmnJdDkxKErZdt3kcjr0N9oqVthZIN9Om4g6msNmjYJlqhUHHfetQSHSwo6S7J1Skj5zoojSwsN4DOzNyRFPUNb1+o70n47/c0S1YqFMX3FlaByPSLI8Q32msqpdZNthEOUkfbXg2TkHMmCSnYYpTSM5ZLq/mWRb0io5QdiMFJI7gu7iav1ZfZysuptKj8SjpE+VoTrMs5P1BxT8xlJBVsnyHKDin0iUQcyGkgW0yiK7H8xKUnDsy884hCA2T8Q6QMPuYKo7nmoCyZyNiBZdrk86dSp9Z+8RGgc4uIzm3O1mXHfnWVfUxi0rviNiopQJk35YmT2T/LuAf4Z/URsbUlLYNr3SIjy6hldG35Zj1CiWRyITESJIG5vdULhfUXOkMLg4U+8kkhNuyUkeeh/aF2ClzuXsSRbyMHHC9Yaq6GrkZHgm3oRr94Bm/whsXDxzL+O8T6cpKT3jbWKWo1GSlD/ADEwhJ+Uan6QG4g4gJbmE0+lBHbLNu0c2T4+fhFVMbMeBIsRD6vVaXkkKU66lOulzvC/os8rGXFfDOGQgiRmKk12yTu4hJzqBAB5JItYwLVmrPKC1PvqceXupRvBR+FiXl5vjnTZl4ZzKyk3NIzAFOZLKrXvy1P22iwMQQFjBdniPDHk69V5NM3dS/aVrdBPIFRyj0AEAwp6w2oka7wcvJLtHku6AC0DYHbeKlMoUOrBBsraM4GJuaJRQAlZQJRhxV3WyFA7jSCyVlJcJ7jeY9SbxGpMm3fvCxMWoaSyyopUbRBgSYa5jMK7FtS3VBDaUkkbAC28cwcbMaDENUNNkFn2CWUe9f8Aqr6+Q5QwON+OVMSTtJpzhCnBZ1YP/iI57mRZGZVypStPGG3jNJz7W/qLvIan4+tZHI01HhHyE9/UXtraJbst2ZZQValNz59IjrBS5ZOhFxDuJZtZy9ucpJQoHUiPkBTfccSUqtsRyMYS6lJVpe1u9bpGU67MOzOdxRWdBfwAsB9hHKnbnyXkpeYve98pP2g3oz3u3EEpOI/pTAQ7bfUGyvW4P1gHS0lxhwAnOg5reHUQW0RKalSEyi1ETEuouNK5nTUD7KHrEMo4hcR5n//Z' />}
                                                label={data}
                                                key={data.id}
                                                onDelete={this.handleRequestDeleteStudent(data)}
                                            />
                                        );
                                    })}
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    required
                                    id="level_class"
                                    name="level_class"
                                    select
                                    label={<IntlMessages id="components.note.niveau" />}
                                    value={this.state.level_class}
                                    onChange={this.handleChange('level_class')}
                                    SelectProps={{}}
                                    margin="normal"
                                    fullWidth >
                                    {niveau.map(option => (
                                        <MenuItem key={option.value} value={option.value}>
                                            {option.label}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    name='students_number'
                                    id="students_number"
                                    value={this.state.students_number}
                                    onChange={this.handleChange('students_number')}
                                    label={<IntlMessages id="components.establishments.formadd.number_students" />}
                                    type="number"
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    name='capacity'
                                    id="capacity"
                                    onChange={this.handleChange('capacity')}
                                    value={this.state.capacity}
                                    label={<IntlMessages id="components.class.formadd.capacity" />}
                                    type="number"
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    required
                                    name='description'
                                    id="description"
                                    onChange={this.handleChange('description')}
                                    value={this.state.description}
                                    label={<IntlMessages id="room.description" />}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    id="start_date"
                                    name="start_date"
                                    type="date"
                                    onChange={this.handleChange('start_date')}
                                    value={this.state.start_date}
                                    helperText={<IntlMessages id="components.class.formadd.startdate" />}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    id="end_date"
                                    name="end_date"
                                    type="date"
                                    onChange={this.handleChange('end_date')}
                                    value={this.state.end_date}
                                    helperText={<IntlMessages id="components.class.formadd.startdate" />}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>

                            <br /><br /><br /><br /><br />
                            <div className="col-sm-12">
                                <h4><font color="red">*</font> {<IntlMessages id="component.required_fields" />}</h4>
                            </div>
                            <div className="col-md-12 text-right ">
                                <br /><br />
                                <Button variant="contained" className="jr-btn bg-indigo text-white " type="submit" >{<IntlMessages id="button.save.registreAppel" />}</Button>
                                <Button variant="contained" className="jr-btn bg-grey text-white " onClick={this.handleCancel}>{<IntlMessages id="components.establishments.formadd.buttonCancel" />}</Button>
                            </div>
                        </form>
                    </ModalBody>
                </Modal>
            </Auxiliary>

        )
    }

}
function mapStateToProps(state) {
    return {
        professors: state.professor.remoteProfessors,
        establishments: state.establishment.remoteEstablishments


    };
}

export default connect(mapStateToProps, { getData, getEstablishment, updateClass })(EditClass);
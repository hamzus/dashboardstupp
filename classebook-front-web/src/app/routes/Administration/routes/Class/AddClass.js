import React from 'react';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import IntlMessages from 'util/IntlMessages';
import StepContent from '@material-ui/core/StepContent';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import baseUrl from 'config/config';
import axios from 'axios';
import { connect } from "react-redux";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Chip from '@material-ui/core/Chip';
import Avatar from '@material-ui/core/Avatar';
import { addClass } from "../../../../../actions/classeAction";
import _ from 'lodash';
import { List } from '@material-ui/core';
import FormControl from '@material-ui/core/FormControl';
import Auxiliary from "util/Auxiliary";
import { Modal, ModalBody, ModalHeader } from "reactstrap";


const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const roleIdStudent = 5;
const niveau = [
    {
        value: 'bac',
        label: 'bac',
    },
    {
        value: '3éme',
        label: '3éme',
    },
    {
        value: '2éme',
        label: '2éme',
    }
];
function jsUpperCaseFirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
};
class AddClass extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            activeStep: 0,
            steps: [<IntlMessages id="component.classe.info.general" />, <IntlMessages id="component.chips.list.professors" />,
            <IntlMessages id="component.students.list" />],
            professorListBySubjectId: [],
            students: [],
            name: '',
            niveau: '',
            establishment: '',
            capacity: '',
            start_date: '',
            end_date: '',
            description: '',
            classNumber: '',
            finalListOfProfessor: [],
            subjectListByEstablishmentId: [],
            studentsListByEstablishment: [],
            previewVisible: true,
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleRequestDelete = this.handleRequestDelete.bind(this);
    };

    handleRequestDelete = data => () => {
        const chipData = [...this.state.students];
        const chipToDelete = chipData.indexOf(data);
        chipData.splice(chipToDelete, 1);
        this.setState({ students: chipData });
    };

    handleNext = () => {
        const { activeStep } = this.state;
        this.setState({
            activeStep: activeStep + 1,
        });
    };

    handleBack = () => {
        const { activeStep } = this.state;
        this.setState({
            activeStep: activeStep - 1,
        });
    };

    handleReset = () => {
        this.setState({
            activeStep: 0,
        });
    };

    addProfessor = (e) => {
        this.setState((prevState) => ({
            professors: [...prevState.professors, 0],
        }));
    };

    handleChangeStudents = event => {
        this.setState({
            students: event.target.value
        });
    };

    handleChange = name => event => {
        this.setState({ [name]: event.target.value });

        if (name == 'establishment') {
            this.setState({ subjectListByEstablishmentId: [] });
            const subjectList = this.props.subjects.filter(element => element.id_establishment == event.target.value)
            this.setState({ subjectListByEstablishmentId: subjectList });
            let filteredListOfProfessor = []
            subjectList.map(element => {
                let subjectId = element.id;
                let subjectName = element.name;
                let obj = {};
                axios.get(`${baseUrl.baseUrl}/professors?filter[where][subject_id]=` + subjectId)
                    .then(response => {
                        obj = {
                            "subjectId": subjectId,
                            "subjectName": subjectName,
                            "professorlist": response.data,
                        }
                        filteredListOfProfessor.push(obj);
                        this.setState({ professorListBySubjectId: filteredListOfProfessor });
                    }).catch((err) => {
                        console.log(err);
                    })
            });

            axios.get(`${baseUrl.baseUrl}/profiles?filter={"where":{"and":[{"establishment_id":` + event.target.value + `},{"role_id":` + roleIdStudent + `}]}}`)
                .then(res => {
                    let profilesIdsListOfStudent = res.data;
                    let inc = 0;
                    let profileId = [];
                    for (var element of profilesIdsListOfStudent) {
                        profileId.push(element.id);
                        inc++;
                    }
                    if (profileId.length == inc) {
                        axios.get(`${baseUrl.baseUrl}/students?filter={"where":{"profile_id": { "inq":[` + profileId + `]}}}`)
                            .then(res => {
                                this.setState({ studentsListByEstablishment: res.data });
                            }).catch((err) => {
                                console.log(err);
                            })
                    }

                })
        }

        if (name == 'professor') {
            let finalListOfProfessor = [...this.state.finalListOfProfessor];
            finalListOfProfessor[event.target.dataset.id] = event.target.value;

            this.setState({ finalListOfProfessor })
        }
    };

    handleSubmit(event) {
        event.preventDefault();
        const data = {
            name: this.state.name,
            level_class: this.state.niveau,
            capacity: this.state.capacity,
            students_number: this.state.classNumber,
            description: this.state.description,
            start_date: this.state.start_date,
            end_date: this.state.end_date,
            establishment_id: this.state.establishment,
            professor_id: this.state.finalListOfProfessor,
            student_id: this.state.students
        };
        this.props.dispatch(addClass(data));
        this.setState({
            previewVisible: false,
            professors: [],
            students: [],
            name: '',
            niveau: '',
            establishment: '',
            start_date: '',
            end_date: '',
            description: '',
            capacity: '',
            description: '',
        })
        this.handleReset();
        this.props.cancelModal();
    };

    getStepContent(step) {

        let { professorListBySubjectId, studentsListByEstablishment, finalListOfProfessor } = this.state;

        switch (step) {
            case 0:
                return <div>
                    <div className="row">
                        <div className="col-md-4">
                            <div className="form-group">
                                <TextField
                                    required
                                    name='establishment'
                                    id="establishment"
                                    select
                                    label={<IntlMessages id="components.student.formadd.establishment" />}
                                    value={this.state.establishment}
                                    onChange={this.handleChange('establishment')}
                                    SelectProps={{}}
                                    margin="normal"
                                    fullWidth >
                                    {this.props.establishments.map(establishment => (
                                        <MenuItem key={establishment.id} value={establishment.id}>
                                            {establishment.name}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="form-group">
                                <TextField
                                    required
                                    id="niveau"
                                    name="niveau"
                                    select
                                    label={<IntlMessages id="components.note.niveau" />}
                                    value={this.state.niveau}
                                    onChange={this.handleChange('niveau')}
                                    SelectProps={{}}
                                    margin="normal"
                                    fullWidth >
                                    {niveau.map(option => (
                                        <MenuItem key={option.value} value={option.value}>
                                            {option.label}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="form-group">
                                <TextField
                                    required
                                    name='name'
                                    id="name"
                                    value={this.state.name}
                                    onChange={this.handleChange('name')}
                                    label={<IntlMessages id="components.class.formadd.name" />}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-4">
                            <div className="form-group">
                                <TextField
                                    required
                                    name='classNumber'
                                    id="classNumber"
                                    value={this.state.classNumber}
                                    onChange={this.handleChange('classNumber')}
                                    label={<IntlMessages id="components.establishments.formadd.number_students" />}
                                    type="number"
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="form-group">
                                <TextField
                                    required
                                    name='capacity'
                                    id="capacity"
                                    value={this.state.capacity}
                                    onChange={this.handleChange('capacity')}
                                    label={<IntlMessages id="components.class.formadd.capacity" />}
                                    type="number"
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="form-group">
                                <TextField
                                    required
                                    name='description'
                                    id="description"
                                    value={this.state.description}
                                    onChange={this.handleChange('description')}
                                    label={<IntlMessages id="room.description" />}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <TextField
                                    required
                                    id="start_date"
                                    type="date"
                                    value={this.state.start_date}
                                    onChange={this.handleChange('start_date')}
                                    helperText={<IntlMessages id="components.class.formadd.startdate" />}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <TextField
                                    required
                                    id="end_date"
                                    type="date"
                                    value={this.state.end_date}
                                    onChange={this.handleChange('end_date')}
                                    helperText={<IntlMessages id="components.class.formadd.enddate" />}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                        </div>

                    </div>
                </div >;
            case 1:
                return <div className="row">
                    {(() => {
                        if (studentsListByEstablishment.length !== 0) {
                            return (
                                <Table className="default-table table-unbordered table table-sm table-hover">
                                    <TableBody>
                                        {professorListBySubjectId.map((element, index) => {
                                            let professorId = `professor-${index}`;
                                            return (
                                                <TableRow key={index}>
                                                    <TableCell>
                                                        <label htmlFor="pet-select"> {index + 1}- {<IntlMessages id="components.note.subject" />} : {element.subjectName} </label>
                                                    </TableCell>

                                                    <TableCell>
                                                        {finalListOfProfessor[index]}
                                                    </TableCell>

                                                    <TableCell>
                                                        <select
                                                            name={professorId}
                                                            data-id={index}
                                                            id={professorId}
                                                            className="professor"
                                                            onChange={this.handleChange('professor')}
                                                        >
                                                            <option value="">Professeur</option>
                                                            {element.professorlist.map(professor =>
                                                                <option
                                                                    key={professor.id}
                                                                    value={professor.id}>
                                                                    {professor.id + ": " + professor.surname.toUpperCase() + " " + jsUpperCaseFirst(professor.name)}
                                                                </option>)}
                                                        </select>
                                                    </TableCell>
                                                </TableRow>
                                            )
                                        })
                                        }
                                    </TableBody>
                                </Table>
                            )
                        }
                    })()}
                </div>;
            case 2:
                return (<div  >
                    {(() => {
                        if (studentsListByEstablishment.length !== 0) {
                            return (<div>
                                <div className="col-lg-3 col-sm-6 col-12">

                                    <FormControl className="w-100 mb-2">
                                        <label htmlFor="pet-select">{<IntlMessages id="sidebar.eleve" />}</label>
                                        <Select
                                            multiple
                                            name="students"
                                            value={this.state.students}
                                            onChange={this.handleChangeStudents}
                                            input={<Input id="name-multiple" />}
                                            MenuProps={{
                                                PaperProps: {
                                                    style: {
                                                        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                                                        width: 200,
                                                    },
                                                },
                                            }}
                                        >
                                            {studentsListByEstablishment.map((student) => (
                                                <MenuItem
                                                    key={student.id + student.name}
                                                    value={student.id}
                                                    style={{
                                                        fontWeight: this.state.students.indexOf(student.name) !== -1 ? '500' : '400',
                                                    }}
                                                >
                                                    {student.id + " " + student.surname.toUpperCase() + " " + jsUpperCaseFirst(student.name)}

                                                </MenuItem>
                                            ))}
                                        </Select>

                                    </FormControl>
                                </div>

                                <div className="manage-margin d-flex flex-wrap">
                                    {this.state.students.map((data, index) => {
                                        return (
                                                <List key={index}>

                                                    <Chip
                                                        key={index + data}
                                                        avatar={<Avatar src='data:image/png;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wCEAAUDBAQEAwUEBAQFBQUGBwwIBwcHBw8LCwkMEQ8SEhEPERETFhwXExQaFRERGCEYGh0dHx8fExciJCIeJBweHx4BBQUFBwYHDggIDh4UERQeHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHv/AABEIAIAAgAMBIgACEQEDEQH/xACUAAACAgMBAQAAAAAAAAAAAAAGBwQFAgMIAQAQAAECBAQDBQYEBAYDAAAAAAECAwAEBREGEiExB0FREyJhcYEUFTJSkaEII7HBFiQzQlNicoLR8KKy4QEAAgMBAQAAAAAAAAAAAAAAAwUCBAYBABEAAgIBBAICAgIDAAAAAAAAAQIAEQMEEiExBRMiQTNRI4EyYXH/2gAMAwEAAhEDEQA/AARpv2d4A7QZ0BtLjQIsTaBWZKVkHx+kTqJUXZebbaBOUqtCioQwhq0tlPwwU0Xhlies0BuflZFgMuJzIDrwSpQ6gQK1ibX2jQt8RsYdvD3FWJl4Yl5JiRkHCy3kbddUoFSRtcDSDYaU2YFr+pzFi6n+7ak7T5yX7CZaUUrQTsYG5hs2ISi/SGnxcwDiearUziKozcu868rMtDKCkAdBC4fTI05r2mrThlGGzZSgnMtR+VI5q/SDllq5NASalXKUmZmXlXQtASL6JJMXlGw7JqqTbc5ONqAUM7LZNwN+8oAgel4GsQ8SUKlBTsP0hMlLAWLjqs7qz1PIfeK/DWKa5LzKXpeVS8Um91D9/wDiKz+w8ngS7iTH13HNQmaTNJepdNrLtPqKSptaZhoOBsp1ulJsSCNbxYzdKdkZGTmmnZd1YcUmbea+DlZxQuSAdQcul/pCGxZUKtUKx72LKpWZABBZUQUkC1wd7+Zj6m4yrLboDk0829e+dC8pJ+Ycr6a9YH67F9yyH2HaOI/K5iii4feAfUUPOCy1ISHQkEDkdCNTqCYo2eIFMdW5IvOJMs8fynAgjJfYEGE5X5+bqEyJ3tO0WvRRSmyVkcyOR62EQWZp1HcWhYCtSg6g+IjoxgixAuFIoiHU6ke93MpGXtDYxKrvcpY/0mKGhziXHGW1OhSSQEEn7GC2pS6F04hz5Y4RU7iFETZw2W0mkvqJBBzfpFHIOZ8WMWN0A6CLDBYS3SJrKea7W8opqMs/xMxfwg2L8ZM75DnUCEqbZAbxskgfb2Tb+8RXturAsYnyCrzbJ/zCAgygYWVdFiwq3OHRwpp9ReorTrcwltCk6AozGE5XrJZZV0MNThpi+UpWHUom3CEtp+K4skeN9ou6RUY/OVM5cC0mrj5UWcJYGmqvWJ9tTi/ypZnIAp1wjYfqY4TxBVZut1IuPLK1E2Q2PhQOgEHv4kOJszxFx8++w6sUan3l6c0T3SB8bluqj9rQI4BpLk/V2z2ZKUnUmJZvWlsB1LelR2AU9mXuEsGLf7N2ZQe8ActrQz6VhaVQwG0MoSbctIu6NRVsy6CpoJFuYghkpMDVIEZvUalnPJmo02nRAKEWeI8IvIZU400Vjw3hbVmldm4ULaKVDw1jqVEoFpCVoChFTiXAFPrMqrI2G3QNCBrePYdSV7hM2nVh1OX2pgyYUzMNlxlehN9jy/8Ah3EYpL6XAlhXtLClXAOhvufJX2gi4gYYqGHZxcvOsHs1Xyr5KH/MBsvNO059KzdbKiArTTwPmId4HGQWIi1GM4zRhHJhpbanZVV97tqFjmH9pHX/ALsYM6dVW53DpSslTrbep+Ycj+0K52oqRPe0N2RewWE8/H/vKL/DlUQ1NpC/6bg110F9FDy2P1ibpxBYslMLhzgpQNGmL/5v0ijoYvidmxsL6RbYSze6pvs07Z7RRUN5xOJmApOUXteI4/xmE1x/nFwgmFdmekTqQrM+0ofMIrbh494xaUtsIcby66iKw6lNhDHEZIkmcouSoQvuKU+5KUFa0OLSVgIFidSYPcTry05og2IIN+kKfjA4VScmm/cGoF91WiziMHtthFi22orQhIzLWrKkdVEx0Jwxwe5S6SzOPMKLik50ptqrxPQeMA/4dcG/xXxFa9obK5OmtB9zTQqJskfW59IevGejYspsipmjrZYlli6i2hSlpT0t0itrMo3BL4jbR4yBurkwZqNaxYid7GWTSnW03s1nObyvBDhyuOTKRLVCnuSM4BfKoXSvxSeYhNSDePKfMJmKdPSc+rNcpcCLeRvYg+sNGnYlmqnSm3alIGQqDKg1MNDVOYgWIPMEEEHzHKKGfEAu5aMZad23BSCP+xgSbrakoOh01i0lC2vuk5T5wPUptxNN9ocSdRvA/V6vWEzANKnZdspOqXW8wPhFIcmpefgS14qUWWrGHnmnGULI1CiNRHKNakhIzD0o4M7YJTHTE3i2uM01KcR0VHYLICZ2Supv/ckm6fOEjxbkG5eqe1SwBlpgZkqG2sMtEzY32noxXrFGTGSPqLJxJaum5ITseojfKTBR3eQFxbpGtKgSW1gXF40pBbWLjRJ08ukO+4h6nYHAmSotf4dSk/2aPaAFMTAsL50m1/UWPrEHFmEKVJzvtLDQStJ0sIUvBHFNRpVPqlPk37d32hCT5WP7Rf0/HFVqFWblZ3KoK53hY+TZkKRqmmOXH7ZStrskX/WCCikKCD4iBkKyoAgmwz3kJg5EUwjxU4fdrYvtaE/xXmGzMybIUVOdnmNzokbbeJvDaxeSKWn0hGY/XnxEoqVeyU38tIninE7j7/DFLih4ImqyAkP1ObUrMdD2bXcSPrnMMx+qTVWmQZp4KQNEpG1oV1AcFHwLhunXykSLTjltLlQzm/j3oKpWrS8uyHXVpSLc4R6pizkzX6HGi4xLqsYbw+/eYnJNkuWvmy2P1gRXJMPVZiVkWimWaWFq1JKiL2HkLxjNYrTVZ96TaeTLtMp1Wd1G2wgownJSq1NGXcDpKLrWdLnnaAc1DEC5azjLi6c1KNaKyHXbW0IvF0riVmoOy8lVVSMwFflJdQEoUBuMxBzE/wDRD7fcQxMAOEBI0JO31iHNykpOktvMocA2uARHMOUo1gSL4t4qJaRxfianyjMni2mpbYNkCcl9Wl35KGwPjsYj8QqMJjDjrssLsghxofL1A8Ie8jhinGWX2kkwli1j3dPK20D2KqDKGmPyTQCUKQQi2yTB21PyDAVAjTFVKk2JxtNpKHcwFik2VHlu0TYHfaJGIEKYxBPMFNuzcII6dYiIVkIKdUk6HoY0qG1BmZyCmIlvg+celKyyWlBK1pKBfZQt8J8xBfQHFHETBvrfaAKXI9tat3bqBSRyIMHeF15sQsZtbmKeqUbgwjHQOThZJPQu9oKcH2UkQKJKbbawWYEGb6mJH7ik9S+xfb3em/hCEx338QPi2vZ6fQx0XU6U7UmUtpHd0hH8ZaWqj4ubQoJCXpdDg9bgj7feO4jzU8vDRt3E8mmqa1bMkwU26dmmI+I3pamVlqVn3Q2VouzmVodBf9Yq+GtVE1hukuqIK2GzLL63bOX/ANcsS+MNERiKRl51pYS8woBFzYHTUHpe0JmQDNtbgTVYnJwgpI87hs1BvtpAh5R1GVYOvhaCDB/8YyrBk0SCAsaJU86EpPjprHuC+H7GIZJl/Dtd9inkryuyqycyMqRmVbf4iADtaCJqlcSsONMTblKZqjalrRlastZCQTmtuAQPOIsG6hcbrdbqP6MkUx3F60vyFbk6f2ShZD7LihYHkQrW8bMJVFTE07T5pedTCsuu5HKIZ4iUppLTdblnqO66jOn2lBSlY2uLgG3jFPNvNTGJ2arSHUvS0w1lcUg3SVDY/tAWUjmoUPRqNhyqByW7EEJRvtAXjqrtSFHmZl5eVLaCo+FtYyl35pxICQbK0I6GFj+IeqOS2HBT0OfnTi7EX2QNSf0HrHMKnLkVTI58gx4iREYt01CeqFQdSLuhTlj4q/4iA0QlxTSvhO3hEuQVaSmhzyAHyvEZaf5hBG99RGrUTIMbNmegltxs3sUmD7By+0rEqoje0AEwPzjbTXSDLA7qzNytuSrfQwDUi1BlnRttJH+oQU2WdmnkoQCQYZOG6amTYTpqYhUimNSYTZIzQRyaRb1gBazxKtUKl7SCA1qNusKb8TlIDtNp1aaHel1di7/oVsfQ6esNanaN+sVuO6UxWsOzElMC7brZSdNR4jy3jittcGD7ac/8IKz2M2/SHl2D35zN+a0iyh6p1/2w3FIVOU9TRzLQTqB06whG6JUaRjqSpjwU3MImkFC07LTe4UD0t+8Pqg1BuXmcjlghew6eEV/IoA4dfuPfG5CV2n6mVKXO0iYQ+w4tKwgpS82SlwA8rjX02g4peOa17vS0KhOrcbZLLSDLBd72GYq+YAaHXxj2Ubpk60kLQg3Glt4smqVIspBYcsAOSoWe4iPGyK4p1B/qUuJ6fN4ypyEYnZaXLNZeza7MJUcosCSNQNSbeJjGiUmSplMak5ZlCEN6JSkaCLifdaaayhd0gawLVavSsg2olwZuVtTeBs7vxAAqnNVLyoT0rSpJyYmHUISlJUoqNrRynxLxw9ibEEy+22gyqTklyr4so5+u/wBIsuL2PahWJx6jMqWzKtqs7rqs9PAfrC3Fydof+P0PrX2P2Yh8hry59eM8DuWckr+Re11KYzQMzyN9r/aNcucssvTlf9I3s7tL6IF4Y3UWia327pzc9/vBTw+URU2U5v7tuUDLoASoA26QV8OpVx6qS62xqDe3hpAM5+Esac08d6fjiykNdPGK7++LGm7xSB5kHltIHueseVV5HsWUnkRGlDobQb6bwL4kqpbZcUpxLbabkqJ0SI9VmoIcNNNYp0jOCXmnJdDkxKErZdt3kcjr0N9oqVthZIN9Om4g6msNmjYJlqhUHHfetQSHSwo6S7J1Skj5zoojSwsN4DOzNyRFPUNb1+o70n47/c0S1YqFMX3FlaByPSLI8Q32msqpdZNthEOUkfbXg2TkHMmCSnYYpTSM5ZLq/mWRb0io5QdiMFJI7gu7iav1ZfZysuptKj8SjpE+VoTrMs5P1BxT8xlJBVsnyHKDin0iUQcyGkgW0yiK7H8xKUnDsy884hCA2T8Q6QMPuYKo7nmoCyZyNiBZdrk86dSp9Z+8RGgc4uIzm3O1mXHfnWVfUxi0rviNiopQJk35YmT2T/LuAf4Z/URsbUlLYNr3SIjy6hldG35Zj1CiWRyITESJIG5vdULhfUXOkMLg4U+8kkhNuyUkeeh/aF2ClzuXsSRbyMHHC9Yaq6GrkZHgm3oRr94Bm/whsXDxzL+O8T6cpKT3jbWKWo1GSlD/ADEwhJ+Uan6QG4g4gJbmE0+lBHbLNu0c2T4+fhFVMbMeBIsRD6vVaXkkKU66lOulzvC/os8rGXFfDOGQgiRmKk12yTu4hJzqBAB5JItYwLVmrPKC1PvqceXupRvBR+FiXl5vjnTZl4ZzKyk3NIzAFOZLKrXvy1P22iwMQQFjBdniPDHk69V5NM3dS/aVrdBPIFRyj0AEAwp6w2oka7wcvJLtHku6AC0DYHbeKlMoUOrBBsraM4GJuaJRQAlZQJRhxV3WyFA7jSCyVlJcJ7jeY9SbxGpMm3fvCxMWoaSyyopUbRBgSYa5jMK7FtS3VBDaUkkbAC28cwcbMaDENUNNkFn2CWUe9f8Aqr6+Q5QwON+OVMSTtJpzhCnBZ1YP/iI57mRZGZVypStPGG3jNJz7W/qLvIan4+tZHI01HhHyE9/UXtraJbst2ZZQValNz59IjrBS5ZOhFxDuJZtZy9ucpJQoHUiPkBTfccSUqtsRyMYS6lJVpe1u9bpGU67MOzOdxRWdBfwAsB9hHKnbnyXkpeYve98pP2g3oz3u3EEpOI/pTAQ7bfUGyvW4P1gHS0lxhwAnOg5reHUQW0RKalSEyi1ETEuouNK5nTUD7KHrEMo4hcR5n//Z' />}
                                                        label={this.state.studentsListByEstablishment.map(student => {
                                                            if (student.id == data) {
                                                                return (
                                                                    student.id + " " + student.surname.toUpperCase() + " " + jsUpperCaseFirst(student.name)
                                                                );
                                                            }
                                                        })}

                                                        onDelete={this.handleRequestDelete(data)}
                                                    />
                                                </List>

                                        );
                                    })}
                                </div>
                            </div>)
                        }
                    })()}
                    <br /><br /><br />
                </div>

                )
            default:
                return 'Unknown step';
        }
    }

    handleToggle() {
        this.props.cancelModal();
    };

    render() {
        const steps = this.state.steps;
        const { activeStep } = this.state;
        return (
            <div className="container">
                <Auxiliary>
                    <Modal isOpen={this.state.previewVisible} >
                        <ModalHeader toggle={this.handleToggle.bind(this)} className="modal-box-header bg-primary text-white" >{<IntlMessages id="sidebar.classes" />}</ModalHeader>
                        <ModalBody>
                            <form className="row" noValidate autoComplete="off" onSubmit={this.handleSubmit}>
                                <div className="w-100">
                                    <Stepper activeStep={activeStep} orientation="vertical">
                                        {steps.map((label, index) => {
                                            return (
                                                <Step key={label}>
                                                    <StepLabel>{label}</StepLabel>
                                                    <StepContent className="pb-3">
                                                        <Typography component={'span'} variant={'body2'}>
                                                            {this.getStepContent(index)}
                                                        </Typography>
                                                        <div className="mt-2">
                                                            <div>
                                                                <Button
                                                                    disabled={activeStep === 0}
                                                                    onClick={this.handleBack}
                                                                    className="jr-btn"
                                                                >

                                                                    {<IntlMessages id="components.class.level.button.back" />}
                                                                </Button>
                                                                <Button
                                                                    variant="contained"
                                                                    color="primary"
                                                                    onClick={this.handleNext}
                                                                    className="jr-btn"
                                                                >
                                                                    {activeStep === steps.length - 1 ? <IntlMessages id="components.class.level.button.terminer" /> : <IntlMessages id="components.class.level.button.next" />}
                                                                </Button>
                                                            </div>
                                                        </div>
                                                    </StepContent>
                                                </Step>
                                            );
                                        })}
                                    </Stepper>
                                    {activeStep === steps.length && (
                                        <Paper square elevation={0} className="p-2">
                                            <Button onClick={this.handleReset} className="jr-btn">
                                                {<IntlMessages id="button.reset" />}
                                            </Button>
                                            <Button variant="contained" color="primary" onClick={this.handleSubmit} className="jr-btn">
                                                {<IntlMessages id="note.button.save" />}
                                            </Button>

                                        </Paper>
                                    )}
                                </div>
                            </form>
                        </ModalBody>
                    </Modal>
                </Auxiliary>
            </div>
        )
    }
}
export default connect()(AddClass);

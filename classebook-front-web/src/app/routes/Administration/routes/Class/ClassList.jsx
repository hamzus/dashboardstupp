import React from 'react';
import IntlMessages from 'util/IntlMessages';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import CardBox from 'components/CardBox/index';
import { connect } from "react-redux";
import Button from '@material-ui/core/Button';
import EditClass from './EditClass';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import {
  getClasses,
  addClass,
  archiverClass,
  updateClass
} from "../../../../../actions/classeAction";
import moment from 'moment';
import 'moment/locale/fr';



class Classlist extends React.Component {

  constructor() {
    super();
    this.state = {
      modal: false,
      archiveModal: false,
      idClass: '',
      classItem: [],
      anchorEl: undefined,
      menuState: false

    }
    this.archiver = this.archiver.bind(this)
    this.handleEdit = this.handleEdit.bind(this)
    this.handleClickOpen = this.handleClickOpen.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.editClose = this.editClose.bind(this)
    this.getEstblishmentName = this.getEstblishmentName.bind(this)

  }

  onOptionMenuSelect = event => {
    this.setState({ menuState: true, anchorEl: event.currentTarget });
  };
  handleRequestClose = () => {
    this.setState({ menuState: false });
  };

  handleClickOpen(e) {
    this.setState({
      archiveModal: true,
      idClass: e.currentTarget.value,
      menuState: false
    }, function () {

    });
  }
  editClose() {
    this.setState({
      modal: false
    })
  }

  handleClose() {
    this.setState({
      archiveModal: false
    });
  }
  getEstblishmentName() {



  }
  componentDidMount() {
  }

  handleEdit(e) {
    const classItem = this.props.classes.find(element => element.id == e.currentTarget.value)
    this.props.editCLass(classItem)
    this.setState({
      modal: true,
      classItem: classItem,
      menuState: false
    })

  }
  archiver() {
    this.props.dispatch(archiverClass(this.state.idClass))
    this.setState({
      archiveModal: false,
      menuState: false
    })

  }

  render() {
    const { anchorEl, menuState } = this.state;
    if (this.props.classes.length) {
      console.log(this.props.classes.length);
      return (
        <div className="animated slideInUpTiny animation-duration-3">
          {this.state.modal ? <EditClass modal={this.state.modal} students={this.props.students} cancelModal={this.editClose} classItem={this.state.classItem} professors={this.props.professors} establishments={this.props.establishments} /> : ""}

          <div className="row mb-md-3">
            <CardBox styleName="col-12" cardStyle="p-0" headerOutside>
              <div className="table-responsive-material">
                <Table className="default-table table-unbordered table table-sm table-hover">

                  <TableHead className="th-border-b" >
                    <TableRow >
                      <TableCell>{<IntlMessages id="components.student.formadd.etablissement" />}</TableCell>
                      <TableCell>{<IntlMessages id="components.class.formadd.name" />}</TableCell>
                      <TableCell>{<IntlMessages id="components.note.niveau" />}</TableCell>
                      <TableCell>{<IntlMessages id="components.establishments.formadd.number_students" />}</TableCell>
                      <TableCell>{<IntlMessages id="components.class.formadd.capacity" />}</TableCell>
                      <TableCell>{<IntlMessages id="room.description" />}</TableCell>
                      <TableCell>{<IntlMessages id="end.date" />}</TableCell>
                      <TableCell>{<IntlMessages id="start.date" />}</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>

                    {this.props.classes.map(classItem => {
                      
                      if (classItem.status) {
                        return (

                          <TableRow className="row" key={classItem.id}>
                            {this.props.establishments.map((establishment, index) => {
                              if (establishment.id === classItem.establishment_id) {
                                return (
                                  <TableCell key={index} >{establishment.name}</TableCell>
                                );
                              }
                            })}
                            <TableCell >{classItem.name}</TableCell>

                            <TableCell >{classItem.level_class}</TableCell>
                            <TableCell >{classItem.students_number}</TableCell>
                            <TableCell >{classItem.capacity}</TableCell>

                            <TableCell >{moment(classItem.start_date).format("DD/MM/YY")}</TableCell>
                            <TableCell >{moment(classItem.end_date).format("DD/MM/YY")}</TableCell>
                            <TableCell >{classItem.description}</TableCell>

                            <TableCell>
                              <IconButton onClick={this.onOptionMenuSelect.bind(this)}>
                                <i className="zmdi zmdi-more-vert" /></IconButton>
                              <Menu id="long-menu"
                                anchorEl={anchorEl}
                                open={menuState}
                                onClose={this.handleRequestClose.bind(this)}
                                MenuListProps={{
                                  style: {
                                    width: 150,
                                    paddingTop: 0,
                                    paddingBottom: 0
                                  },
                                }}>
                                <MenuItem onClick={this.handleEdit} value={classItem.id} >{<IntlMessages id="button.modify" />}</MenuItem>
                                <MenuItem onClick={this.handleClickOpen} value={classItem.id} >{<IntlMessages id="button.delete" />}</MenuItem>
                              </Menu>

                            </TableCell>

                          </TableRow>
                        );
                      }
                    })}
                  </TableBody>
                </Table>
              </div>
            </CardBox>
          </div>
          <Dialog
            open={this.state.archiveModal}
            onClose={this.handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">{<IntlMessages id="message.confirm.modal" />}</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                {<IntlMessages id="message.confirm.modal" />}
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose} color="primary">
                {<IntlMessages id="button.no" />}
              </Button>
              <Button onClick={this.archiver} color="primary" autoFocus>
                {<IntlMessages id="button.yes" />}

              </Button>
            </DialogActions>
          </Dialog>

        </div>)
    } else {
      return (<div>pas  de classe</div>)
    }
  }
}


export default connect()(Classlist);

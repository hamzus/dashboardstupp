
import React from 'react';
import IntlMessages from 'util/IntlMessages';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';
import Avatar from '@material-ui/core/Avatar';
import { addEstablishment } from "../../../../../actions/establishmentAction";
import { connect } from 'react-redux';
import Auxiliary from "util/Auxiliary";
import { Modal, ModalBody, ModalHeader } from "reactstrap";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const categories = [
  {
    value: 'Primary School',
    label: 'Ecole primaire',
  },
  {
    value: 'College',
    label: 'Collège',
  },
  {
    value: 'High School',
    label: 'Lycée',
  },
  {
    value: 'Universitie',
    label: 'Université',
  },
];
const mode_payment = [
  {
    value: 'Semestre',
    label: 'Semestre',
  },
  {
    value: 'Trimestre',
    label: 'Trimestre',
  },
  {
    value: 'Mensuel',
    label: 'Mensuel',
  },
  {
    value: 'Annuel',
    label: 'Annuel',
  }

];

class AddEstablishment extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      previewVisible: true,
      name: '',
      adress: '',
      code: '',
      city: '',
      country: '',
      phone: '',
      email_establishment: '',
      website: '',
      surname_director: '',
      name_director: '',
      email_director: '',
      phone_director: '',
      categories: '',
      number_students: '',
      contract_start_date: '',
      contract_end_date: '',
      number_sms: '',
      mode_payment: '',
      modules: []
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  handleChangeModule = event => {
    this.setState({ modules: event.target.value });
  };
  handleCancel() {
    this.setState({
      previewVisible: false,
      name: '',
      adress: '',
      code: '',
      city: '',
      country: '',
      phone: '',
      email_establishment: '',
      website: '',
      surname_director: '',
      name_director: '',
      email_director: '',
      phone_director: '',
      number_students: '',
      contract_start_date: '',
      contract_end_date: '',
      number_sms: '',
      mode_payment: '',
      modules: []
    });
    this.props.cancelModal();
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const name = this.state.name;
    const adress = this.state.adress;
    const code = this.state.code;
    const city = this.state.city;
    const country = this.state.country;
    const phone = this.state.phone;
    const email_establishment = this.state.email_establishment;
    const website = this.state.website;
    const surname_director = this.state.surname_director;
    const name_director = this.state.name_director;
    const email_director = this.state.email_director;
    const phone_director = this.state.phone_director;
    const categories = this.state.categories;
    const number_students = this.state.number_students;
    const contract_start_date = this.state.contract_start_date;
    const contract_end_date = this.state.contract_end_date;
    const number_sms = this.state.number_sms;
    const mode_payment = this.state.mode_payment;
    const module_id = this.state.modules;
    const data = {
      name, adress, code, city, country, phone, email_establishment, website, surname_director, name_director, email_director, phone_director, categories, number_students, contract_start_date, contract_end_date, number_sms, mode_payment, module_id
    }
    this.props.dispatch(addEstablishment(data));
    this.setState({
      previewVisible: false,
      name: '',
      adress: '',
      code: '',
      city: '',
      country: '',
      phone: '',
      email_establishment: '',
      website: '',
      surname_director: '',
      name_director: '',
      email_director: '',
      phone_director: '',
      number_students: '',
      contract_start_date: '',
      contract_end_date: '',
      number_sms: '',
      mode_payment: '',
      modules: []
    });
    this.props.cancelModal();
  };

  render() {

    return (
      <div className="app-wrapper">
        <Auxiliary>

          <Modal isOpen={this.state.previewVisible} >
            <ModalHeader toggle={this.handleCancel} className="modal-box-header bg-primary text-white" >{<IntlMessages id="pages.establishementPage" />}</ModalHeader>
            <ModalBody>
              <form className="row" noValidate autoComplete="off" onSubmit={this.handleSubmit}>
                <div className="col-sm-12">
                  <h1>{<IntlMessages id="component.etablishments.info.general" />}</h1>
                </div>

                <div className="col-sm-6">
                  <TextField
                    required
                    name='name'
                    id="name"
                    label={<IntlMessages id="components.establishments.formadd.name" />}
                    onChange={this.handleChange('name')}
                    margin="normal"
                    fullWidth
                    value={this.state.name}
                  />
                </div>
                <div className="col-sm-6">
                  <TextField
                    required
                    id="adress"
                    name='adress'
                    label={<IntlMessages id="components.establishments.formadd.adress" />}
                    onChange={this.handleChange('adress')}
                    value={this.state.adress}


                    margin="normal"
                    fullWidth
                  />
                </div>
                <div className="col-sm-4">
                  <TextField
                    required
                    id="code"
                    name="code"
                    type="number"
                    onChange={this.handleChange('code')}
                    value={this.state.code}
                    label={<IntlMessages id="components.establishments.formadd.codezip" />}
                    margin="normal"
                    fullWidth
                  />
                </div>

                <div className="col-sm-4">
                  <TextField
                    required
                    id="city"
                    onChange={this.handleChange('city')}
                    value={this.state.city}
                    label={<IntlMessages id="components.establishments.formadd.city" />}
                    margin="normal"
                    fullWidth
                  />
                </div>
                <div className="col-sm-4">
                  <TextField
                    required
                    id="country"
                    onChange={this.handleChange('country')}
                    value={this.state.country}
                    label={<IntlMessages id="components.establishments.formadd.country" />}
                    margin="normal"
                    fullWidth
                  />
                </div>

                <div className="col-sm-4">
                  <TextField
                    required
                    id="phone"
                    onChange={this.handleChange('phone')}
                    value={this.state.phone}
                    label={<IntlMessages id="components.establishments.formadd.phone" />}
                    type="number"
                    margin="normal"
                    fullWidth
                  />

                </div>
                <div className="col-sm-4">
                  <TextField
                    id="email_establishment"
                    type="email"
                    onChange={this.handleChange('email_establishment')}
                    value={this.state.email_establishment}
                    label={<IntlMessages id="components.establishments.formadd.email_establishment" />}
                    margin="normal"
                    fullWidth
                  />
                </div>
                <div className="col-sm-4">
                  <TextField
                    id="website"
                    onChange={this.handleChange('website')}
                    value={this.state.website}
                    label={<IntlMessages id="components.establishments.formadd.website" />}
                    type="url"
                    margin="normal"
                    fullWidth
                  />
                </div>

                <div className="col-sm-12">
                  <br /><br />
                  <h1>{<IntlMessages id="component.etablishments.info.director" />}</h1>
                </div>
                <div className="col-sm-6">
                  <TextField
                    required
                    id="surname_director"
                    onChange={this.handleChange('surname_director')}
                    value={this.state.surname_director}
                    label={<IntlMessages id="components.establishments.formadd.surname_director" />}
                    margin="normal"
                    fullWidth
                  />
                </div>
                <div className="col-sm-6">
                  <TextField
                    required
                    id="name_director"
                    onChange={this.handleChange('name_director')}
                    value={this.state.name_director}
                    label={<IntlMessages id="components.establishments.formadd.name_director" />}
                    margin="normal"
                    fullWidth
                  />
                </div>

                <div className="col-sm-6">
                  <TextField
                    id="email_director"
                    type="email"
                    onChange={this.handleChange('email_director')}
                    value={this.state.email_director}
                    label={<IntlMessages id="components.establishments.formadd.email_director" />}
                    margin="normal"
                    fullWidth
                  />
                </div>

                <div className="col-sm-6">
                  <TextField
                    required
                    id="phone_director"
                    onChange={this.handleChange('phone_director')}
                    value={this.state.phone_director}
                    label={<IntlMessages id="components.establishments.formadd.phone_director" />}
                    type="number"
                    margin="normal"
                    fullWidth
                  />
                </div>

                <div className="col-sm-12">
                  <br /><br />
                  <h1>{<IntlMessages id="component.etablishments.info.licence" />}</h1>
                </div>

                <div className="col-sm-6">
                  <TextField
                    required
                    id="categories"
                    onChange={this.handleChange('categories')}
                    select
                    label={<IntlMessages id="components.establishments.formadd.Categories" />}
                    value={this.state.categories}
                    SelectProps={{}}
                    margin="normal"
                    fullWidth
                  >
                    {categories.map(option => (
                      <MenuItem key={option.value} value={option.value}>
                        {option.label}
                      </MenuItem>
                    ))}
                  </TextField>
                </div>

                <div className="col-sm-6">
                  <TextField
                    required
                    id="number_students"
                    onChange={this.handleChange('number_students')}
                    value={this.state.number_students}
                    label={<IntlMessages id="components.establishments.formadd.number_students" />}
                    type="number"
                    margin="normal"
                    fullWidth
                  />
                </div>

                <div className="col-sm-6">
                  <TextField
                    required
                    id="contract_start_date"
                    type="email"
                    onChange={this.handleChange('contract_start_date')}
                    value={this.state.contract_start_date}
                    type="date"
                    helperText={<IntlMessages id="components.establishments.formadd.contract_start_date" />}
                    margin="normal"
                    fullWidth
                  />
                </div>

                <div className="col-sm-6">
                  <TextField
                    required
                    id="contract_end_date"
                    type="email"
                    onChange={this.handleChange('contract_end_date')}
                    value={this.state.contract_end_date}
                    type="date"
                    margin="normal"
                    helperText={<IntlMessages id="components.establishments.formadd.contract_end_date" />}
                    fullWidth
                  />
                </div>
                <div className="col-sm-6">
                  <TextField
                    required
                    id="number_sms"
                    onChange={this.handleChange('number_sms')}
                    value={this.state.number_sms}
                    label={<IntlMessages id="components.establishments.formadd.number_sms" />}
                    type="number"
                    margin="normal"
                    fullWidth
                  />
                </div>

                <div className="col-sm-6">
                  <TextField
                    required
                    id="mode_payment"
                    onChange={this.handleChange('mode_payment')}
                    value={this.state.mode_payment}
                    select
                    label={<IntlMessages id="components.establishments.formadd.mode_payment" />}
                    SelectProps={{}}
                    margin="normal"
                    fullWidth
                  >
                    {mode_payment.map(option => (
                      <MenuItem key={option.value} value={option.value}>
                        {option.label}
                      </MenuItem>
                    ))}
                  </TextField>
                </div>
                <div className="col-sm-12">
                  <br /><br />
                  <h1>{<IntlMessages id="sidebar.modules" />}</h1>
                </div>
                <div className="col-sm-6">
                  <FormControl className="w-100">
                    <InputLabel htmlFor="name-multiple">{<IntlMessages id="sidebar.modules" />}</InputLabel>
                    <Select
                      multiple
                      name="modules"
                      value={this.state.modules}
                      onChange={this.handleChangeModule}
                      input={<Input id="name-multiple" />}
                      MenuProps={{
                        PaperProps: {
                          style: {
                            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                            width: 200,
                          },
                        },
                      }}
                    >
                      {
                        this.props.moduleList.map((moduleEstablishment, index) => (
                          <MenuItem
                            key={index}
                            value={moduleEstablishment.id}
                          >
                            {moduleEstablishment.name}
                          </MenuItem>
                        ))}
                    </Select>
                  </FormControl>
                </div>
                <div className="col-sm-6">
                  <div className="manage-margin d-flex flex-wrap">

                    {this.state.modules.map(data => {
                      return (
                        <Chip
                          avatar={<Avatar
                            src={require('../../../../../assets/images/module.jpg')} />}
                          label={this.props.moduleList.map(moduleEstablishment => {
                            if (moduleEstablishment.id == data) {
                              return (
                                moduleEstablishment.name
                              );
                            }
                          })}
                          key={data.id}
                        />
                      );
                    })}
                  </div>
                </div>
                <br /><br /><br /><br /><br />
                <div className="col-sm-12">
                  <h4><font color="red">*</font> {<IntlMessages id="component.required_fields" />}</h4>
                </div>
                <div className="col-md-12 text-right ">
                  <br /><br />
                  <Button variant="contained" className="jr-btn bg-indigo text-white " type="submit" >{<IntlMessages id="components.establishments.formadd.buttonAdd" />}</Button>
                  <Button variant="contained" className="jr-btn bg-grey text-white " onClick={this.handleCancel}>{<IntlMessages id="components.establishments.formadd.buttonCancel" />}</Button>
                </div>
              </form>
            </ModalBody>
          </Modal>
        </Auxiliary>
      </div>
    );
  }
}

export default connect()(AddEstablishment);

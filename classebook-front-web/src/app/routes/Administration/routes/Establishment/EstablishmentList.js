
import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import CardBox from 'components/CardBox/index';
import IntlMessages from 'util/IntlMessages';
import EditEstablishment from './EditEstablishment';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { ListItem, List } from '@material-ui/core';

var dateFormat = require('dateformat');

class EstablishmentList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      anchorEl: undefined,
      menuState: false,
      edit: false,
      currency_Categories: 'Primary School',
      currency_mode_payment: 'Semestre',
      list: [],
      establishmentItem: []
    };
    this.handleEdit = this.handleEdit.bind(this);
    this.handleRequestDelete = this.handleRequestDelete.bind(this);
    this.handleCancelModal = this.handleCancelModal.bind(this);
  };

  onOptionMenuSelect = event => {
    this.setState({ menuState: true, anchorEl: event.currentTarget });
  };
  handleRequestClose = () => {
    this.setState({ menuState: false });
  };

  handleEdit(e) {
    e.preventDefault();
    const establishmentItem = this.props.list.find(element => element.id == e.currentTarget.value)
    this.setState({
      menuState: false,
      edit: true,
      establishmentItem: establishmentItem

    })
  };

  handleRequestDelete(e) {
    e.preventDefault();
    this.setState({ menuState: false });
    this.props.RequestDeleteEstablishment(e.currentTarget.value);
  };
  handleCancelModal() {
    this.setState({ edit: false })
  };

  render() {
    const { anchorEl, menuState } = this.state;
    const list = this.props.list;
    return (
      <div className="app-wrapper">
        {this.state.edit ? <EditEstablishment modal={this.state.edit} establishment={this.state.establishmentItem} cancelModal={this.handleCancelModal} moduleList={this.props.moduleList} /> : ""}
        <CardBox styleName="col-lg-12" >
          <div className="table-responsive-material">
            <Table className="default-table table-unbordered table table-sm table-hover">
              <TableHead className="th-border-b">
                <TableRow>
                  <TableCell>{<IntlMessages id="components.establishments.formadd.name" />}</TableCell>
                  <TableCell>{<IntlMessages id="components.establishments.formadd.city" />}</TableCell>
                  <TableCell>{<IntlMessages id="components.establishments.formadd.country" />}</TableCell>
                  <TableCell>{<IntlMessages id="components.establishments.formadd.phone" />}</TableCell>
                  <TableCell>{<IntlMessages id="components.establishments.formadd.contract_end_date" />} </TableCell>
                  <TableCell >{<IntlMessages id="sidebar.modules" />} </TableCell>
                  <TableCell></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {list.map((establishment, index) => {
                  return (
                    <TableRow key={index}>
                      <TableCell>{establishment.name}</TableCell>
                      <TableCell>{establishment.city}</TableCell>
                      <TableCell>{establishment.country}</TableCell>
                      <TableCell>{establishment.phone}</TableCell>
                      <TableCell>{dateFormat(establishment.contract_end_date, "fullDate")}</TableCell>
                      {establishment.module_id.map((moduleId, index) => {
                        return (
                          <List>
                            <TableCell  key={index} >

                              {this.props.moduleList.map(moduleEstablishment => {
                                if (moduleEstablishment.id == moduleId) {
                                  return (
                                    moduleEstablishment.name
                                  );
                                }
                              })}

                            </TableCell> </List>

                        );
                      })}
                      <TableCell></TableCell>
                      <TableCell>
                        <IconButton onClick={this.onOptionMenuSelect.bind(this)}>
                          <i className="zmdi zmdi-more-vert" /></IconButton>
                        <Menu id="long-menu"
                          anchorEl={anchorEl}
                          open={menuState}
                          onClose={this.handleRequestClose.bind(this)}
                          MenuListProps={{
                            style: {
                              width: 150,
                              paddingTop: 0,
                              paddingBottom: 0
                            },
                          }}>
                          <MenuItem onClick={this.handleEdit} value={establishment.id} >{<IntlMessages id="button.modify" />}</MenuItem>
                          <MenuItem onClick={this.handleRequestDelete} value={establishment.id} >{<IntlMessages id="button.delete" />}</MenuItem>
                        </Menu>

                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </div>
        </CardBox>
      </div>

    );
  }
}

export default EstablishmentList;



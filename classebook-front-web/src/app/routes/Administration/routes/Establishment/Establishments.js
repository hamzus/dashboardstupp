
import React from 'react';
import ContainerHeader from 'components/ContainerHeader/index';
import IntlMessages from 'util/IntlMessages';
import EstablishmentList from './EstablishmentList';
import AddEstablishment from './AddEstablishment';
import ModalDeleteEstablishment from './ModalDeleteEstablishment';
import { getEstablishment } from "../../../../../actions/establishmentAction";
import { connect } from "react-redux";
import { getModules } from '../../../../../actions/ModuleAction';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import ImportExport from '@material-ui/icons/ImportExport';
import Input from '@material-ui/icons/Input';

function mapStateToProps(state) {
    return {
        establishments: state.establishment.remoteEstablishments,
        modules: state.module.remoteModules
    };
}

class Establishments extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            edit: false,
            modalDelete: false,
            itemId: 0,
            establishmentItem: [],
            addEstablishmentModal: false

        };
        this.EditItemEstablishment = this.EditItemEstablishment.bind(this);
        this.handleCancelModal = this.handleCancelModal.bind(this);
        this.handleCancelModalDelete = this.handleCancelModalDelete.bind(this);
        this.RequestDeleteEstablishment = this.RequestDeleteEstablishment.bind(this);
        this.addEstablishment = this.addEstablishment.bind(this);
    };

    componentDidMount() {
        this.props.getEstablishment();
        this.props.getModules();
    };

    handleCancelModal() {
        this.setState({ edit: false, addEstablishmentModal: false })
    };

    handleCancelModalDelete() {
        this.setState({ modalDelete: false, itemId: 0 })
    };

    EditItemEstablishment(id) {
        const establishmentItem = this.props.establishments.find(element => element.id === id);
        this.setState({ establishmentItem, edit: true }, function () {
        });
    };

    RequestDeleteEstablishment(id) {
        this.setState({ modalDelete: true, itemId: id }, function () {
        });

    };
    addEstablishment() {
        this.setState({ addEstablishmentModal: true });
    }

    render() {

        return (
            <div className="app-wrapper">
                <ContainerHeader match={this.props.match} title={<IntlMessages id="pages.establishementPage" />} />
                <div className="col-md-12 text-right ">
                    <Fab size="small" color="primary" aria-label="Add" onClick={this.addEstablishment}>
                        <AddIcon />
                    </Fab>
                    &nbsp;&nbsp;&nbsp;
                    <Fab size="small" color="primary"  >
                    <Input />
                    </Fab>
                </div>
                {this.state.addEstablishmentModal ? <AddEstablishment moduleList={this.props.modules}  cancelModal={this.handleCancelModal}/> : ''}
                <EstablishmentList editEstablishment={this.EditItemEstablishment} RequestDeleteEstablishment={this.RequestDeleteEstablishment} list={this.props.establishments} moduleList={this.props.modules} />
                {this.state.modalDelete ? <ModalDeleteEstablishment itemId={this.state.itemId} cancelModalDelete={this.handleCancelModalDelete} /> : ''}
            </div>
        );
    }
}

export default connect(mapStateToProps, { getEstablishment, getModules })(Establishments);

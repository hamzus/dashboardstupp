
import React from "react";
import { Modal, ModalBody, ModalHeader } from "reactstrap";
import IntlMessages from 'util/IntlMessages';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Auxiliary from "util/Auxiliary";
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';
import Avatar from '@material-ui/core/Avatar';
import { connect } from "react-redux";
import { editData } from "../../../../../actions/establishmentAction";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const categories = [
    {
        value: 'Primary School',
        label: 'Ecole primaire',
    },
    {
        value: 'College',
        label: 'Collège',
    },
    {
        value: 'High School',
        label: 'Lycée',
    },
    {
        value: 'Universities',
        label: 'Universités',
    },
];
const mode_payment = [
    {
        value: 'Semestre',
        label: 'Semestre',
    },
    {
        value: 'Trimestre',
        label: 'Trimestre',
    },
    {
        value: 'Mensuel',
        label: 'Mensuel',
    },
    {
        value: 'Annuel',
        label: 'Annuel',
    }

];

class EditEstablishment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            previewVisible: this.props.modal,
            name: '',
            adress: '',
            code: '',
            city: '',
            country: '',
            phone: '',
            email_establishment: '',
            website: '',
            surname_director: '',
            name_director: '',
            email_director: '',
            phone_director: '',
            categories: '',
            number_students: '',
            contract_start_date: '',
            contract_end_date: '',
            number_sms: '',
            mode_payment: '',
            status: '',
            id: '',
            modules: []

        };
        this.handleChange = this.handleChange.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeModule = this.handleChangeModule.bind(this)
    };

    componentDidMount() {
        const contract_start_date = this.props.establishment.contract_start_date;
        const newStartDate = contract_start_date.split('T');
        const contract_end_date = this.props.establishment.contract_end_date;
        const newEndDate = contract_end_date.split('T');
        this.setState({
            name: this.props.establishment.name,
            adress: this.props.establishment.adress,
            code: this.props.establishment.code,
            city: this.props.establishment.city,
            country: this.props.establishment.country,
            phone: this.props.establishment.phone,
            email_establishment: this.props.establishment.email_establishment,
            website: this.props.establishment.website,
            surname_director: this.props.establishment.surname_director,
            name_director: this.props.establishment.name_director,
            email_director: this.props.establishment.email_director,
            phone_director: this.props.establishment.phone_director,
            categories: this.props.establishment.categories,
            number_students: this.props.establishment.number_students,
            contract_start_date: newStartDate[0],
            contract_end_date: newEndDate[0],
            number_sms: this.props.establishment.number_sms,
            mode_payment: this.props.establishment.mode_payment,
            status: this.props.establishment.status,
            id: this.props.establishment.id,
            modules: Array.from(this.props.establishment.module_id)
        })
    };

    handleCancel(e) {
        this.props.cancelModal()
    };

    handleChangeModule = event => {
        this.setState({ modules: event.target.value });

    };
    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    };

    handleToggle() {
        this.props.cancelModal();
    };

    handleSubmit = (e) => {

        e.preventDefault();

        const data = {
            name: this.state.name,
            adress: this.state.adress,
            code: this.state.code,
            city: this.state.city,
            country: this.state.country,
            phone: this.state.phone,
            email_establishment: this.state.email_establishment,
            website: this.state.website,
            surname_director: this.state.surname_director,
            name_director: this.state.name_director,
            email_director: this.state.email_director,
            phone_director: this.state.phone_director,
            categories: this.state.categories,
            number_students: this.state.number_students,
            contract_start_date: this.state.contract_start_date,
            contract_end_date: this.state.contract_end_date,
            number_sms: this.state.number_sms,
            mode_payment: this.state.mode_payment,
            status: this.state.status,
            module_id: this.state.modules,
            id: this.state.id,
        }
        this.props.dispatch(editData(data));
        this.props.cancelModal();
    };

    render() {
        return (
            <Auxiliary>

                <Modal isOpen={this.props.modal} >
                    <ModalHeader >{<IntlMessages id="pages.establishementPage" />}</ModalHeader>
                    <ModalBody>
                        <form className="row" noValidate autoComplete="off" onSubmit={this.handleSubmit}>
                            <div className="col-sm-12">
                                <h1>{<IntlMessages id="component.etablishments.info.general" />}</h1>
                            </div>

                            <div className="col-sm-6">
                                <TextField
                                    required
                                    id="name"
                                    name="name"
                                    label={<IntlMessages id="components.establishments.formadd.name" />}
                                    onChange={this.handleChange}
                                    value={this.state.name}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    required
                                    id="adress"
                                    name="adress"
                                    label={<IntlMessages id="components.establishments.formadd.adress" />}
                                    onChange={this.handleChange}
                                    value={this.state.adress}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-4">
                                <TextField
                                    required
                                    id="code"
                                    name="code"
                                    type="number"
                                    label={<IntlMessages id="components.establishments.formadd.codezip" />}
                                    onChange={this.handleChange}
                                    value={this.state.code}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>

                            <div className="col-sm-4">
                                <TextField
                                    required
                                    id="city"
                                    name="city"
                                    label={<IntlMessages id="components.establishments.formadd.city" />}
                                    onChange={this.handleChange}
                                    value={this.state.city}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-4">
                                <TextField
                                    required
                                    id="country"
                                    name="country"
                                    label={<IntlMessages id="components.establishments.formadd.country" />}
                                    onChange={this.handleChange}
                                    value={this.state.country}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>

                            <div className="col-sm-4">
                                <TextField
                                    required
                                    id="phone"
                                    name="phone"
                                    label={<IntlMessages id="components.establishments.formadd.phone" />}
                                    onChange={this.handleChange}
                                    value={this.state.phone}
                                    type="number"
                                    margin="normal"
                                    fullWidth
                                />

                            </div>
                            <div className="col-sm-4">
                                <TextField
                                    id="email_establishment"
                                    name="email_establishment"
                                    type="email"
                                    label={<IntlMessages id="components.establishments.formadd.email_establishment" />}
                                    onChange={this.handleChange}
                                    value={this.state.email_establishment}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-4">
                                <TextField
                                    id="website"
                                    name="website"
                                    label={<IntlMessages id="components.establishments.formadd.website" />}
                                    onChange={this.handleChange}
                                    value={this.state.website}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>

                            <div className="col-sm-12">
                                <br /><br />
                                <h1>{<IntlMessages id="component.etablishments.info.director" />}</h1>
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    required
                                    id="surname_director"
                                    name="surname_director"
                                    label={<IntlMessages id="components.establishments.formadd.surname_director" />}
                                    onChange={this.handleChange}
                                    value={this.state.surname_director}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    required
                                    id="name_director"
                                    name="name_director"
                                    label={<IntlMessages id="components.establishments.formadd.name_director" />}
                                    onChange={this.handleChange}
                                    value={this.state.name_director}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    id="email_director"
                                    name="email_director"
                                    type="email"
                                    label={<IntlMessages id="components.establishments.formadd.email_director" />}
                                    onChange={this.handleChange}
                                    value={this.state.email_director}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>

                            <div className="col-sm-6">
                                <TextField
                                    required
                                    id="phone_director"
                                    name="phone_director"
                                    label={<IntlMessages id="components.establishments.formadd.phone_director" />}
                                    onChange={this.handleChange}
                                    value={this.state.phone_director}
                                    type="number"
                                    margin="normal"
                                    fullWidth
                                />
                            </div>

                            <div className="col-sm-12">
                                <br /><br />
                                <h1>{<IntlMessages id="component.etablishments.info.licence" />}</h1>
                            </div>

                            <div className="col-sm-6">
                                <TextField
                                    required
                                    id="categories"
                                    name="categories"
                                    select
                                    label={<IntlMessages id="components.establishments.formadd.Categories" />}
                                    value={this.state.categories}
                                    onChange={this.handleChange}
                                    SelectProps={{}}
                                    margin="normal"
                                    fullWidth
                                >
                                    {categories.map(option => (
                                        <MenuItem key={option.value} value={option.value}>
                                            {option.label}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div>

                            <div className="col-sm-6">
                                <TextField
                                    required
                                    id="number_students"
                                    name="number_students"
                                    label={<IntlMessages id="components.establishments.formadd.number_students" />}
                                    onChange={this.handleChange}
                                    value={this.state.number_students}
                                    type="number"
                                    margin="normal"
                                    fullWidth
                                />
                            </div>

                            <div className="col-sm-6">
                                <TextField
                                    required
                                    id="contract_start_date"
                                    name="contract_start_date"
                                    type="date"
                                    helperText={<IntlMessages id="components.establishments.formadd.contract_start_date" />}
                                    onChange={this.handleChange}
                                    value={this.state.contract_start_date}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>

                            <div className="col-sm-6">
                                <TextField
                                    required
                                    id="contract_end_date"
                                    name="contract_end_date"
                                    type="date"
                                    margin="normal"
                                    helperText={<IntlMessages id="components.establishments.formadd.contract_end_date" />}
                                    onChange={this.handleChange}
                                    value={this.state.contract_end_date}
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    required
                                    id="number_sms"
                                    name="number_sms"
                                    label={<IntlMessages id="components.establishments.formadd.number_sms" />}
                                    onChange={this.handleChange}
                                    value={this.state.number_sms}
                                    type="number"
                                    margin="normal"
                                    fullWidth
                                />
                            </div>

                            <div className="col-sm-6">
                                <TextField
                                    required
                                    id="mode_payment"
                                    name="mode_payment"
                                    select
                                    label={<IntlMessages id="components.establishments.formadd.mode_payment" />}
                                    value={this.state.mode_payment}
                                    onChange={this.handleChange}
                                    SelectProps={{}}
                                    margin="normal"
                                    fullWidth
                                >
                                    {mode_payment.map(option => (
                                        <MenuItem key={option.value} value={option.value}>
                                            {option.label}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div>
                            <div className="col-sm-12">
                                <br /><br />
                                <h1>{<IntlMessages id="sidebar.modules" />}</h1>
                            </div>
                            <div className="col-sm-6">
                                <FormControl className="w-100">
                                    <InputLabel htmlFor="name-multiple">{<IntlMessages id="sidebar.modules" />}</InputLabel>
                                    <Select
                                        multiple
                                        name="modules"
                                        value={this.state.modules}
                                        onChange={this.handleChangeModule}
                                        input={<Input id="name-multiple" />}
                                        MenuProps={{
                                            PaperProps: {
                                                style: {
                                                    maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                                                    width: 200,
                                                },
                                            },
                                        }}
                                    >
                                        {
                                            this.props.moduleList.map((moduleEstablishment, index) => (
                                                <MenuItem
                                                    key={index}
                                                    value={moduleEstablishment.id}
                                                >
                                                    {moduleEstablishment.name}
                                                </MenuItem>
                                            ))}
                                    </Select>
                                </FormControl>
                            </div>
                            <div className="col-sm-6">
                                <div className="manage-margin d-flex flex-wrap">

                                    {this.state.modules.map(data => {
                                        return (
                                            <Chip
                                            avatar={<Avatar
                                            src={require('../../../../../assets/images/module.jpg')} />}
                                            label =  {this.props.moduleList.map(moduleEstablishment  => {
                                            if (moduleEstablishment.id == data){
                                              return (
                                                moduleEstablishment.name
                                            );
                                            }
                                            })}
                                            key={data.id}
                                          />
                                        );
                                    })}
                                </div>
                            </div>
                            <div className="col-md-12 text-right ">
                                <br /><br />
                                <Button variant="contained" className="jr-btn bg-indigo text-white " type="submit" >{<IntlMessages id="components.establishments.formModify.buttonModify" />}</Button>
                                <Button variant="contained" className="jr-btn bg-grey text-white " onClick={this.handleCancel}>{<IntlMessages id="components.establishments.formadd.buttonCancel" />}</Button>
                            </div>
                        </form>
                    </ModalBody>
                </Modal>
            </Auxiliary>

        )
    }

}

export default connect()(EditEstablishment);

import React from 'react';
import ContainerHeader from 'components/ContainerHeader/index';
import IntlMessages from 'util/IntlMessages';
import AddRoom from './AddRoom';
import RoomList from './RoomList';
import EditRoom from './EditRoom';
import DeleteRoom from './DeleteRoom'
import baseUrl from 'config/config';
import { UncontrolledAlert } from 'reactstrap';
import { getEstablishment } from "../../../../../actions/establishmentAction";
import { connect } from "react-redux";
import axios from 'axios';
import Input from '@material-ui/icons/Input';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

function mapStateToProps(state) {
    return {
        establishments: state.establishment.remoteEstablishments
    };
};

class Rooms extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            addRoomModal: false,
            alert_success: false,
            edit: false,
            Rooms_list: [],
            room_Item: [],
            modal_delete: false,
            item_id: 0,
        };
        this.AddItemRoom = this.AddItemRoom.bind(this);
        this.EditItemRoom = this.EditItemRoom.bind(this);
        this.handleCancelModal = this.handleCancelModal.bind(this);
        this.handleSubmitEdit = this.handleSubmitEdit.bind(this);
        this.RequestDeleteRoom = this.RequestDeleteRoom.bind(this);
        this.handleAnnuleModalDelete = this.handleAnnuleModalDelete.bind(this);
        this.deleteItemRoom = this.deleteItemRoom.bind(this);
        this.addRoom = this.addRoom.bind(this);
    };

    addRoom() {
        this.setState({ addRoomModal: true });
    };

    componentWillMount() {
        this.props.getEstablishment();
        axios.get(`${baseUrl.baseUrl}/rooms`)
            .then(res => {
                const Room_list = res.data;
                const Rooms_list = Room_list.filter(element => element.status);
                this.setState({ Rooms_list });

            });
    }
    AddItemRoom(room) {
        axios.post(`${baseUrl.baseUrl}/rooms`, {
            name: room.name,
            description: room.description,
            status: true,
            id_establishment: room.id_establishment
        })
            .then(res => {

                this.setState({
                    Rooms_list: [...this.state.Rooms_list, res.data],
                    alert_success: true
                })
            }).catch(function (error) {
                alert('Merci de vérifier votre champ obligatoire')
            });
    }
    EditItemRoom(id) {

        const room_Item = this.state.Rooms_list.find(element => element.id == id);
        this.setState({ room_Item: room_Item, edit: true });
    }
    handleSubmitEdit(room) {
        axios.put(`${baseUrl.baseUrl}/rooms/` + room.id, {
            name: room.name,
            description: room.description,
            status: room.status,
            id_establishment: room.id_establishment,
            id: room.id,

        })
            .then(res => {
                this.setState({
                    Rooms_list: [...this.state.Rooms_list.filter(element => element.id != room.id), res.data],
                    edit: false
                })
            }).catch(function (error) {
                alert('error')
            });

    }
    deleteItemRoom(idItem) {
        axios.get(`${baseUrl.baseUrl}/rooms/` + idItem)
            .then(res => {
                const data_room = res.data;
                axios.put(`${baseUrl.baseUrl}/rooms/` + data_room.id, {
                    name: data_room.name,
                    description: data_room.description,
                    status: false,
                    id_establishment: data_room.id_establishment,
                    id: data_room.id,
                })
                    .then(res => {

                        this.setState({
                            Rooms_list: [...this.state.Rooms_list.filter(element => element.id != data_room.id)],
                        })
                    }).catch(function (error) {
                        alert('error')
                    });
            });
    }
    RequestDeleteRoom(id) {
        this.setState({ modal_delete: true, item_id: id });
    }

    handleCancelModal() {
        this.setState({ edit: false, addRoomModal: false })
    }
    handleAnnuleModalDelete() {
        this.setState({ modal_delete: false, item_id: 0 })
    }
    render() {
        return (
            <div className="app-wrapper">
                <ContainerHeader match={this.props.match} title={<IntlMessages id="sidebar.rooms" />} />
                {this.state.alert_success ? <UncontrolledAlert className="alert-addon-card bg-success bg-success text-white shadow-lg">
                    <span className="icon-addon alert-addon">
                        <i className="zmdi zmdi-cloud-done zmdi-hc-fw zmdi-hc-lg" />
                    </span>
                    <span className="d-inline-block"> {<IntlMessages id="message.success.add_room" />} </span>
                </UncontrolledAlert> : ''}
                <div className="col-md-12 text-right ">
                    <Fab size="small" color="primary" aria-label="Add" onClick={this.addRoom}>
                        <AddIcon />
                    </Fab>
                    &nbsp;&nbsp;&nbsp;
                        <Fab size="small" color="primary" >
                        <Input />
                    </Fab>
                </div>

                {this.state.addRoomModal ? <AddRoom ListEstablishment={this.props.establishments} handleSubmit={this.AddItemRoom} cancelModal={this.handleCancelModal} /> : ''}
                <RoomList rooms={this.state.Rooms_list} ListEstablishment={this.props.establishments} editRoom={this.EditItemRoom} RequestDeleteRoom={this.RequestDeleteRoom} />
                {this.state.edit ? <EditRoom room={this.state.room_Item} ListEstablishment={this.props.establishments} cancelModal={this.handleCancelModal} handleSubmit={this.handleSubmitEdit} /> : ""}
                {this.state.modal_delete ? <DeleteRoom item_id={this.state.item_id} annuleModalDelete={this.handleAnnuleModalDelete} handleDelete={this.deleteItemRoom} /> : ''}
            </div>
        )
    }
}
//export default Rooms;
export default connect(mapStateToProps, { getEstablishment })(Rooms);
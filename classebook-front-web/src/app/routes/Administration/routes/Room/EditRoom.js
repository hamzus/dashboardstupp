import React from 'react';
import { Modal, ModalBody, ModalHeader } from "reactstrap";
import IntlMessages from 'util/IntlMessages';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Auxiliary from "util/Auxiliary";
import MenuItem from '@material-ui/core/MenuItem';
class EditRoom extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            previewVisible: true,
            name: '',
            description: '',
            id_establishment: 0,
            status: '',
            id: 0
        };
        this.handleAnnule = this.handleAnnule.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    componentDidMount() {
        this.setState({
            name: this.props.room.name,
            description: this.props.room.description,
            id_establishment: this.props.room.id_establishment,
            status: this.props.room.status,
            id: this.props.room.id,
        })
    }

    handleAnnule() {
        this.props.cancelModal();
    }
    handleChange = name => event => {
        this.setState({ [name]: event.target.value });
    };
    handleToggle() {
        this.props.cancelModal();
    }
    handleSubmit = (e) => {
        e.preventDefault();
        const data = {
            name: this.state.name,
            description: this.state.description,
            id_establishment: this.state.id_establishment,
            status: this.state.status,
            id: this.state.id,
        }
        this.props.handleSubmit(data);
        this.setState({ previewVisible: false });
    }
    render() {
        return (
            <Auxiliary>
                <Modal isOpen={this.state.previewVisible} toggle={this.handleToggle.bind(this)}>
                    <ModalHeader className="modal-box-header bg-primary text-white" >
                        {<IntlMessages id="modal.modif.room" />}
                    </ModalHeader>
                    <ModalBody>
                        <form className="row" onSubmit={this.handleSubmit}>
                            <div className="col-sm-6">
                                <TextField
                                    required
                                    id="establishment"
                                    select
                                    label={<IntlMessages id="components.student.formadd.establishment" />}
                                    value={this.state.id_establishment}
                                    onChange={this.handleChange('id_establishment')}
                                    SelectProps={{}}
                                    margin="normal"
                                    fullWidth >
                                    {this.props.ListEstablishment.map(establishment => (
                                        <MenuItem key={establishment.id} value={establishment.id}>
                                            {establishment.name}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    required
                                    name='name'
                                    id="name"
                                    label={<IntlMessages id="room.name" />}
                                    onChange={this.handleChange('name')}
                                    value={this.state.name}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>
                            <div className="col-sm-12">
                                <TextField
                                    name='description'
                                    id="description"
                                    label={<IntlMessages id="room.description" />}
                                    onChange={this.handleChange('description')}
                                    value={this.state.description}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>


                            <br /><br /><br /><br /><br />
                            <div className="col-sm-12">
                                <h4><font color="red">*</font> {<IntlMessages id="component.required_fields" />}</h4>
                            </div>
                            <div className="col-md-12 text-right ">
                                <br /><br />
                                <Button variant="contained" className="jr-btn bg-indigo text-white " type="submit" >{<IntlMessages id="components.establishments.formModify.buttonModify" />}</Button>
                                <Button variant="contained" className="jr-btn bg-grey text-white " onClick={this.handleAnnule}>{<IntlMessages id="components.establishments.formadd.buttonCancel" />}</Button>
                            </div>
                        </form>
                    </ModalBody>
                </Modal>
            </Auxiliary>
        )
    }
}
export default EditRoom;
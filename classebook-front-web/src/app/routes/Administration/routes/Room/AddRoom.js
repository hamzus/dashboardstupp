import React from 'react';
import IntlMessages from 'util/IntlMessages';
import TextField from '@material-ui/core/TextField';
import CardBox from 'components/CardBox/index';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import Auxiliary from "util/Auxiliary";
import { Modal, ModalBody, ModalHeader } from "reactstrap";

class AddRoom extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            previewVisible: true,
            ListEstablishment: [],
            name: '',
            description: '',
            id_establishment: 0
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleCancel() {
        this.setState({
            previewVisible: false,
            name: '',
            description: '',
            id_establishment: 0
        });
        this.props.cancelModal();
    };

    handleChange = name => event => {
        this.setState({ [name]: event.target.value });
    };
    handleSubmit = (e) => {
        e.preventDefault();
        const name = this.state.name;
        const description = this.state.description;
        const id_establishment = this.state.id_establishment;
        const data = { name, description, id_establishment }
        this.props.handleSubmit(data);
        this.setState({
            previewVisible: false,
            name: '',
            description: '',
            id_establishment: '',
        });
        this.props.cancelModal();
    }
    render() {
        return (
            <div className="app-wrapper">
                <Auxiliary>

                    <Modal isOpen={this.state.previewVisible} >
                        <ModalHeader toggle={this.handleCancel} className="modal-box-header bg-primary text-white">{<IntlMessages id="sidebar.rooms" />}</ModalHeader>
                        <ModalBody>
                            <form className="row" autoComplete="off" onSubmit={this.handleSubmit}>
                                <div className="col-sm-6">
                                    <TextField
                                        required
                                        id="establishment"
                                        select
                                        label={<IntlMessages id="components.student.formadd.establishment" />}
                                        value={this.state.id_establishment}
                                        onChange={this.handleChange('id_establishment')}
                                        SelectProps={{}}
                                        margin="normal"
                                        fullWidth >
                                        {this.props.ListEstablishment.map(establishment => (
                                            <MenuItem key={establishment.id} value={establishment.id}>
                                                {establishment.name}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                                <div className="col-sm-6">
                                    <TextField
                                        required
                                        name='name'
                                        id="name"
                                        label={<IntlMessages id="room.name" />}
                                        onChange={this.handleChange('name')}
                                        value={this.state.name}
                                        margin="normal"
                                        fullWidth
                                    />
                                </div>
                                <div className="col-sm-12">
                                    <TextField
                                        name='description'
                                        id="description"
                                        label={<IntlMessages id="room.description" />}
                                        onChange={this.handleChange('description')}
                                        value={this.state.description}
                                        margin="normal"
                                        fullWidth
                                    />
                                </div>
                                <br /><br /><br /><br /><br />
                                <div className="col-sm-12">
                                    <h4><font color="red">*</font> {<IntlMessages id="component.required_fields" />}</h4>
                                </div>
                                <div className="col-md-12 text-right ">
                                    <br /><br />
                                    <Button variant="contained" className="jr-btn bg-indigo text-white " type="submit" >{<IntlMessages id="components.establishments.formadd.buttonAdd" />}</Button>
                                    <Button variant="contained" className="jr-btn bg-grey text-white " onClick={this.handleCancel}>{<IntlMessages id="components.establishments.formadd.buttonCancel" />}</Button>
                                </div>
                            </form>

                        </ModalBody>
                    </Modal>
                </Auxiliary>

            </div>
        )
    }
}
export default AddRoom;
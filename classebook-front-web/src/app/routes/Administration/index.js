import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import asyncComponent from '../../../util/asyncComponent';

const roleIdSuperAdmin = 1;
const roleIdAdmin = 2;
const roleIdProfessor = 3;
const roleIdParent = 4;
const roleIdStudent = 5;

const Administration = ({ match }) => (

  <div className="app-wrapper">
    {(() => {
      const roleId = parseInt(localStorage.roles_id);
      if (roleId === roleIdSuperAdmin || roleId === roleIdAdmin) {
        return (
          <Switch>
            <Redirect exact from={`${match.url}/`} to={`${match.url}/establishment`} />
            <Route path={`${match.url}/professor`} component={asyncComponent(() => import('./routes/Professor/Professors'))} />
            <Route path={`${match.url}/establishment`} component={asyncComponent(() => import('./routes/Establishment/Establishments'))} />
            <Route path={`${match.url}/student`} component={asyncComponent(() => import('./routes/Student/Students'))} />
            <Route path={`${match.url}/classe`} component={asyncComponent(() => import('./routes/Class/Classes'))} />
            <Route path={`${match.url}/rooms`} component={asyncComponent(() => import('./routes/Room/Rooms'))} />
            <Route path={`${match.url}/subjects`} component={asyncComponent(() => import('./routes/Subject/Subjects'))} />
            <Route path={`${match.url}/level`} component={asyncComponent(() => import('./routes/ClassLevel/ClassLevel'))} />
          </Switch>
        )
      }
    })()}

  </div>
);
export default Administration;

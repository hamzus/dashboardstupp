import React from "react";
import Widget from "components/Widget";
import axios from 'axios';
import baseUrl from 'config/config';
import IntlMessages from 'util/IntlMessages';


class Contact extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      professorChercherPhone: '',
      professorChercherAddressMAil: '',
      personConnectedMail: '',
      personConnectedPhone: ''
    };
  };
  componentWillMount() {
    switch (localStorage.roles_id) {
      case '1':
        this.setState({ personConnectedMail: 'superAdmin@gmail.com' });
        this.setState({ personConnectedPhone: 76100100 });
        break;

      case '2':
        this.setState({ personConnectedMail: 'Admin@gmail.com' });
        this.setState({ personConnectedPhone: 76101101 });
        break;

      case '3':
        {
          axios.get(`${baseUrl.baseUrl}/profiles?filter[where][user_id]= ` + localStorage.user_id)
            .then(res => {
              axios.get(`${baseUrl.baseUrl}/professors?filter[where][profile_id]= ` + res.data[0].id)
                .then(res => {
                  const professorListChercher = res.data;
                  const personConnectedMail = professorListChercher[0].address_mail;
                  const personConnectedPhone = professorListChercher[0].phone;
                  this.setState({ personConnectedMail });
                  this.setState({ personConnectedPhone });
                })
            })
        };
        break;

      case '4':
        {
          axios.get(`${baseUrl.baseUrl}/profiles?filter[where][user_id]= ` + localStorage.user_id)
            .then(res => {
              axios.get(`${baseUrl.baseUrl}/parents?filter[where][profile_id]= ` + res.data[0].id)
                .then(res => {
                  const parentListChercher = res.data;
                  const personConnectedMail = parentListChercher[0].address_mail;
                  const personConnectedPhone = parentListChercher[0].phone;
                  this.setState({ personConnectedMail });
                  this.setState({ personConnectedPhone });
                })
            })
        };
        break;

      case '5':
        {
          axios.get(`${baseUrl.baseUrl}/profiles?filter[where][user_id]= ` + localStorage.user_id)
            .then(res => {
              axios.get(`${baseUrl.baseUrl}/students?filter[where][profile_id]= ` + res.data[0].id)
                .then(res => {
                  const studentListChercher = res.data;
                  const personConnectedMail = studentListChercher[0].address_mail;
                  const personConnectedPhone = studentListChercher[0].phone;
                  this.setState({ personConnectedMail });
                  this.setState({ personConnectedPhone });
                })
            })
        };
        break;
      default:
        this.setState({ personConnectedMail: 'mail@gmail.com' });
        this.setState({ personConnectedPhone: 99999999 });
        break;
    }
  }
  render() {
    return (
      <Widget title="Contact" styleName="jr-card-profile-sm">
        <div className="media align-items-center flex-nowrap jr-pro-contact-list">
          <div className="mr-3">
            <i className={`zmdi zmdi-${'email'} jr-fs-xxl text-grey`} />
          </div>
          <div className="media-body">
            <span className="mb-0 text-grey jr-fs-sm">{<IntlMessages id="components.profile.email" />}</span>
            <p className="jr-link">{this.state.personConnectedMail}</p>
          </div>
        </div>
        <div className="media align-items-center flex-nowrap jr-pro-contact-list">
          <div className="mr-3">
            <i className={`zmdi zmdi-${'phone'} jr-fs-xxl text-grey`} />
          </div>
          <div className="media-body">
            <span className="mb-0 text-grey jr-fs-sm">{<IntlMessages id="components.profile.phone" />}</span>
            <p className="mb-0">{this.state.personConnectedPhone}</p>
          </div>
        </div>

      </Widget>
    )
  }
}
export default Contact;






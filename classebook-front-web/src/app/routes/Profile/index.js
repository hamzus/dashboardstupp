import React, { Component } from "react";
import About from "./About/index";
import Contact from "./Contact/index";
import ProfileHeader from "./ProfileHeader/index";
import Auxiliary from "../../../util/Auxiliary";

class Profile extends Component {

  render() {
    return (
      <Auxiliary>
        <ProfileHeader />
        <div className="jr-profile-content">
          <div className="row">
            <div className="col-xl-12 col-lg-8 col-md-7 col-12">
              <About />
            </div>
            <div className="col-xl-12 col-lg-4 col-md-5 col-12">
              <Contact />
            </div>
          </div>
        </div>
      </Auxiliary>
    );
  }
}

export default Profile;



import React from "react";
import Avatar from '@material-ui/core/Avatar';
import axios from 'axios';
import baseUrl from 'config/config';


class ProfileHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      roleList: []
    };
    this.getRoleName = this.getRoleName.bind(this);

  };

  componentWillMount() {
    axios.get(`${baseUrl.baseUrl}/roles`)
      .then(res => {
        const roleList = res.data;
        this.setState({ roleList });
      }).catch(function (error) {
        alert('error')
      });
  }

  getRoleName() {
    const getRoleNameReturn = this.state.roleList.map(role => {

      if (role.id == localStorage.roles_id) {
        return role.name;
      }
    })

    return getRoleNameReturn;
  };
  render() {
    const user = JSON.parse(localStorage.getItem('user'));
    return (
      <div className="jr-profile-banner">
        <div className="jr-profile-container">
          <div className="jr-profile-banner-top">
            <div className="jr-profile-banner-top-left">
              <div className="jr-profile-banner-avatar">
                <Avatar className="size-90" alt="..." src={'https://via.placeholder.com/150x150'} />
              </div>
              <div className="jr-profile-banner-avatar-info">
                <h2 className="mb-2 jr-mb-sm-3 jr-fs-xxl jr-font-weight-light">{user.realm.toUpperCase()} {(user.username.charAt(0).toUpperCase() + user.username.substring(1).toLowerCase())}</h2>
                <p className="mb-0 jr-fs-lg">{this.getRoleName()}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default ProfileHeader;


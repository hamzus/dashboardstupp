import React from "react";
import Widget from "components/Widget/index";
import AboutItem from "./AboutItem";
import axios from 'axios';
import baseUrl from 'config/config';


var dateFormat = require('dateformat');
class About extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      entity: '',
      studentClass: '',
      studentEstablishment: '',
      loaded: false,
      titleEstablishment: '',
      iconEstablishment: '',
      titleClasse: '',
      iconClasse: '',
    };
    this.getStepContent = this.getStepContent.bind(this)
    this.getdata = this.getdata.bind(this)
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };
  componentDidMount() {
    this.getdata();
  }
  getdata() {
    axios.get(`${baseUrl.baseUrl}/profiles?filter[where][user_id]= ` + localStorage.user_id)
      .then(res => {
        const role = res.data[0].role_id
        let url = this.getStepContent(role)
        axios.get(`${baseUrl.baseUrl}/` + url + `?filter[where][profile_id]= ` + res.data[0].id)
          .then(res => {
            const entity = res.data[0];
            if (url == 'students') {
              axios.get(`${baseUrl.baseUrl}/students/` + res.data[0].id + `/class `)
                .then(res => {
                  const studentClass = res.data;
                  axios.get(`${baseUrl.baseUrl}/establishments/ ` + localStorage.establishment_id)
                    .then(res => {
                      const studentEstablishment = res.data;
                      this.setState({
                        entity: entity,
                        titleEstablishment: 'Établissement',
                        iconEstablishment: 'city-alt',
                        studentEstablishment: studentEstablishment,
                        titleClasse: 'Classe',
                        iconClasse: 'graduation-cap',
                        studentClass: studentClass
                      })
                    })
                })
            } else {
              this.setState({
                entity: entity,
              })
            }
          }
          );
      })
  }
  getStepContent(id) {
    let roleUser = '';
    switch (id) {
      case 1:
        roleUser = 'superAdmin';
        break;

      case 2:
        roleUser = 'admin';
        break;

      case 3:
        roleUser = 'professors';
        break;

      case 4:
        roleUser = 'parents';
        break;

      case 5:
        roleUser = 'students';
        break;

      default:
        roleUser = 'Unknown step';
        break;
    }

    return roleUser;
  }
  render() {
    const aboutList = [
      {
        id: 1,
        title: 'Utiliseur',
        icon: 'face',
        desc: this.state.entity.name + ' ' + this.state.entity.surname
      },
      {
        id: 2,
        title: 'Date de naissance',
        icon: 'cake',
        desc: dateFormat(this.state.entity.date_of_birth, "fullDate")
      },
      {
        id: 3,
        title: 'CIN',
        icon: 'dns',
        desc: this.state.entity.cin
      },
      {
        id: 4,
        title: this.state.titleClasse,
        icon: this.state.iconClasse,
        desc: this.state.studentClass.name,
      },
      {
        id: 5,
        title: this.state.titleEstablishment,
        icon: this.state.iconEstablishment,
        desc: this.state.studentEstablishment.name
      },
    ]
    return (
      <Widget styleName="jr-card-full jr-card-tabs-right jr-card-profile">
        <div className="card-header">
          <h4 className="card-title mb-0">About</h4>
        </div>
        <div className="jr-tabs-content jr-task-list">
          <div className="row">
            {aboutList.map((about, index) => <div
              className="col-xl-4 col-lg-6 col-md-6 col-sm-6 col-12"><AboutItem data={about} /></div>)}
          </div>
        </div>
      </Widget>
    );
  }
}


export default About;

import React from 'react';
import IntlMessages from 'util/IntlMessages';

class Cafeteria extends React.Component {

  render() {
    return (
      <div className="app-wrapper">
      <br/>
        <div className="d-flex justify-content-center">
          <h1><IntlMessages id="sidebar.components.cafeteria"/></h1>
        </div>

      </div>
    );
  }
}

export default Cafeteria;
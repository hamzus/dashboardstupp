import _ from 'lodash';
import React, { Component } from 'react';
import 'react-confirm-alert/src/react-confirm-alert.css';
import StudentItem from './StudentItem';
import baseUrl from 'config/config';
import axios from 'axios';

class StudentList extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    componentDidMount() {

    }

    render() {

        let studentsByClass = [];
        studentsByClass = this.props.studentsByClass;

        let list_presence = [];
        list_presence = this.props.list_presence;

        let list_presenceIds = list_presence.map(item => item.student_id);

        let firstFilterStudent = studentsByClass.map((data) => {
            let absentStudentId = list_presenceIds.find(item => item == data.id)
            if (_.isFinite(absentStudentId)) { return data }
        }
        )
        let absentStudents = firstFilterStudent.filter(element => element)
        return (
            <div>
                {(() => {
                    if (_.isFinite(absentStudents.length)) {
                        return (
                            <div className="container" >

                                <div className="row" autoComplete="off">
                                    <div className=" col-12  animated slideInUpTiny animation-duration-3">
                                        {absentStudents.map((data, index) => (
                                            <StudentItem
                                                key={index}
                                                data={data}
                                                profileSupervisor={this.state.profile_idSupervisor}
                                            />
                                        ))}
                                    </div>
                                </div>
                            </div>
                        )
                    } else if (_.isFinite(this.props.classe)) {
                        return (
                            <h2>Pas de liste d'élèves absents</h2>
                        )
                    } else {
                        return (
                            <h2>La liste est affiché aprés avoir choisie une classe</h2>
                        )
                    }
                })()}
            </div>)
    }
}

export default StudentList; 
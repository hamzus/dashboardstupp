import React from 'react';
import IntlMessages from 'util/IntlMessages';
import TextField from '@material-ui/core/TextField';
import CardBox from 'components/CardBox/index';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import StudentList from './StudentList'
import moment from 'moment';
import { connect } from "react-redux";
import { getClasses } from "../../../actions/classeAction";
import { getStudents } from "../../../actions/studentAction";
import { getPresence } from "../../../actions/presenceAction";
// import { getCallRegister } from "../../../actions/RegistreAction";
import { getEstablishment } from "../../../actions/establishmentAction";
import _ from 'lodash'
import memoize from 'memoize-one'
// import Async from 'react-select/async';

function mapStateToProps(state) {
  return {
    classes: state.classes,
    students: state.student.remoteStudents,
    presences: state.presence.remotePresences,
    establishments: state.establishment.remoteEstablishments
  };
};

class BilletPass extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      classe: '',
      list_presence: [],
      establishment_id: 0,
    };
    this.handleChange = this.handleChange.bind(this);
    this.filterMemoize = this.filterMemoize.bind(this);
  }

  filterMemoize = memoize(
    (students, classe) => students.filter(student => student.class_id == classe)
  );

  componentDidMount() {
    this.props.getClasses();
    this.props.getStudents();
    this.props.getPresence();
    this.props.getEstablishment();

  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    })
  }

  render() {
    const allClasses = Array.from(this.props.classes);
    const allEstablishment = Array.from(this.props.establishments);
    const userEstablishment_id = localStorage.establishment_id;

    const establishmentClasses = allClasses.filter(item => item.establishment_id == userEstablishment_id || item.establishment_id == this.state.establishment_id)

    const studentsByClass = this.filterMemoize(this.props.students, this.state.classe);
    return (
      <div className="app-wrapper">
        <CardBox styleName="col-sm-6"
          heading={<IntlMessages id="pages.billetPass.form.class" />}>
          <div className="row">
            <div className="col-sm" align="right">
              <TextField
                required
                id="establishment"
                select
                label={<IntlMessages id="components.student.formadd.establishment" />}
                value={this.state.establishment_id}
                onChange={this.handleChange('establishment_id')}
                SelectProps={{}}
                margin="normal"
                fullWidth >
                {allEstablishment.map(establishment => (
                  <MenuItem key={establishment.id} value={establishment.id}>
                    {establishment.name}
                  </MenuItem>
                ))}
              </TextField>
            </div>
            <div className="col-sm" align="right">
              <TextField
                required
                id="classe"
                select
                label={<IntlMessages id="components.note.class" />}
                value={this.state.classe}
                onChange={this.handleChange('classe')}
                onBlur={this.handleBlur}
                SelectProps={{}}
                margin="normal"
                fullWidth >
                {establishmentClasses.map(option => (
                  <MenuItem key={option.id} value={option.id}>
                    {option.name}
                  </MenuItem>
                ))}
              </TextField>
            </div>
          </div>
        </CardBox>
        <div className="row">
          <StudentList
            studentsByClass={studentsByClass}
            list_presence={this.props.presences}
            classe={this.state.classe} />
        </div>
      </div>
    )
  }
}


export default connect(mapStateToProps, { getEstablishment, getClasses, getStudents, getPresence })(BilletPass);

import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Badge } from 'reactstrap';
import baseUrl from 'config/config';
import axios from 'axios';


class FormDialog extends React.Component {
  state = {
    open: false,
    reason: "Absence",
    emailParent: ''
  };

  componentDidMount() {

  }

  handleSubmit = () => {
    axios.post(`${baseUrl.baseUrl}/absent_pass`, {
      reason: this.state.reason,
      status: true,
      student_id: this.props.student_id
    })
      .then(resBillet => {
        axios.get(`${baseUrl.baseUrl}/parents/findOne?filter={"where":{"id":` + this.props.parent_id + `}}`)
          .then(res => {
            console.log(this.props.profileSupervisor)
            //Envoyer mail au parent
            axios.post(`${baseUrl.baseUrl}/mails`, {
              sender_id: this.props.profileSupervisor,
              receiver_id: res.data.profile_id,
              subject: "Votre enfant a recu un billet",
              message: resBillet.data.reason,
              profile_id: res.data.profile_id,
            });
            //Envoyer mail au student
            axios.post(`${baseUrl.baseUrl}/mails`, {
              sender_id: this.props.profileSupervisor,
              receiver_id: this.props.profile_id,
              subject: "Votre avez recu un billet",
              message: resBillet.data.reason,
              profile_id: this.props.profile_id,
            })
          }).then(res => alert("Un Mail a été envoyer au Parent et a L'èleve avec succés"))
      })
    this.handleRequestClose();

  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    })
  }

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleRequestClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { address_mail, student_id, parent_id, profile_id } = this.props;
    return (
      <div>
        <Button
          variant="contained" className="bg-primary text-white"
          onClick={this.handleClickOpen}
        >
          Donner Billet
        </Button>
        <Dialog open={this.state.open} onClose={this.handleRequestClose}>
          <DialogTitle>Billet Electronique</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Ecrivez le motif de ce billet
            </DialogContentText>
            <TextField
              id="reason"
              onChange={this.handleChange('reason')}
              autoFocus
              margin="dense"
              label="Motif"
              type="text"
              fullWidth
            />
            <TextField
              autoFocus
              margin="dense"
              id="email"
              onChange={this.handleChange('email')}
              label="Email Address"
              type="email"
              fullWidth
              value={address_mail}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleRequestClose} color="secondary">
              Annuler
            </Button>
            <Button onClick={this.handleSubmit} color="primary">
              Donner Billet
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default FormDialog;
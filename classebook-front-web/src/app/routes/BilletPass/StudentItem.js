import React, { Component } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import FormDialog from './FormDialog';
import IntlMessages from '../../../util/IntlMessages';
import { Card, CardBody, CardText } from 'reactstrap';
import baseUrl from 'config/config';
import axios from 'axios';

function jsUpperCaseFirst(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

class StudentItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profileSupervisor: ''
    }
  }

  componentDidMount() {
    axios.get(`${baseUrl.baseUrl}/users/${localStorage.user_id}?access_token=${localStorage.token}`)
      .then(res => {
        console.log(res)
        let idUser = parseInt(res.data.id);
        axios.get(`${baseUrl.baseUrl}/profiles?filter[where][user_id]=` + idUser)
          .then(response => {
            console.log(response)
            this.setState({ profileSupervisor: response.data[0].id })
          })
      })
  }

  render() {
    const { data, handleClickAbsent, handleClickPresent } = this.props;
    const { name, surname, address_mail, id, profile_id, parent_id } = data;
    return (
      <Card className="col-lg-12" >

        <Avatar
          alt='...'
          src={require('../../../assets/images/placeholder.jpg')}
          className="user-avatar avatar-shadow"
        />

        <CardBody className="col-12">

          <CardText>
            {name.toUpperCase()} {jsUpperCaseFirst(surname)}
          </CardText>
          <FormDialog
            name={name}
            surname={surname}
            address_mail={address_mail}
            student_id={id}
            profile_id={profile_id}
            parent_id={parent_id}
            profileSupervisor={this.state.profileSupervisor}
          />
        </CardBody>
      </Card>


    );
  }
}


export default StudentItem;
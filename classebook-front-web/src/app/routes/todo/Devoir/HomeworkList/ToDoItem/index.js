import React from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton'
import { SortableElement, SortableHandle } from 'react-sortable-hoc';
import Button from '@material-ui/core/Button';
import labels from 'app/routes/todo/data/labels';
import users from 'app/routes/todo/data/users';
import IntlMessages from 'util/IntlMessages';
import AssignHomework from '../../AssignHomework';

class ToDoItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      assignModel: false,
      idHomework: 0
    };
    this.addHomeworkClass = this.addHomeworkClass.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  };

  handleCancel() {
    this.setState({ assignModel: false })
  };

  addHomeworkClass(e) {
    e.preventDefault();
    this.setState({ assignModel: true, idHomework: e.currentTarget.value });
  };

  render() {
    const DragHandle = SortableHandle(() =>
      <i className="zmdi zmdi-menu draggable-icon d-none d-sm-flex" style={{ fontSize: 25 }} />);
    const todo = this.props.todo;
    const onTodoSelect = this.props.onTodoSelect;
    const onTodoChecked = this.props.onTodoChecked;
    const onMarkAsStart = this.props.onMarkAsStart;
    const roleIdProfessor = 3;
    const roleId = parseInt(localStorage.roles_id);
    return (
      <div className="module-list-item">
        <div className="module-list-icon">
          <span className="bar-icon">
            <DragHandle />
          </span>
        </div>

        <div className="module-list-info" onClick={() => {
          onTodoSelect(todo);
        }}>
          <div className="row">
            <div className="module-todo-content col-9 col-sm-10 col-md-11">
              <div className={`subject ${todo.completed && 'text-muted text-strikethrough'}`}>
                {(() => {
                  const roleIdParent = 4;
                  const roleIdStudent = 5;
                  const roleId = parseInt(localStorage.roles_id);
                  if (roleId === roleIdParent || roleId === roleIdStudent) {
                    return (
                      <div>
                        {todo.subjectName} : &nbsp;{todo.title}
                      </div>
                    )
                  } else {
                    return (
                      <div>
                        {todo.title}
                      </div>
                    )
                  }
                })()}


              </div>
              <div className="manage-margin">
              </div>
            </div>
          </div>
        </div>
        {(() => {
          if (roleId === roleIdProfessor) {
            return (
              <div className="text-right">
                <Button className="jr-btn bg-indigo text-white" onClick={this.addHomeworkClass} value={todo.id} >{<IntlMessages id="button.Assign_to_class" />}</Button>
              </div>
            )
          }
        })()}

        {this.state.assignModel ? <AssignHomework idHomework={this.state.idHomework} cancelModal={this.handleCancel} /> : ''}
      </div>
    )
  }
}
export default ToDoItem;
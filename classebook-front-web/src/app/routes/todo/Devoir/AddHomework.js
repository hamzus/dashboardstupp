
import React from 'react';
import { Modal, ModalBody, ModalHeader } from "reactstrap";
import IntlMessages from 'util/IntlMessages';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Auxiliary from "util/Auxiliary";
import MenuItem from '@material-ui/core/MenuItem';
import { connect } from "react-redux";
import { addHomework } from 'actions/ToDo';
import axios from 'axios';
import baseUrl from 'config/config';


const titles = [
    {
        value: 'Exercice de livre',
        label: 'Exercice de livre',
    },
    {
        value: 'Serie d\'exercice',
        label: 'Serie d\'exercice',
    },
    {
        value: 'Autre',
        label: 'Autre',
    }

];

class AddHomework extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            previewVisible: true,
            title: '',
            subject_id: 0,
            description: '',
            professor_id: 2,
            RoomsList: [],
            professorsList: []
        };
        this.handleCancel = this.handleCancel.bind(this);
        this.handleChange = this.handleChange.bind(this);
    };
    componentWillMount() {

        axios.get(`${baseUrl.baseUrl}/subjects`)
            .then(res => {
                const RoomList = res.data;
                const RoomsList = RoomList.filter(element => element.status);
                this.setState({ RoomsList });
            });

        axios.get(`${baseUrl.baseUrl}/professors/findOne?filter={"where":{"profile_id":` + localStorage.profileId + `}}`)
            .then(res => {
                this.setState({ professor_id: res.data.id });
            });
    }

    handleCancel() {
        this.props.cancelModal();
    };

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    handleSubmit = (e) => {
        e.preventDefault();
        let date = new Date();
        const title = this.state.title;
        const description = this.state.description;
        const date_creation = date;
        const professor_id = this.state.professor_id;
        const subject_id = this.state.subject_id;
        const data = { title, description, date_creation, professor_id, subject_id, date_creation };

        this.props.dispatch(addHomework(data));
        this.setState({
            title: '',
            subject_id: 0,
            description: '',
            professor_id: 0
        });
        this.props.cancelModal();
    };

    render() {
        const list = this.state.RoomsList;
        return (
            <Auxiliary>
                <Modal isOpen={this.state.previewVisible}>
                    <ModalHeader className="modal-box-header bg-primary text-white" >
                        {<IntlMessages id="modal.addToDo" />}
                    </ModalHeader>
                    <ModalBody>
                        <form className="row" onSubmit={this.handleSubmit}>
                            <div className="col-sm-6">
                                <TextField
                                    required
                                    id="subject_id"
                                    select
                                    label={<IntlMessages id="sidebar.subjects" />}
                                    value={this.state.subject_id}
                                    onChange={this.handleChange('subject_id')}
                                    SelectProps={{}}
                                    margin="normal"
                                    fullWidth >
                                    {list.map(suject => (
                                        <MenuItem key={suject.id} value={suject.id}>
                                            {suject.name}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div>
                            <div className="col-sm-6">
                                <TextField
                                    required
                                    id="title"
                                    select
                                    label={<IntlMessages id="toDo.titre" />}
                                    value={this.state.title}
                                    onChange={this.handleChange('title')}
                                    SelectProps={{}}
                                    margin="normal"
                                    fullWidth >
                                    {titles.map(option => (
                                        <MenuItem key={option.value} value={option.value}>
                                            {option.label}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div>
                            <div className="col-sm-12">
                                <TextField
                                    name='description'
                                    id="description"
                                    label={<IntlMessages id="room.description" />}
                                    onChange={this.handleChange('description')}
                                    value={this.state.description}
                                    margin="normal"
                                    fullWidth
                                />
                            </div>


                            <br /><br /><br /><br /><br />
                            <div className="col-sm-12">
                                <h4><font color="red">*</font> {<IntlMessages id="component.required_fields" />}</h4>
                            </div>
                            <div className="col-md-12 text-right ">
                                <br /><br />
                                <Button variant="contained" className="jr-btn bg-indigo text-white " type="submit" >{<IntlMessages id="components.establishments.formadd.buttonAdd" />}</Button>
                                <Button variant="contained" className="jr-btn bg-grey text-white " onClick={this.handleCancel}>{<IntlMessages id="components.establishments.formadd.buttonCancel" />}</Button>
                            </div>
                        </form>
                    </ModalBody>
                </Modal>
            </Auxiliary>
        )
    };
}

export default connect()(AddHomework);
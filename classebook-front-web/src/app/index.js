import React from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Header from 'components/Header/index';
import Sidebar from 'containers/SideNav/index';
import Footer from 'components/Footer';
import Tour from '../components/Tour/index';
import Administration from './routes/Administration/index';
import RegistreAppel from './routes/RegistreAppel/index';
import Calendar from './routes/Calendar/index';
import Mail from './routes/Mail/Mail';
import Devoir from './routes/todo/Devoir';
import Home from './routes/Home/index';
import Profile from './routes/Profile/index'
import GradesMenu from './routes/GradesMenu/index';
import BilletPass from './routes/BilletPass/BilletPass';
import {
  ABOVE_THE_HEADER,
  BELOW_THE_HEADER,
  COLLAPSED_DRAWER,
  FIXED_DRAWER,
  HORIZONTAL_NAVIGATION,
} from 'constants/ActionTypes';
import { isIOS, isMobile } from 'react-device-detect';
import asyncComponent from '../util/asyncComponent';
import TopNav from 'components/TopNav';
import ClassLevel from './routes/Administration/routes/ClassLevel/ClassLevel';
import Complaint from './routes/Complaint/index';
import Cafeteria from './routes/Cafeteria/index';
import HealthMonitoring from './routes/HealthMonitoring/index';
import Learning from './routes/Learning/index';
import Payment from './routes/Payment/Payments';

class App extends React.Component {

  render() {
    const roleIdSuperAdmin = 1;
    const roleIdAdmin = 2;
    const roleIdProfessor = 3;
    const roleIdParent = 4;
    const roleIdStudent = 5;
    const roleIdSupervisor = 6;
    const roleId = parseInt(localStorage.roles_id);
    const { match, drawerType, navigationStyle, horizontalNavPosition } = this.props;
   const drawerStyle = drawerType.includes(FIXED_DRAWER) ? 'fixed-drawer' : drawerType.includes(COLLAPSED_DRAWER) ? 'collapsible-drawer' : 'mini-drawer';

    //set default height and overflow for iOS mobile Safari 10+ support.
    if (isIOS && isMobile) {
      document.body.classList.add('ios-mobile-view-height')
    }
    else if (document.body.classList.contains('ios-mobile-view-height')) {
      document.body.classList.remove('ios-mobile-view-height')
    }

    return (
      <div className={`app-container ${drawerStyle}`}>
        <Tour />

        <Sidebar />
        <div className="app-main-container">
          <div
            className={`app-header ${navigationStyle === HORIZONTAL_NAVIGATION ? 'app-header-horizontal' : ''}`}>
            {(navigationStyle === HORIZONTAL_NAVIGATION && horizontalNavPosition === ABOVE_THE_HEADER) &&
              <TopNav styleName="app-top-header" />}
            <Header />
            {(navigationStyle === HORIZONTAL_NAVIGATION && horizontalNavPosition === BELOW_THE_HEADER) &&
              <TopNav />}
          </div>

          <main className="app-main-content-wrapper">
            <div className="app-main-content">

              <Switch>
                {(() => {
                  if (roleId === roleIdSuperAdmin || roleId === roleIdAdmin) {
                    return (
                      <Route path={`${match.url}/administration`} component={Administration} />
                    )
                  }
                })()}

                {(() => {
                  if (roleId === roleIdSuperAdmin || roleId === roleIdAdmin || roleId === roleIdSupervisor) {
                    return (
                      <Route path={`${match.url}/billetPass`} component={BilletPass} />
                    )
                  }
                })()}

                {(() => {
                  if (roleId === roleIdSuperAdmin || roleId === roleIdAdmin || roleId === roleIdProfessor) {
                    return (
                      <Route path={`${match.url}/call_register`} component={RegistreAppel} />
                    )
                  }
                })()}
                            
                <Route path={`${match.url}/mail`} component={Mail} />
                <Route path={`${match.url}/calendar/`} component={Calendar} />
                <Route path={`${match.url}/gradesmenu`} component={GradesMenu} />
                <Route path={`${match.url}/devoir`} component={Devoir} />
                <Route path={`${match.url}/home`} component={Home} />
                <Route path={`${match.url}/level`} component={ClassLevel} />
                <Route path={`${match.url}/profile`} component={Profile} />
                <Route path={`${match.url}/complaints`} component={Complaint} />
                <Route path={`${match.url}/Cafeteria`} component={Cafeteria} />
                <Route path={`${match.url}/health-monitoring`} component={HealthMonitoring} />
                <Route path={`${match.url}/e-learning`} component={Learning} />
                <Route path={`${match.url}/payments`} component={Payment} />
                <Route component={asyncComponent(() => import('components/Error404'))} />
              </Switch>
            </div>
            <Footer />
          </main>
        </div>
      </div>
    );
  }
}


const mapStateToProps = ({ settings }) => {
  const { drawerType, navigationStyle, horizontalNavPosition } = settings;
  return { drawerType, navigationStyle, horizontalNavPosition }
};
export default withRouter(connect(mapStateToProps)(App));
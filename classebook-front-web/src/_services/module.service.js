import axios from 'axios';
import config from '../config/config';

export const moduleService = {
    post,
    get,
    put,
    archive
};

function post(apiEndpoint, payload) {
    return axios.post(config.baseUrl + apiEndpoint, payload).then((response) => {
        return response;
    }).catch((err) => {
        console.log(err);
    })
};

function get(apiEndpoint) {
    return axios.get(config.baseUrl + apiEndpoint).then((response) => {
        return response;
    }).catch((err) => {
        console.log(err);
    })
};

function put(apiEndpoint, data) {
    return axios.put(config.baseUrl + apiEndpoint, {
        name: data.name,
        description: data.description,
        status: true,
        id: data.id,
    })
        .then((response) => {
            return response;
        }).catch((err) => {
            console.log(err);
        })
};

function archive (apiEndpoint, data) {
    return axios.put(config.baseUrl + apiEndpoint, {
        name: data.name,
        description: data.description,
        status: false,
        id: data.id,
    })
        .then((response) => {
            return response;
        }).catch((err) => {
            console.log(err);
        })
};

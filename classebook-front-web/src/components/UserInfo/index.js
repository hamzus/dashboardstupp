import React from 'react';
import Avatar from '@material-ui/core/Avatar'
import { connect } from 'react-redux'
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { userSignOut } from 'actions/Auth';
import IntlMessages from 'util/IntlMessages';
import ColorOption from 'containers/Customizer/ColorOption';
import { NavLink } from 'react-router-dom';

class UserInfo extends React.Component {

  state = {
    anchorEl: null,
    open: false,
    roleName: '',
    colorOption: false,
  };

  activePanel = () => {
    this.setState({ colorOption: true });
    this.setState({ open: false });
  };

  handleClick = event => {
    this.setState({ open: true, anchorEl: event.currentTarget });
  };

  handleRequestClose = () => {
    this.setState({
      open: false,
      roleName: ''
    });
  };

  handleCancelPanel() {
    this.setState({ colorOption: false })
};

  componentWillMount() {
    switch (localStorage.roles_id) {
      case '1':
        this.setState({ roleName: 'Super Admin' });
        break;
      case '2':
        this.setState({ roleName: 'Admin' });
        break;
      case '3':
        this.setState({ roleName: 'Professeur' });
        break;
      case '4':
        this.setState({ roleName: 'Parent' });
        break;
      case '5':
        this.setState({ roleName: 'Élève' });
        break;
      case '6':
        this.setState({ roleName: 'Superviseur' });
        break;
      default:
        this.setState({ roleName: '' });
        break;
    }
  }
  componentWillUnmount() {
  }

  render() {
    const roleName = this.state.roleName;
    const user = JSON.parse(localStorage.getItem('user'));
    return (
      <div className="user-profile d-flex flex-row align-items-center">
        <Avatar
          alt='...'
          src={'https://via.placeholder.com/150x150'}
          className="user-avatar "
        />
        <div className="user-detail" onClick={this.handleClick}>
          <h4 className="user-name" >{user.realm} {user.username}
            <i className="zmdi zmdi-caret-down zmdi-hc-fw align-middle" />
          </h4>
          <h6 className="user-name">  {roleName}
          </h6>
        </div>
        <Menu className="user-info"
          id="simple-menu"
          anchorEl={this.state.anchorEl}
          open={this.state.open}
          onClose={this.handleRequestClose}
          PaperProps={{
            style: {
              minWidth: 120,
              paddingTop: 0,
              paddingBottom: 0
            }
          }}
        >
          <MenuItem onClick={this.handleRequestClose} >
            <NavLink Style="text-decoration: none;" to="/app/profile">
              <i className="zmdi zmdi-account zmdi-hc-fw mr-2" />
              <IntlMessages id="popup.profile" />
            </NavLink>
          </MenuItem>
          <MenuItem onClick={this.activePanel.bind(this)}>
            <i className="zmdi zmdi-settings zmdi-hc-fw mr-2" />
            <IntlMessages id="popup.setting" />
          </MenuItem>
          <MenuItem onClick={() => {
            this.setState({ roleName: '' });
            this.handleRequestClose();
            this.props.userSignOut();
          }}>
            <i className="zmdi zmdi-sign-in zmdi-hc-fw mr-2" />

            <IntlMessages id="popup.logout" />
          </MenuItem>
        </Menu>
        {this.state.colorOption ? <ColorOption cancelPanel = {this.handleCancelPanel.bind(this)}/> : ''}
      </div>
    );
  }
}

const mapStateToProps = ({ settings }) => {
  const { locale } = settings;
  return { locale }
};
export default connect(mapStateToProps, { userSignOut })(UserInfo);



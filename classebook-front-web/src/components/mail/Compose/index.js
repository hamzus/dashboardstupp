import React from 'react';
import { Modal, ModalHeader } from 'reactstrap';
import Moment from 'moment';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import baseUrl from 'config/config';
import axios from 'axios';
import MenuItem from '@material-ui/core/MenuItem';
import * as moment from 'moment';
const roleIdParent = 4;
const roleIdProfessor = 3;

class ComposeMail extends React.Component {
  constructor() {
    super();
    this.state = {
      to: '',
      cc: '',
      bcc: '',
      subject: '',
      message: '',
      professorInfoList: [],
      professorsList: [],
      receiver_id: '',
      dateHourMail: '2019-07-18T13:07:42.438Z'
    }
  }

  componentWillMount() {
    const roleId = parseInt(localStorage.roles_id);
    if (roleId === roleIdParent) {
      axios.get(`${baseUrl.baseUrl}/parents/findOne?filter={"where":{"profile_id":` + localStorage.profileId + `}}`)
        .then(res => {
          let parentId = res.data.id;
          axios.get(`${baseUrl.baseUrl}/students/findOne?filter={"where":{"parent_id":` + parentId + `}}`)
            .then(res => {
              let classId = res.data.class_id;
              axios.get(`${baseUrl.baseUrl}/classes/` + classId)
                .then(res => {
                  let professorsIds = res.data.professor_id;
                  let professorInfo = [];
                  professorsIds.map(professorId => {
                    axios.get(`${baseUrl.baseUrl}/professors/` + professorId)
                      .then(res => {
                        let professorName = res.data.name + ' ' + res.data.surname;
                        let mailAddress = res.data.address_mail;
                        let subjectId = res.data.subject_id;
                        axios.get(`${baseUrl.baseUrl}/subjects/` + subjectId)
                          .then(res => {
                            let obj = {
                              'professorName': professorName,
                              'mailAddress': mailAddress,
                              'subjectName': res.data.name
                            };
                            professorInfo.push(obj);
                            this.setState({ professorInfoList: professorInfo });
                          })
                      })
                  })
                })
            })
        })
    }

    if (roleId === roleIdProfessor) {
      axios.get(`${baseUrl.baseUrl}/profiles?filter={"where":{"and":[{"establishment_id":` + localStorage.establishment_id + `},{"role_id":` + roleIdProfessor + `}]}}`)
        .then(res => {
          let profilesIdsList = res.data
          let professorsdetails = [];
          for (var element of profilesIdsList) {
            let profileId = element.id
            axios.get(`${baseUrl.baseUrl}/professors/findOne?filter={"where":{"profile_id":` + profileId + `}}`)
              .then(res => {
                professorsdetails.push(res.data);
                this.setState({ professorsList: professorsdetails });
              })
          }
        })
    }

  }

  render() {
    const { onMailSend, onClose, user } = this.props;
    const { receiver_id, subject, message, dateHourMail } = this.state;
    return (
      <Modal className="modal-box modal-box-mail" toggle={onClose} isOpen={this.props.open}
        style={{ zIndex: 2600 }}>
        <ModalHeader className="modal-box-header bg-primary text-white">
          New Message
          <IconButton className="text-white"
            onClick={onClose}>
            <CloseIcon />
          </IconButton>
        </ModalHeader>
        <div className="modal-box-content d-flex flex-column">
          {(() => {
            const roleId = parseInt(localStorage.roles_id);
            if (roleId === roleIdParent) {
              return (
                <div>
                  <TextField
                    required
                    id="receiver_id"
                    label="To"
                    onChange={(event) => this.setState({ receiver_id: event.target.value })}
                    select
                    value={receiver_id}
                    SelectProps={{}}
                    defaultValue={receiver_id}
                    margin="normal"
                    fullWidth >
                    {this.state.professorInfoList.map((professorInfo, index) => (
                      <MenuItem key={index} value={professorInfo.mailAddress}>
                        {professorInfo.subjectName} : {professorInfo.professorName}
                      </MenuItem>
                    ))}
                  </TextField>
                </div>
              )
            } else if (roleId === roleIdProfessor) {
              return (
                <div>
                  <TextField
                    required
                    id="receiver_id"
                    label="To"
                    onChange={(event) => this.setState({ receiver_id: event.target.value })}
                    select
                    value={receiver_id}
                    SelectProps={{}}
                    defaultValue={receiver_id}
                    margin="normal"
                    fullWidth >
                    {this.state.professorsList.map((professorInfo, index) => (
                      <MenuItem key={index} value={professorInfo.address_mail}>
                        {professorInfo.name}  {professorInfo.surname}
                      </MenuItem>
                    ))}
                  </TextField>
                </div>
              )
            }
            else {
              return (
                <div>
                  <TextField
                    id="required"
                    label="To*"
                    onChange={(event) => this.setState({ receiver_id: (event.target.value) })}
                    defaultValue={receiver_id}
                    margin="normal"
                    fullWidth />
                </div>
              )
            }
          })()}
          <TextField
            id="required"
            label="Subject"
            onChange={(event) => this.setState({ subject: event.target.value })}
            value={subject}
            margin="normal"
          />
          <TextField
            id="required"
            label="Message"
            onChange={(event) => this.setState({ message: event.target.value, dateHourMail: moment() })}
            value={message}
            multiline
            rowsMax="4"
            margin="normal" />
        </div>

        <div className="modal-box-footer">
          <Button className="attach-file jr-btn text-muted">
            <i className="zmdi zmdi-attachment mr-2 zmdi-hc-2x" /> Attach File
          </Button>

          <Button disabled={receiver_id === ''} variant="contained" color="primary" onClick={() => {
            onClose();
            onMailSend(
              {
                'id': '15453a06c08fb021776',
                'from': {
                  'name': user.name,
                  'avatar': user.avatar,
                  'email': user.email
                },
                'receiver_id': receiver_id,
                'subject': subject,
                'message': message,
                'dateHourMail': dateHourMail,
                'read': false,
                'starred': false,
                'important': false,
                'hasAttachments': false,
                'folder': 1,
                'selected': false,
                'labels': [],
              })

          }}>
            <i className="zmdi zmdi-mail-send mr-2" /> Send Message</Button>
        </div>
      </Modal>
    );
  }
}

export default ComposeMail;
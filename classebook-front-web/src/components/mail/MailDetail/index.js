import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import labels from 'app/routes/Mail/data/labels';
import CustomScrollbars from 'util/CustomScrollbars';
import baseUrl from 'config/config';
import axios from 'axios';


var dateFormat = require('dateformat');
class MailDetail extends React.Component {

  state = {
    anchorEl: undefined,
    open: false,
    showDetail: false,
    senderInfo: '',
    receiverInfo: '',

  };

  handleClick = event => {
    this.setState({ open: true, anchorEl: event.currentTarget });
  };

  handleRequestClose = () => {
    this.setState({ open: false });
  };

  componentWillMount() {
    axios.get(`${baseUrl.baseUrl}/profiles/` + this.props.mail.sender_id + `/user`)
      .then(res => {
        const senderInfo = res.data;
        this.setState({ senderInfo });
      })
    axios.get(`${baseUrl.baseUrl}/profiles/` + this.props.mail.receiver_id + `/user`)
      .then(res => {
        const receiverInfo = res.data;
        this.setState({ receiverInfo });
      })

  }


  render() {
    const { mail, onStartSelect, onImportantSelect, width } = this.props;
    const options = [
      'Reply',
      'Forward',
      'Print',
    ];
    return (
      <div className="module-detail mail-detail">
        <CustomScrollbars className="module-list-scroll scrollbar" style={{
          height: width >= 1200 ? 'calc(100vh - 315px)' : 'calc(100vh - 295px)'
        }}>
          <div className="mail-header">
            <div className="mail-header-content col pl-0">
              <div className="subject">
                {mail.subject}
              </div>
            </div>
            <div className="mail-header-actions">
              <IconButton type="button" className="icon-btn" onClick={() => {
                onStartSelect(mail);
              }}>
                {mail.starred ?
                  <i className="zmdi zmdi-star" /> :
                  <i className="zmdi zmdi-star-outline" />
                }
              </IconButton>
              <IconButton type="button" className="icon-btn" onClick={() => {
                onImportantSelect(mail);
              }}>
                {mail.important ?
                  <i className="zmdi zmdi-label-alt" />
                  : <i className="zmdi zmdi-label-alt-outline" />
                }
              </IconButton>
            </div>
          </div>
          <hr />
          <div className="mail-user-info">
            <div className="sender-name">{this.state.senderInfo.realm + ' ' + this.state.senderInfo.username}
              <div className="send-to text-grey">to me</div>
            </div>
            <IconButton
              aria-label="More"
              aria-owns={this.state.open ? 'long-SidenavContent.js' : null}
              aria-haspopup
              onClick={this.handleClick}>
              <MoreVertIcon />
            </IconButton>
            <Menu id="long-menu"
              anchorEl={this.state.anchorEl}
              open={this.state.open}
              onClose={this.handleRequestClose}
              MenuListProps={{
                style: {
                  width: 200,
                },
              }}>
              {options.map(option =>
                <MenuItem key={option} onClick={this.handleRequestClose}>
                  {option}
                </MenuItem>,
              )}
            </Menu>
          </div>
          <div className="show-detail" onClick={() => {
            this.setState({ showDetail: !this.state.showDetail });
          }}>{this.state.showDetail ? 'Hide Detail' : 'Show Detail'}</div>
          {this.state.showDetail && (<div>
            <div>
              <strong>From: </strong>{this.state.senderInfo.realm + ' ' + this.state.senderInfo.username}
            </div>
            < div>
              < strong> To: </strong>
              {this.state.receiverInfo.realm + ' ' + this.state.receiverInfo.username}
            </div>
            <div><strong>Date: </strong>{dateFormat(mail.dateHourMail, 'dddd, mmmm dS, yyyy, HH:MM')}</div>
          </div>)}

          <br></br>
          <p className="message">
            <font size="+2"> {mail.message}</font>

          </p>

          {mail.hasAttachments &&
            <div className="attachment-block">
              <h3>Attachments ({mail.hasAttachments.length})</h3>
              <div className="row">
                {mail.attachments.map((attachment, index) =>
                  <div className="col-3" key={index}>
                    <img className="size-100" src={attachment.preview} alt={attachment.fileName} />
                    <div className="size">{attachment.size}</div>
                  </div>
                )}
              </div>
            </div>
          }

        </CustomScrollbars>
      </div>
    );
  }
}

export default MailDetail;
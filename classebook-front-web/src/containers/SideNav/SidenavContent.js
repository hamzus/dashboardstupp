import React, { Component } from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import IntlMessages from 'util/IntlMessages';
import CustomScrollbars from 'util/CustomScrollbars';


class SidenavContent extends Component {

  componentDidMount() {
    const { history } = this.props;
    const that = this;
    const pathname = `${history.location.pathname}`;// get current path

    const menuLi = document.getElementsByClassName('menu');
    for (let i = 0; i < menuLi.length; i++) {
      menuLi[i].onclick = function (event) {
        for (let j = 0; j < menuLi.length; j++) {
          const parentLi = that.closest(this, 'li');
          if (menuLi[j] !== this && (parentLi === null || !parentLi.classList.contains('open'))) {
            menuLi[j].classList.remove('open')
          }
        }
        this.classList.toggle('open');
      }
    }

    const activeLi = document.querySelector('a[href="' + pathname + '"]');// select current a element
    try {
      const activeNav = this.closest(activeLi, 'ul'); // select closest ul
      if (activeNav.classList.contains('sub-menu')) {
        this.closest(activeNav, 'li').classList.add('open');
      } else {
        this.closest(activeLi, 'li').classList.add('open');
      }
    } catch (error) {

    }
  }

  componentWillReceiveProps(nextProps) {
    const { history } = nextProps;
    const pathname = `${history.location.pathname}`;// get current path

    const activeLi = document.querySelector('a[href="' + pathname + '"]');// select current a element
    try {
      const activeNav = this.closest(activeLi, 'ul'); // select closest ul
      if (activeNav.classList.contains('sub-menu')) {
        this.closest(activeNav, 'li').classList.add('open');
      } else {
        this.closest(activeLi, 'li').classList.add('open');
      }
    } catch (error) {

    }
  }

  closest(el, selector) {
    try {
      let matchesFn;
      // find vendor prefix
      ['matches', 'webkitMatchesSelector', 'mozMatchesSelector', 'msMatchesSelector', 'oMatchesSelector'].some(function (fn) {
        if (typeof document.body[fn] == 'function') {
          matchesFn = fn;
          return true;
        }
        return false;
      });

      let parent;

      // traverse parents
      while (el) {
        parent = el.parentElement;
        if (parent && parent[matchesFn](selector)) {
          return parent;
        }
        el = parent;
      }
    } catch (e) {

    }

    return null;
  }

  render() {
    const roleIdSuperAdmin = 1;
    const roleIdAdmin = 2;
    const roleIdProfessor = 3;
    const roleIdParent = 4;
    const roleIdStudent = 5;
    const roleSupervisor = 6;
    const roleId = parseInt(localStorage.roles_id)
    return (
      <CustomScrollbars className=" scrollbar">

        <ul className="nav-menu">
          <li>
            <NavLink className="prepend-icon" to="/app/home">
              <span className="nav-text"><IntlMessages id="sidebar.home" /></span>
            </NavLink>
          </li>
          {(() => {
            if (roleId === roleIdSuperAdmin || roleId === roleIdAdmin) {
              return (
                <li className="menu collapse-box">
                  <Button>
                    <i className="zmdi zmdi-view-dashboard zmdi-hc-fw" />
                    <span className="nav-text">
                      <IntlMessages id="sidebar.administration" />
                    </span>
                  </Button>
                  <ul className="sub-menu">
                    <li>

                      <NavLink className="prepend-icon" to="/app/administration/establishment">
                        <span className="nav-text"><IntlMessages id="sidebar.establishement" /></span>
                      </NavLink>
                    </li>
                    <li>
                      <NavLink className="prepend-icon" to="/app/administration/subjects">
                        <span className="nav-text"><IntlMessages id="sidebar.subjects" /></span>
                      </NavLink>
                    </li>
                    <li>
                      <NavLink className="prepend-icon" to="/app/administration/professor">
                        <span className="nav-text"><IntlMessages id="sidebar.professor" /></span>
                      </NavLink>
                    </li>

                    <li>
                      <NavLink className="prepend-icon" to="/app/administration/student">
                        <span className="nav-text"><IntlMessages id="sidebar.eleve" /></span>
                      </NavLink>
                    </li>
                    <li>

                      <NavLink className="prepend-icon" to="/app/administration/level">
                        <span className="nav-text"><IntlMessages id="sidebar.submenu.classlevel" /></span>
                      </NavLink>
                    </li>
                    <li>
                      <NavLink className="prepend-icon" to="/app/administration/classe">
                        <span className="nav-text"><IntlMessages id="sidebar.classes" /></span>
                      </NavLink>
                    </li>
                    <li>
                      <NavLink className="prepend-icon" to="/app/administration/rooms">
                        <span className="nav-text"><IntlMessages id="sidebar.rooms" /></span>
                      </NavLink>
                    </li>
                  </ul>
                </li>)

            }
          })()}

          {(() => {
            if (roleId === roleIdSuperAdmin || roleId === roleIdAdmin || roleId === roleIdProfessor){
              return (
                <li className="menu no-arrow">
                  <NavLink to="/app/call_register">
                    <i className="zmdi zmdi-view-dashboard zmdi-hc-fw" />
                    <span className="nav-text"><IntlMessages id="pages.call_register" /> </span>
                  </NavLink>
                </li>
              )
            }
          })()}

          {(() => {
            if (roleId === roleIdSuperAdmin || roleId === roleIdAdmin || roleId === roleSupervisor){
              return (
                <li className="menu no-arrow">
                  <NavLink to="/app/billetPass">
                    <i className="zmdi zmdi-view-dashboard zmdi-hc-fw" />
                    <span className="nav-text"><IntlMessages id="pages.sideNav.component.billetPass" /> </span>
                  </NavLink>
                </li>
              )
            }
          })()}
          <li className="menu no-arrow">
            <NavLink to="/app/mail">
              <i className="zmdi zmdi-view-dashboard zmdi-hc-fw" />
              <span className="nav-text"><IntlMessages id="sidebar.components.mail.menu" /> </span>
            </NavLink>
          </li>
          <li className="menu no-arrow">
            <NavLink to="/app/devoir">
              <i className="zmdi zmdi-view-dashboard zmdi-hc-fw" />
              <span className="nav-text"><IntlMessages id="sidebar.components.mail.devoir" /> </span>
            </NavLink>
          </li>

          <li className="menu colapse-box">
            <Button>
              <i className="zmdi zmdi-view-dashboard zmdi-hc-fw" />
              <span className="nav-text">
                <IntlMessages id="sidebar.calendar" />
              </span>
            </Button>
            <ul className="sub-menu">
              <li>
                <NavLink className="prepend-icon" to="/app/calendar/emploi">
                  <span className="nav-text"><IntlMessages id="sidebar.submenu.components.emploi" /> </span>
                </NavLink>
              </li>
              {(() => {
                if (roleId === roleIdSuperAdmin || roleId === roleIdAdmin) {
                  return (
                    <li>
                      <NavLink className="prepend-icon" to="/app/calendar/planning">
                        <span className="nav-text"><IntlMessages id="sidebar.submenu.components.planning" /> </span>
                      </NavLink>
                    </li>
                  )
                }
              })()}
            </ul>
          </li>
          <li className="menu colapse-box">
            <Button>
              <i className="zmdi zmdi-view-dashboard zmdi-hc-fw" />
              <span className="nav-text">
                <IntlMessages id="sidebar.notes" />
              </span>
            </Button>
            <ul className="sub-menu">
              <li>
                <NavLink className="prepend-icon" to="/app/gradesmenu/grades">
                  <span className="nav-text"><IntlMessages id="sidebar.submenu.components.grades" /> </span>
                </NavLink>
              </li>
              <li>
                <NavLink className="prepend-icon" to="/app/gradesmenu/exams">
                  <span className="nav-text"><IntlMessages id="sidebar.submenu.components.exams" /> </span>
                </NavLink>
              </li>
              <li>
                <NavLink className="prepend-icon" to="/app/gradesmenu/report">
                  <span className="nav-text"><IntlMessages id="sidebar.submenu.components.report" /> </span>
                </NavLink>
              </li>
            </ul>
          </li>
          <li className="menu no-arrow">
            <NavLink to="/app/complaints">
              <i className="zmdi zmdi-view-dashboard zmdi-hc-fw" />
              <span className="nav-text"><IntlMessages id="sidebar.components.complaints" /> </span>
            </NavLink>
          </li>
          <li className="menu no-arrow">
            <NavLink to="/app/cafeteria">
              <i className="zmdi zmdi-view-dashboard zmdi-hc-fw" />
              <span className="nav-text"><IntlMessages id="sidebar.components.cafeteria" /> </span>
            </NavLink>
          </li>
          <li className="menu no-arrow">
            <NavLink to="/app/health-monitoring">
              <i className="zmdi zmdi-view-dashboard zmdi-hc-fw" />
              <span className="nav-text"><IntlMessages id="sidebar.components.health-monitiring" /> </span>
            </NavLink>
          </li>
          <li className="menu no-arrow">
            <NavLink to="/app/e-learning">
              <i className="zmdi zmdi-view-dashboard zmdi-hc-fw" />
              <span className="nav-text"><IntlMessages id="sidebar.components.e-learning" /> </span>
            </NavLink>
          </li>
          <li className="menu no-arrow">
            <NavLink to="/app/payments">
              <i className="zmdi zmdi-view-dashboard zmdi-hc-fw" />
              <span className="nav-text"><IntlMessages id="sidebar.components.payments" /> </span>
            </NavLink>
          </li>

        </ul>

      </CustomScrollbars >
    );
  }
}

export default withRouter(SidenavContent);

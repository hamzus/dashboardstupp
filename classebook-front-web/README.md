# Project Name ClassEbook 

# Dev Team 

Name Surname

OUNI Sabri, 
BELHEDI Chokri, 
GHARBI Ghassen, 
MEJRI Ibtihel, 
Kais HLIOUI, 
Mohamed BELAID, 
Malika TAHRI...

# We bought the Jumbo React to fast delivrey the classebook application.

[Jumbo React - React Redux Material Admin Template (3.0)](https://themeforest.net/item/react-material-bootstrap-4-admin-template/20978545 "Jumbo React")
Jumbo React is a react redux based admin template. 

# Installation

**Note: Jumbo React is using [yarn](https://yarnpkg.com/en/docs/install) instead npm**

Installing all the dependencies of project, run following command:

``` $yarn ```

# Documentation

it is well documented at [here](http://jumbo-react.g-axon.com/docs/material/ "Documentation") .

## Branches
| Branch                           | Description   |
| -------------------------------- | ------------- |
| `master`                         | This is the main production branch. You should pull from here to update your local repo. |
| `starter-template`               | This branch is for you to kickstart your project with the starter template with auth (Firebase). This way you can start building your project from ground-up. |
| `starter-template-without-auth`  | If you are looking to use a different authentication system, this is the right branch for you to start with. |

## Issues
##### We accept issues here on GitHub but please follow the rules defined below before raising an issue:

* An issue must be related to coding. It should not be a general query. If you have any query, please ask that [here](https://themeforest.net/item/react-material-bootstrap-4-admin-template/20978545/support "Support for Jumbo React")
* You can request a feature as an issue. We will make this open for voting for other users. So that they can vote and we can build what is in demand.
* While posting an issue be descriptive with the process to replicate the issue.

## Pull Requests
We appreciate your collaboration. If you build something which you believe could help others in their project. You can make that as a pull request. But make sure that it should be in a genral uses rather then something which is built for a specific project.

We merge only those features which we find useful for the community.
## Install mocha and chai

sudo npm install -g mocha
npm install --save-dev mocha
npm install chai --save-dev

## command run test mocha

mocha
## command run   DockerFIleOld 
docker build . -t << image_name >> . 
docker image ls
docker run -p 8000:80 << container_name >>
Docker container ps
## command run   DockerFIle 
sudo docker build -t imageName:tag .
sudo docker images
sudo docker run -it --name containerName -p 3000:3000 imageName
sudo docker ps  